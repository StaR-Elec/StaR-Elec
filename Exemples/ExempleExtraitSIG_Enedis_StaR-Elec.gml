<?xml version="1.0" encoding="UTF-8"?>
<gml:FeatureCollection xmlns:gss="http://www.isotc211.org/2005/gss" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gsr="http://www.isotc211.org/2005/gsr" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gmlexr="http://www.opengis.net/gml/3.3/exr" xmlns:star="http://StaR-Elec.com" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gts="http://www.isotc211.org/2005/gts" gml:id="id1c04843b-4ab5-4b0f-97e3-1a30d9054493">
<gml:boundedBy>
<gml:Envelope srsName="EPSG:2154" srsDimension="2">
<gml:lowerCorner>634256.874770253 6313844.530405261</gml:lowerCorner>
<gml:upperCorner>635198.730633151 6314371.692150872</gml:upperCorner>
</gml:Envelope>
</gml:boundedBy>
<gml:featureMember><star:Transformateur gml:id="idb6f19bf4-547e-46ea-b6fd-9b2ac2f38c0f">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="ide8ebf9d6-1f3d-4764-af2a-fa29e4fa611a"/>
<star:NatureEnroulement>EnroulementsSepares</star:NatureEnroulement>
<star:Puissance uom="kVA">630</star:Puissance>
<star:ReglagePriseFixe xlink:href="2.5"/>
</star:Transformateur>
</gml:featureMember>
<gml:featureMember><star:Transformateur gml:id="idedaf18b2-c6bd-43c7-83cc-fd4d8d4f6203">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id779e2f45-0478-40ed-9a9e-d29982706bd0"/>
<star:NatureEnroulement>EnroulementsSepares</star:NatureEnroulement>
<star:Puissance uom="kVA">630</star:Puissance>
</star:Transformateur>
</gml:featureMember>
<gml:featureMember><star:Transformateur gml:id="idecb38fc6-2132-4eaf-9d7f-770541317dfe">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idb224adf6-2684-42c8-9ee5-9b16bcdb5942"/>
<star:NatureEnroulement>EnroulementsSepares</star:NatureEnroulement>
<star:Puissance uom="kVA">630</star:Puissance>
</star:Transformateur>
</gml:featureMember>
<gml:featureMember><star:Transformateur gml:id="id3c4383a1-b226-4d09-b03e-e1c9396865d4">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="ida6e3c341-ec53-4330-b400-d43b242c494a"/>
<star:NatureEnroulement>EnroulementsSepares</star:NatureEnroulement>
<star:Puissance uom="kVA">630</star:Puissance>
</star:Transformateur>
</gml:featureMember>
<gml:featureMember><star:Transformateur gml:id="id737e1310-5409-45e4-b79d-3a69cf4ad342">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Projected</star:Statut>
<star:noeudParent xlink:href="id87fb6532-7c2c-4a6e-a58d-43c589661fc0"/>
<star:NatureEnroulement>EnroulementsSepares</star:NatureEnroulement>
<star:Puissance uom="kVA">400</star:Puissance>
<star:ReglagePriseFixe xlink:href="2.5"/>
</star:Transformateur>
</gml:featureMember>
<gml:featureMember><star:Transformateur gml:id="id5ec4759b-f981-41ba-97b2-07bd311f118d">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Projected</star:Statut>
<star:noeudParent xlink:href="id355da1ea-34d1-4a9f-86bf-426c487fd621"/>
<star:NatureEnroulement>EnroulementsSepares</star:NatureEnroulement>
<star:Puissance uom="kVA">630</star:Puissance>
<star:ReglagePriseFixe xlink:href="2.5"/>
</star:Transformateur>
</gml:featureMember>
<gml:featureMember><star:TableauHTA gml:id="idc1a982ce-14f5-41eb-a30d-106f8ffbbfcc">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire></star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="ide8ebf9d6-1f3d-4764-af2a-fa29e4fa611a"/>
<star:NombreEmplacementsCellules>4</star:NombreEmplacementsCellules>
</star:TableauHTA>
</gml:featureMember>
<gml:featureMember><star:TableauHTA gml:id="id2ee3be85-58f3-4876-8a0b-fb5a6a0f4975">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire></star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id779e2f45-0478-40ed-9a9e-d29982706bd0"/>
<star:NombreEmplacementsCellules>4</star:NombreEmplacementsCellules>
</star:TableauHTA>
</gml:featureMember>
<gml:featureMember><star:TableauHTA gml:id="idf5a4c4d4-769f-4a7b-83f1-bb32f346d1d6">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire></star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id87fb6532-7c2c-4a6e-a58d-43c589661fc0"/>
<star:NombreEmplacementsCellules>3</star:NombreEmplacementsCellules>
</star:TableauHTA>
</gml:featureMember>
<gml:featureMember><star:TableauHTA gml:id="id1381bb36-6394-4808-b119-60588cb4e815">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire></star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id7d7a6396-cad7-4ea1-844e-64cbf4937257"/>
<star:NombreEmplacementsCellules>3</star:NombreEmplacementsCellules>
</star:TableauHTA>
</gml:featureMember>
<gml:featureMember><star:TableauHTA gml:id="id57d00150-1c9a-4008-8d0b-e411d7a14c6d">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire></star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id355da1ea-34d1-4a9f-86bf-426c487fd621"/>
<star:NombreEmplacementsCellules>3</star:NombreEmplacementsCellules>
</star:TableauHTA>
</gml:featureMember>
<gml:featureMember><star:TableauBT gml:id="id9c3cb43e-62d4-41d4-89b4-c9133533f9ea">
<star:reseau xlink:href="Reseau"/>
<star:Code>1383149758</star:Code>
<star:Commentaire>P81200</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="ide8ebf9d6-1f3d-4764-af2a-fa29e4fa611a"/>
<star:NombrePlages>8</star:NombrePlages>
</star:TableauBT>
</gml:featureMember>
<gml:featureMember><star:TableauBT gml:id="idd35400f0-a296-4952-a070-a7e4d0b91ace">
<star:reseau xlink:href="Reseau"/>
<star:Code>1608899811</star:Code>
<star:Commentaire>P81800</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id779e2f45-0478-40ed-9a9e-d29982706bd0"/>
<star:NombrePlages>8</star:NombrePlages>
</star:TableauBT>
</gml:featureMember>
<gml:featureMember><star:TableauBT gml:id="idfdf2b440-ce74-49ba-9b50-8c2a3eaeaece">
<star:reseau xlink:href="Reseau"/>
<star:Code>2576111580</star:Code>
<star:Commentaire>P81200</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idb224adf6-2684-42c8-9ee5-9b16bcdb5942"/>
<star:NombrePlages>8</star:NombrePlages>
</star:TableauBT>
</gml:featureMember>
<gml:featureMember><star:TableauBT gml:id="id8ef6ad98-29d3-4f9e-b1df-d8a1ebe81506">
<star:reseau xlink:href="Reseau"/>
<star:Code>2576111195</star:Code>
<star:Commentaire>P81200</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="ida6e3c341-ec53-4330-b400-d43b242c494a"/>
<star:NombrePlages>8</star:NombrePlages>
</star:TableauBT>
</gml:featureMember>
<gml:featureMember><star:TableauBT gml:id="idf25dc22b-d6bf-4c49-a40c-8a36e1a1c15d">
<star:reseau xlink:href="Reseau"/>
<star:Code>1465988072</star:Code>
<star:Commentaire>P81200</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id87fb6532-7c2c-4a6e-a58d-43c589661fc0"/>
<star:NombrePlages>8</star:NombrePlages>
</star:TableauBT>
</gml:featureMember>
<gml:featureMember><star:TableauBT gml:id="id03e50ab1-8f11-4fa1-9ce4-54e68f15d70e">
<star:reseau xlink:href="Reseau"/>
<star:Code>1465987760</star:Code>
<star:Commentaire>P81800</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id355da1ea-34d1-4a9f-86bf-426c487fd621"/>
<star:NombrePlages>8</star:NombrePlages>
</star:TableauBT>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="idf0c7290c-9885-461e-85f9-a960300c7035">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634866.9655275227 6314265.87664864</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="id64500598-d690-4c4c-b9d9-bceb242a6f5b">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634705.8197157662 6314209.108055593</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="idc28345c7-bc0e-4d8c-b6ce-092a7395b9b1">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>635198.730633151 6314371.692150872</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="id846e5aed-a34b-4dfd-8268-a3e54b396bb2">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634914.2321507887 6314277.252067167</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="id28e3a1d6-f0ba-49f4-aa41-c7a50a595583">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634871.2901016492 6314307.489665393</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="id8786b0db-a75f-448d-93c0-42a8ba6fde87">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634868.508373422 6314310.160286384</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="idef78bb67-55f4-4408-ae54-aadb198e8d7a">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634877.7659778874 6314282.162254252</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="id83a6bdc4-8bdd-4b9c-a627-30cd851ddcc6">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634879.2277718488 6314277.548709505</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="ida6fc77ab-a7c3-4f34-ab36-98978cbe95e7">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634879.0542541001 6314276.988740775</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="id9c10cd18-bddd-4775-b6af-5460d2486d71">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634505.4797312899 6314185.884168164</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="id881533b9-5f60-40b6-b78b-4e11dd0dd79a">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634614.0603587572 6314204.568237465</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="id61726455-1b22-46c9-be05-8ced4e260888">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634509.7800369256 6314185.972966814</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="ida2849593-4e3b-4905-9e77-3ff82c8b9198">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634815.1098364544 6314229.726168815</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="id1bfee6be-544f-4656-ab53-2abf9aed4c53">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634815.9063089796 6314227.734522774</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="id0535c79e-043a-457d-8c1f-b1d0bbd98705">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634617.04748199 6314191.239821343</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="id1a2a53c3-05f8-4d27-9a52-82cb4d1c1693">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634616.6195715844 6314184.521304049</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="id70047720-e8be-4524-8617-23e6fbfc327e">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634512.1012678836 6314174.2445021</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:Support gml:id="id80a45a22-0927-4c8d-a92a-7259c2586614">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634256.874770253 6314248.526480904</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Support>
</gml:featureMember>
<gml:featureMember><star:ReseauUtilite gml:id="Reseau">
<star:Mention>Réseau pour tests StaR-Elec</star:Mention>
<star:Nom>Réseau de Distribution Publique exploité par Enedis</star:Nom>
<star:Responsable>Enedis</star:Responsable>
<star:Theme>ELECTRD</star:Theme>
</star:ReseauUtilite>
</gml:featureMember>
<gml:featureMember><star:PosteElectrique gml:id="ide8ebf9d6-1f3d-4764-af2a-fa29e4fa611a">
<star:reseau xlink:href="Reseau"/>
<star:Code>81004P0401</star:Code>
<star:Commentaire>LEBON PHILLIPE</star:Commentaire>
<star:DateConstruction>2012-03-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="iddcca61e7-c850-4bb4-92e6-ce1ba701a60a"/>
<star:Categorie xlink:href="Distribution"/>
<star:Fonction xlink:href="TransformationHTA-BT"/>
<star:TypePoste xlink:href="UP - Urbain Portable (PAC)"/>
</star:PosteElectrique>
</gml:featureMember>
<gml:featureMember><star:PosteElectrique gml:id="id779e2f45-0478-40ed-9a9e-d29982706bd0">
<star:reseau xlink:href="Reseau"/>
<star:Code>81004P0080</star:Code>
<star:Commentaire>INOPROD4</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="idad8670ab-73a7-4594-b120-c2f3ebf903ff"/>
<star:Categorie xlink:href="Distribution"/>
<star:Fonction xlink:href="TransformationHTA-BT"/>
<star:TypePoste xlink:href="UP - Urbain Portable (PAC)"/>
</star:PosteElectrique>
</gml:featureMember>
<gml:featureMember><star:PosteElectrique gml:id="idb224adf6-2684-42c8-9ee5-9b16bcdb5942">
<star:reseau xlink:href="Reseau"/>
<star:Code>81004P0249</star:Code>
<star:Commentaire>COUSTOU</star:Commentaire>
<star:DateConstruction>2023-02-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id1e7b1781-2915-40dd-836c-4721f495e55c"/>
<star:Categorie xlink:href="Distribution"/>
<star:Fonction xlink:href="TransformationHTA-BT"/>
<star:TypePoste xlink:href="UP - Urbain Portable (PAC)"/>
</star:PosteElectrique>
</gml:featureMember>
<gml:featureMember><star:PosteElectrique gml:id="ida6e3c341-ec53-4330-b400-d43b242c494a">
<star:reseau xlink:href="Reseau"/>
<star:Code>81004P0233</star:Code>
<star:Commentaire>MOISSAN</star:Commentaire>
<star:DateConstruction>2023-02-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="ide2b4fe7b-b477-4648-9de7-eff2beefdc56"/>
<star:Categorie xlink:href="Distribution"/>
<star:Fonction xlink:href="TransformationHTA-BT"/>
<star:TypePoste xlink:href="UP - Urbain Portable (PAC)"/>
</star:PosteElectrique>
</gml:featureMember>
<gml:featureMember><star:PosteElectrique gml:id="id87fb6532-7c2c-4a6e-a58d-43c589661fc0">
<star:reseau xlink:href="Reseau"/>
<star:Code>81004P0083</star:Code>
<star:Commentaire>INOPROD2</star:Commentaire>
<star:DateConstruction>2012-04-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id0232e615-0515-4309-814e-2c89ea9fe662"/>
<star:Categorie xlink:href="Distribution"/>
<star:Fonction xlink:href="TransformationHTA-BT"/>
<star:TypePoste xlink:href="UP - Urbain Portable (PAC)"/>
</star:PosteElectrique>
</gml:featureMember>
<gml:featureMember><star:PosteElectrique gml:id="id7d7a6396-cad7-4ea1-844e-64cbf4937257">
<star:reseau xlink:href="Reseau"/>
<star:Code>81004P6132</star:Code>
<star:Commentaire>AUROCK</star:Commentaire>
<star:DateConstruction>2017-06-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id8dad5015-0c4a-41aa-a518-6a7f798befa1"/>
<star:Categorie xlink:href="Distribution"/>
<star:Fonction xlink:href="Client HTA"/>
<star:TypePoste xlink:href="UP - Urbain Portable (PAC)"/>
</star:PosteElectrique>
</gml:featureMember>
<gml:featureMember><star:PosteElectrique gml:id="ida494e7dd-f674-4d00-8ced-cee8da938d86">
<star:reseau xlink:href="Reseau"/>
<star:Code>81004P6677</star:Code>
<star:Commentaire>STIMIP</star:Commentaire>
<star:DateConstruction>1985-12-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id0bfdbf4b-809b-46d3-ae10-68f0f7f6ab6e"/>
<star:Categorie xlink:href="Distribution"/>
<star:Fonction xlink:href="Client HTA"/>
<star:TypePoste xlink:href="RC - Rural Compact"/>
</star:PosteElectrique>
</gml:featureMember>
<gml:featureMember><star:PosteElectrique gml:id="id355da1ea-34d1-4a9f-86bf-426c487fd621">
<star:reseau xlink:href="Reseau"/>
<star:Code>81004P0084</star:Code>
<star:Commentaire>INOPROD 3</star:Commentaire>
<star:DateConstruction>2012-04-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="idad93cb22-ef1e-4b52-a04f-ce763565d9fd"/>
<star:Categorie xlink:href="Distribution"/>
<star:Fonction xlink:href="TransformationHTA-BT"/>
<star:TypePoste xlink:href="UP - Urbain Portable (PAC)"/>
</star:PosteElectrique>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id779203f2-f5ab-454e-b6f0-a974f3ad0da8">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634743.0311629407 6314023.699712921 634738.1825777744 6314025.775299455 634737.7049423842 6314025.879203755 634730.1954783892 6314018.223114833 634712.7296552095 6314007.98727824 634705.4192678643 6314003.703059519 634690.284346493 6313996.842188225 634665.9320662606 6313988.0886585945 634651.319132563 6313982.601994344 634634.4855005534 6313976.281832563 634633.8823174933 6313975.231974365</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id1c9edea4-b327-4512-8c28-b05297cc0e52">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634633.8823174933 6313975.231974365 634632.7650883647 6313975.544035971 634572.924135952 6313953.04261165 634572.3277166681 6313951.726969244</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="idd9111e79-6fef-406f-988f-acfd3bce294a">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634742.8034005335 6314023.223114291 634739.0893859835 6314024.671816368 634738.6320633561 6314024.8155092895 634738.0733388314 6314024.660360194 634735.3798505733 6314021.766945619 634734.8983910701 6314020.341450508 634734.79967738 6314018.575090932 634743.3399912127 6313995.342201856 634743.7813317543 6313994.841009781 634744.1544343305 6313994.779939346 634744.5567196889 6313994.86347555 634745.027036062 6313995.078305826</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id7bdbb082-55d2-44eb-8f5d-ff0d5f8d0796">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634756.637195532 6314179.294896901 634756.8693833051 6314179.703528118 634757.2425271769 6314180.362718752 634757.3662257956 6314180.580456737 634759.3803034283 6314181.076035684 634763.0085429343 6314174.278538572 634769.7788566696 6314164.299918504 634787.6475193875 6314147.025583206 634792.6307096476 6314153.026583843 634791.1772793622 6314155.776968422</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id30dbb4c8-33b8-4a1c-ac50-acbecb20eb39">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634791.1772793622 6314155.776968422 634793.9216649497 6314154.372363199 634799.789795666 6314160.35595054 634818.1769484803 6314180.997380368 634823.2339019962 6314178.872094585</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="idd88abc5c-9d71-4a8b-9914-b6e8dd083001">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634823.2339019962 6314178.872094585 634820.2401057943 6314181.866164391 634823.6470527417 6314185.7365861405 634827.2901217566 6314182.854948837</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id7d5f8f1c-d37a-4ad2-8cea-e634abadbcc0">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634791.1772793622 6314155.776968422 634793.8780464726 6314153.101033167 634787.8804059022 6314145.96771238 634782.2626357205 6314140.41558025 634779.9159933043 6314141.600071171</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id335f3c01-31a3-4187-aea0-540b8dd83b13">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634779.9159933043 6314141.600071171 634781.3142714624 6314139.659319513 634779.6825526663 6314137.822904799 634766.0872715553 6314122.524756822 634754.8267630913 6314109.87129096 634758.0803092659 6314106.760160283</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="idabc630ce-4eea-4d8c-9828-462de6ccea5b">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634691.5091692691 6314156.015668331 634690.7319517469 6314161.49757339</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="idcdbb5b6b-7922-4f14-80e1-7ca11c54f215">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634755.0168452603 6314179.41138406 634753.7761889608 6314181.803349446 634739.0008454961 6314176.963385795 634722.3753304438 6314171.517579724 634719.9822240883 6314170.7334794095 634712.8700237427 6314168.403591852 634698.9002159317 6314161.342141555 634692.6720824065 6314159.367465898 634691.672960323 6314156.487808771 634691.5091692691 6314156.015668331</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id9c0fe002-eb6b-4f24-a0bb-18ff69a362a2">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634691.5091692691 6314156.015668331 634689.158873426 6314158.314047532 634671.3782218366 6314152.828981223 634669.208556535 6314151.767289202 634667.7200120098 6314153.545964939</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id56363a28-2c8f-4d55-951e-1ee54fdb79d6">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634753.7776118946 6314178.515708764 634753.2704698516 6314178.674804258 634752.5989364074 6314178.886226542 634750.2773406048 6314178.052576586 634750.2708842797 6314177.758931914 634750.2395429647 6314176.283707853</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="iddbedb5eb-8fb0-4eae-b265-5000ba4462d2">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634750.2395429647 6314176.283707853 634748.7461552509 6314177.365139366 634740.360864559 6314174.629358695 634714.3069052122 6314165.824170997 634710.8796944603 6314163.7081202045 634708.9756175877 6314160.960926413 634709.0585185462 6314156.7865109425 634710.3966450945 6314151.720469317 634712.554920753 6314146.177030226 634716.4889662019 6314138.678684173 634719.6339203108 6314135.125918303 634721.4330837427 6314135.962951828</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="idb4ebb6bf-2a3e-4ab6-a218-aa9feab8cbe5">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634721.4330837427 6314135.962951828 634720.2933503861 6314134.185345399 634723.9256691181 6314130.855258626 634725.8401662197 6314131.7462683</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id3e8bc62f-ff16-4393-9036-f2b97b78e64b">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634725.8401662197 6314131.7462683 634724.7629253485 6314130.035069013 634733.7970736054 6314121.984462179 634740.1004432652 6314116.36629212 634736.1503362418 6314111.933008686</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id52db09b6-42eb-4cb4-9b16-d14e5f1f7f47">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634755.475068977 6314179.494451027 634755.3608158352 6314181.953888451 634758.9655353688 6314183.437097509 634757.4492785572 6314188.465673547</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="idacf8d136-e995-43e1-b76c-d1e001dce96a">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634757.4492785572 6314188.465673547 634757.6610402153 6314184.171298908 634753.4238253848 6314182.670419232 634737.5886981694 6314177.606584478 634712.3940841943 6314169.186785332 634709.4013991823 6314167.902233031 634707.7972857427 6314170.6718631275 634704.0019521125 6314176.126140578 634700.7293052176 6314184.230311588 634701.5908744029 6314184.642653914</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id4dfe735f-fdfe-4cb8-851c-f63972a4f9f7">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634755.9644957926 6314179.486349459 634756.6188343547 6314181.157141476 634761.2442106032 6314182.605815151 634762.685091922 6314181.576770812</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="idd7235fa4-c292-414c-ada7-c343c775375d">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634757.4492785572 6314188.465673547 634759.4522295546 6314184.652763994 634764.564225554 6314186.235214182 634764.4659816684 6314186.55171462 634763.3425861232 6314190.1814261265</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id268a5bef-b5e0-423c-9250-567599332763">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634762.685091922 6314181.576770812 634762.7730997421 6314183.138430403 634770.6659251412 6314185.877343147 634790.2837795067 6314192.685623894 634792.2046576967 6314190.403833414</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id73e727f9-4ccc-48c2-83f3-9680c9cfd410">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634792.2046576967 6314190.403833414 634791.9476484022 6314193.113213344 634815.346982842 6314201.517103203 634814.4980022328 6314205.706936082</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id4c204eba-a29f-45eb-8246-40bef9f5b18e">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634814.4980022328 6314205.706936082 634816.1421560415 6314201.874059811 634821.4394476779 6314203.633772057 634820.3861589842 6314207.6425063</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id18db566d-42bd-48e6-9924-e0a1776d5339">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634821.4063028275 6314104.5386675205 634823.1082343111 6314106.885968634 634825.5438092892 6314109.2810600605 634827.2566735235 6314110.787132035 634829.0555939727 6314111.952830411</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="ided89ea9f-f193-444f-a59b-be65a09d2620">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634823.105126144 6314103.176798433 634824.9823393506 6314105.340815592 634826.6831518788 6314107.793018525 634827.2597140389 6314108.288668631 634827.8676469725 6314108.355495054 634828.7571665305 6314107.811583191 634830.9211867756 6314105.934336165 634832.0206819458 6314104.817248836 634834.0823062959 6314103.47631169 634837.1107778295 6314102.236152203 634839.9633312491 6314101.460993108 634844.8582272258 6314101.097261745 634847.111845332 6314101.329099472 634849.5871066648 6314102.380235792 634852.9983353617 6314104.13878697 634856.4397295682 6314105.3246720955 634866.514299272 6314108.778529961 634867.6586323451 6314108.839857637 634868.1917514851 6314108.442788003 634869.732270756 6314104.640750542 634870.3515983983 6314101.775488071</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="ida1a8babe-5028-4916-a487-c65fbd155874">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634815.9063089796 6314227.734522774 634817.6320568031 6314227.916844084 634819.6546712011 6314230.146573089 634826.461516855 6314245.3917811755 634826.838318116 6314246.845126799 634952.0718997186 6314285.932409464 634976.8604803501 6314294.894059344 634977.834953173 6314295.189573228 634978.7373164213 6314294.270937566 634980.3722211842 6314289.334270259 634981.3857072721 6314289.040061194 634981.7127627435 6314289.206144462 635049.8862134529 6314315.890297407 635075.272621068 6314325.977771226 635076.1981340173 6314326.152819709 635077.1696491571 6314326.572231335</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id08abc826-cebc-42df-816c-08b6c2232686">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634815.1098364544 6314229.726168815 634817.860062598 6314231.164599197 634825.7142726199 6314246.820590985 634827.1589446603 6314247.316950578 634892.2347808437 6314268.182088459 634892.6401492709 6314268.514343726 634892.9161599245 6314269.384134097 634893.0528246266 6314269.718643474 634892.8571097997 6314270.32466558 634882.7265056204 6314307.872237019 634882.7031038157 6314308.179118873 634882.7235949615 6314308.478639692 634882.8662057172 6314308.688227234 634881.5628691617 6314312.183587962 634882.796692836 6314312.552848425</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="idda68f735-4d55-4a20-8ea3-f0fc75ce6a98">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634882.8131834401 6314312.017259105 634881.0847906463 6314311.519283714 634880.7808856166 6314311.373984713 634880.6314918179 6314311.189428465 634880.6050407604 6314310.893953547 634893.6364657211 6314270.105345883 634893.8033042878 6314269.868188044 634894.1181472465 6314269.6497681355 634894.6201441508 6314269.472734179 634895.1818850744 6314269.510977792 634921.2057211996 6314277.582168583 634950.3239804169 6314286.203809268 634952.9045367831 6314286.94537878 634955.7417345087 6314288.033437883 634972.9532694168 6314294.149633349 634979.4364463707 6314296.280006656 634979.908445457 6314296.33798361 634980.2560315719 6314296.21219374 634980.478518379 6314295.820726799 634982.6056154462 6314290.371451907 634982.9115520103 6314290.163096446 634983.1532352562 6314290.1550751375 634983.6949702314 6314290.309367726 635005.4874241085 6314299.721714949 635023.9466386483 6314307.0262140725 635075.3300600193 6314326.752494548 635077.0990121692 6314326.249155766</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="ideb903d38-ea13-4408-9c47-785890bf63da">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634385.0629434783 6313844.530405261 634366.2640843194 6313862.620705544 634362.4947799398 6313865.697201114 634362.314469123 6313866.949430801 634362.7773287889 6313868.301157692 634370.0775238846 6313876.376574683 634373.1677982225 6313878.339611993 634559.712097113 6313949.861818097 634656.4813674454 6313985.53064212 634689.5003667163 6313997.701889848 634707.4600684241 6314006.076488201 634718.1808440197 6314012.495879091 634730.0200757002 6314019.58419189 634730.6865975603 6314019.728446951 634731.2703115838 6314019.527751144 634746.6781342014 6313978.363583185 634747.0768205298 6313978.017590126 634747.6965560648 6313978.062340106 634748.2141638247 6313978.317731656 634748.3217960595 6313978.883247851 634748.173453417 6313979.658698249 634733.637621365 6314017.948491702 634733.3550565509 6314020.469279288 634733.6853186942 6314023.044864581 634737.1311916383 6314026.792085522 634737.9750207224 6314026.994791734 634738.6756615693 6314026.918985968 634741.9438914941 6314025.083424032 634743.4919951174 6314024.213316437</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id9ad58fbf-ca23-4fb6-8000-8be2544ff5fb">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634736.2845750108 6314030.071831309 634734.054491348 6314027.8798060585 634730.0947009359 6314021.933156257 634728.6273843364 6314020.641802129 634709.5962742958 6314009.225320508 634687.8926913613 6313999.2118414 634450.7241857284 6313910.585290373 634376.1192606187 6313881.619464292 634368.9784325012 6313878.137997173 634365.2308459161 6313874.268434828 634364.5784484504 6313873.902288597 634363.6486945593 6313873.817183093 634362.6307133706 6313874.290245933 634352.6684763982 6313882.454525494 634326.5509087498 6313913.508984373 634311.4903396223 6313933.88255226 634311.1137679822 6313934.721853514 634311.0831922097 6313935.245573112 634311.8665526788 6313937.05414028</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id33252433-1d13-42ef-9ae6-4183b7ad2422">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634736.2845750108 6314030.071831309 634727.5138088799 6314036.054346905 634713.7355468816 6314036.592495351 634687.9872478873 6314065.577985871 634679.7178529752 6314075.004758968 634673.9434590907 6314080.089034146 634666.8598470044 6314088.205193944 634652.4510747436 6314109.23062657 634644.9704684161 6314119.206213238 634637.2637903588 6314132.006801318 634642.2269998221 6314134.31176035 634658.1327566919 6314144.109148745 634667.2630024505 6314149.792651453 634671.769921711 6314152.245290565 634698.9463696425 6314160.531585179 634710.5942616415 6314165.767405374 634713.4374350972 6314167.568683286 634753.0348229767 6314180.714691284 634753.7019893712 6314180.935862076 634754.6702863432 6314179.063650807</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id40498fb3-7baa-41a3-a51d-19bcf2c9f620">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634754.3011516233 6314178.882935865 634753.3054638951 6314179.7514064135 634752.149437241 6314179.367507608 634713.868360142 6314166.658997518 634711.0693725782 6314164.885320259 634699.2981473523 6314159.594592044 634672.1577602177 6314151.319982652 634667.7662415883 6314148.929310946 634658.6580887772 6314143.259608574 634642.7011202534 6314133.430682569 634638.6926453366 6314131.569251574 634645.8003042663 6314119.765671182 634653.2631145534 6314109.812211253 634667.650170506 6314088.817929019 634674.6526513039 6314080.795354524 634680.4270535586 6314075.712078246 634688.7360193997 6314066.239020319 634714.2003044959 6314037.5745854685 634727.8407436191 6314037.040590098 634736.4756650401 6314031.179091121 634737.8531801258 6314030.456264972 634741.5378962145 6314027.060805412 634743.0264728662 6314026.716658285 634743.9573342584 6314027.1723741805 634744.9476380352 6314028.16504025</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id4c52668a-04f8-431d-a770-48d972fa2ba9">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634760.2717054063 6314022.190486117 634759.2546221071 6314023.366820525 634757.2966367367 6314025.869695576 634755.9188278329 6314027.630459759 634753.9413208188 6314028.280399009 634752.0208378991 6314028.463338331 634748.2448986016 6314028.244270813</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="idc12b7ecd-4a14-45f8-b906-85a50cb931c8">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634746.5237296319 6314024.793263202 634748.499555125 6314026.327098522 634749.8606265992 6314026.622366345 634752.7980187632 6314026.553770432 634756.374060358 6314024.37597464 634758.5817172411 6314021.983889398 634759.6611256278 6314020.85398401</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id4c3f6a8d-2d3a-48b9-b9fa-8a32c0b9c14a">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634748.2448986016 6314028.244270813 634747.860110551 6314028.341402391 634746.3121884703 6314028.994730787 634745.4611041484 6314029.595261753 634744.3881272594 6314030.658181821 634744.139250286 6314031.357554484 634744.1813096446 6314031.8466996625 634744.4059945481 6314032.194456267 634745.4984425494 6314033.33611181 634768.2110670428 6314057.070046369 634799.077970838 6314091.736295516 634814.9178252106 6314109.525040259 634815.3044823548 6314109.889419688 634815.9734205429 6314110.083603232 634816.7383118621 6314109.926341424 634817.6368073719 6314109.619111479 634819.1642493309 6314108.906016241 634821.3170248383 6314107.23665048 634821.9135992943 6314106.663228853 634821.9303831414 6314105.804968045 634821.0775059034 6314104.880078438</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="idf65cfb2c-95bf-4579-b1c4-f2e5414b6e63">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634822.1129318873 6314103.985301156 634823.6180361931 6314105.732870118 634826.0885359737 6314108.600184008 634827.0424725296 6314109.067694015 634827.8554365247 6314108.925013562 634829.0037573832 6314108.270042333 634831.9995483715 6314105.394833999 634834.7666911879 6314103.6064312095 634841.2316169194 6314101.651147817 634843.9103839423 6314101.356954843 634845.0307183362 6314101.415486932 634846.8646902555 6314101.637858302 634848.2958015952 6314102.30415741 634851.2131896834 6314103.663265195 634853.7705419555 6314104.854568422 634858.6455339185 6314106.288161022 634868.447320635 6314109.734317447 634878.9384336781 6314113.423436101 634884.9281979649 6314116.516965193 634889.1644208756 6314119.807017972 634891.2919805192 6314122.639247584 634895.4915483267 6314129.661780647 634901.7780533893 6314142.775542539 634907.7635864115 6314155.25947778 634910.4406582525 6314160.84327138 634911.0818951458 6314161.069654489 634916.8905088181 6314158.519492032 634919.5509727153 6314157.352347771</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:PleineTerre gml:id="id876f1283-d9a7-4192-9586-6077d73688d9">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634919.8886080019 6314158.423414182 634917.2030460909 6314159.575784354 634911.5890614839 6314161.986455467 634910.857301202 6314162.163418914 634910.4177858391 6314162.167105936 634909.5182950983 6314161.282566398 634900.2514332926 6314141.440731016 634894.4807443664 6314129.083861437 634892.4462279818 6314125.433689264 634889.7238084628 6314121.352735471 634886.5233426135 6314118.399639457 634879.0170722479 6314114.701465154 634873.0379994867 6314112.4070268115 634859.830134672 6314107.340134668 634853.1912529825 6314105.344930089 634851.2292217249 6314104.384391066 634849.4117992807 6314103.27578954 634847.1081447329 6314102.318116323 634844.8598111812 6314102.240076499 634841.9792193194 6314102.411090597 634838.4656017208 6314102.978014449 634836.1269202556 6314103.681931412 634833.3079631488 6314105.3668754045 634828.5550360876 6314109.559488111 634827.7784004321 6314110.103451994 634826.5575677829 6314110.211592898 634824.8869834307 6314109.101759545 634823.0656551524 6314107.407791066 634822.187266047 6314107.610958887 634818.8345842577 6314109.739929401 634816.7400441411 6314110.49074809 634815.5210222665 6314110.695774421 634813.8068151266 6314110.221655457 634810.6548936865 6314107.220201254 634805.7697459157 6314101.593993242 634798.5020321098 6314093.222607675 634785.8569123128 6314078.656730597 634773.415562227 6314064.326900383 634761.9672775829 6314052.034641531 634746.9132406334 6314035.870632034 634743.6593010423 6314032.37753874 634743.226159522 6314031.234348362 634743.8035069973 6314030.154607506 634744.2870552669 6314029.564152531 634744.9476380352 6314028.16504025</gml:posList>
</gml:LineString>
</star:Geometrie>
</star:PleineTerre>
</gml:featureMember>
<gml:featureMember><star:Plage gml:id="id561e748b-049f-43b6-bb7a-8fe34adc7dbf">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire></star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idd35400f0-a296-4952-a070-a7e4d0b91ace"/>
<star:Coupure>false</star:Coupure>
<star:NumeroPlage>1</star:NumeroPlage>
<star:Protection>true</star:Protection>
</star:Plage>
</gml:featureMember>
<gml:featureMember><star:Plage gml:id="id52b6e261-949f-4ea1-8331-6072183bfe1c">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire></star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idd35400f0-a296-4952-a070-a7e4d0b91ace"/>
<star:Coupure>false</star:Coupure>
<star:NumeroPlage>2</star:NumeroPlage>
<star:Protection>true</star:Protection>
</star:Plage>
</gml:featureMember>
<gml:featureMember><star:Plage gml:id="ide2c8d5f2-9f07-422e-98b2-74ac8630fa75">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire></star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idd35400f0-a296-4952-a070-a7e4d0b91ace"/>
<star:Coupure>false</star:Coupure>
<star:NumeroPlage>3</star:NumeroPlage>
<star:Protection>true</star:Protection>
</star:Plage>
</gml:featureMember>
<gml:featureMember><star:Plage gml:id="id4066b565-c795-45e3-a7ce-680702e4f176">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire></star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idd35400f0-a296-4952-a070-a7e4d0b91ace"/>
<star:Coupure>false</star:Coupure>
<star:NumeroPlage>4</star:NumeroPlage>
<star:Protection>true</star:Protection>
</star:Plage>
</gml:featureMember>
<gml:featureMember><star:Plage gml:id="id847f2184-96f2-475b-9832-9f948909e5a2">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire></star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idd35400f0-a296-4952-a070-a7e4d0b91ace"/>
<star:Coupure>false</star:Coupure>
<star:NumeroPlage>5</star:NumeroPlage>
<star:Protection>true</star:Protection>
</star:Plage>
</gml:featureMember>
<gml:featureMember><star:Plage gml:id="id605baf95-b9cf-4c1c-856a-3025bc3b7094">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire></star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id8ef6ad98-29d3-4f9e-b1df-d8a1ebe81506"/>
<star:Coupure>false</star:Coupure>
<star:NumeroPlage>1</star:NumeroPlage>
<star:Protection>true</star:Protection>
</star:Plage>
</gml:featureMember>
<gml:featureMember><star:Plage gml:id="ide91f670c-66dc-4493-a6ab-92c5bb6b9bf1">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire></star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id8ef6ad98-29d3-4f9e-b1df-d8a1ebe81506"/>
<star:Coupure>false</star:Coupure>
<star:NumeroPlage>2</star:NumeroPlage>
<star:Protection>true</star:Protection>
</star:Plage>
</gml:featureMember>
<gml:featureMember><star:Plage gml:id="idf1d92668-d9ef-4050-80ff-b4af8fa823b2">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire></star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idf25dc22b-d6bf-4c49-a40c-8a36e1a1c15d"/>
<star:Coupure>false</star:Coupure>
<star:NumeroPlage>1</star:NumeroPlage>
<star:Protection>true</star:Protection>
</star:Plage>
</gml:featureMember>
<gml:featureMember><star:Plage gml:id="idc202fe3f-65af-4771-ad3e-161a102b0ef3">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire></star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idf25dc22b-d6bf-4c49-a40c-8a36e1a1c15d"/>
<star:Coupure>false</star:Coupure>
<star:NumeroPlage>2</star:NumeroPlage>
<star:Protection>true</star:Protection>
</star:Plage>
</gml:featureMember>
<gml:featureMember><star:Plage gml:id="id7cc748d8-c4f5-468c-9655-5ae95c2cd804">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>233.01.10</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idedf311aa-797c-41f1-b1d2-97461a0e4b1e"/>
<star:Coupure>false</star:Coupure>
<star:NumeroPlage>1</star:NumeroPlage>
<star:Protection>false</star:Protection>
</star:Plage>
</gml:featureMember>
<gml:featureMember><star:Plage gml:id="idb96a532e-daaa-443a-8bc7-1fb3320cfeb9">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>RRD 2 Plages233.02.10</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idd72fb309-2b9f-4f51-a7f8-f084c183c347"/>
<star:Coupure>false</star:Coupure>
<star:NumeroPlage>1</star:NumeroPlage>
<star:Protection>false</star:Protection>
</star:Plage>
</gml:featureMember>
<gml:featureMember><star:OrganeCoupureAerien gml:id="id742f7f40-3ee6-4dab-883c-efc596a93f60">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id881533b9-5f60-40b6-b78b-4e11dd0dd79a"/>
<star:PouvoirDeCoupure uom="A">100</star:PouvoirDeCoupure>
<star:Type xlink:href="Interrupteur"/>
</star:OrganeCoupureAerien>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idc927f6cb-18f5-40a4-9e85-8ecf6277f106">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634779.9159933043 6314141.600071171</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idfe8261b3-2913-45b1-b74a-bfa1bc3aa187">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634758.0803092659 6314106.760160283</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>ExtremiteReseau</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id4ee847c7-0c7a-49ab-9c4b-d19825b17103">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634823.2339019962 6314178.872094585</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id693d94b9-bf44-48eb-9495-8107563cbf29">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634791.1772793622 6314155.776968422</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Derivation</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id68fb6494-fe70-48a0-a225-08cf6bdea613">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634763.3425861232 6314190.1814261265</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>ExtremiteReseau</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="ide8c35b31-8f25-490c-96f4-7ee80b7fa694">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634757.4492785572 6314188.465673547</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Derivation</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id53ccda7d-0f3a-4327-918f-b53fb7f6d401">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634792.2046576967 6314190.403833414</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idba634d9b-8a73-40e6-9f22-db90bb1dcccc">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634762.685091922 6314181.576770812</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idf72de956-6c8d-4a3a-8c0d-36e79a41533b">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634750.2395429647 6314176.283707853</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idc29b426b-7446-46ee-b4e2-242638fd7d51">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634827.2901217566 6314182.854948837</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>ExtremiteReseau</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idd11da1ae-690a-4e5b-8513-d7eabb6d7707">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634814.4980022328 6314205.706936082</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idc2e7a464-aa52-4ba3-8deb-98368aba1796">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634820.3861589842 6314207.6425063</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>ExtremiteReseau</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id3e2c90c4-e693-46e7-9d6a-4527557cb07b">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634745.027036062 6313995.078305826</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>ExtremiteReseau</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idb2e8bddb-68ee-4ced-8dad-f71d5da3c0e2">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634690.7319517469 6314161.49757339</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>ExtremiteReseau</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id5dff1e9f-1e5c-43a6-95e5-b55ae8c4433e">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634691.5091692691 6314156.015668331</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Derivation</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id0f5d6d4a-5e18-46e8-b97a-3cdf426bddf2">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634667.7200120098 6314153.545964939</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>ExtremiteReseau</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idbdc186fc-c3e4-4e1d-a900-c680cdc0cc2a">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634721.4330837427 6314135.962951828</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id05e05c88-5af2-42c9-b1b4-bfa81713efd3">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634701.5908744029 6314184.642653914</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>ExtremiteReseau</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idc8b09f15-5dca-4d38-8577-b4fe2e156d89">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634736.1503362418 6314111.933008686</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>ExtremiteReseau</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idccd7d3c6-fc41-49b6-b3a2-487ea5c23f6a">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634725.8401662197 6314131.7462683</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id7e9eed79-4d51-467d-a567-90b4ed9b41e1">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634633.8823174933 6313975.231974365</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id57aeff01-fbc7-4be8-9556-394e7fbac484">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634572.3277166681 6313951.726969244</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id2d32513a-3739-4f7f-aa07-8f100ad40d4c">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>635077.1696491571 6314326.572231335</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>HTA</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id553c015f-4d3b-4fd8-a940-f223a4b555dd">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>635077.0990121692 6314326.249155766</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>HTA</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id287513ff-53c6-43e1-9788-bd21ddbb6277">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634744.9476380352 6314028.16504025</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>HTA</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id8720189f-6b5e-46ad-8282-48665c614b9b">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634748.2448986016 6314028.244270813</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>HTA</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id4aba5036-813e-4d23-9ccc-7c6c666e2e25">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634736.2845750108 6314030.071831309</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>HTA</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idcf2ed0e7-445c-4dfa-8e73-e634c461f7c8">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634385.0629434783 6313844.530405261</gml:pos>
</gml:Point>
</star:Geometrie>
<star:DomaineTension>HTA</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id732c19fc-4c73-405b-833a-87d5c1c269af">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="idf0c7290c-9885-461e-85f9-a960300c7035"/>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id278a7c0f-5886-4436-9165-a3f065836ac6">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id64500598-d690-4c4c-b9d9-bceb242a6f5b"/>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id49729d8f-7af5-4671-8af2-77c036feb4bb">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="idc28345c7-bc0e-4d8c-b6ce-092a7395b9b1"/>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>ExtremiteReseau</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idd274adae-6518-4ef8-a5ff-6ecb53423945">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id846e5aed-a34b-4dfd-8268-a3e54b396bb2"/>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id9d2a1aa2-3d81-4a5f-b44e-f0cb18df9eae">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id28e3a1d6-f0ba-49f4-aa41-c7a50a595583"/>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Derivation</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idb16f8d19-a879-4121-8450-739c04dac8f4">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="idef78bb67-55f4-4408-ae54-aadb198e8d7a"/>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Derivation</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id977ba275-9e6e-4da9-a25b-794536ee1df7">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id83a6bdc4-8bdd-4b9c-a627-30cd851ddcc6"/>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>ExtremiteReseau</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id7a5f04f7-1fac-46e0-8250-13a82d0ca1b4">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="ida6fc77ab-a7c3-4f34-ab36-98978cbe95e7"/>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>ExtremiteReseau</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idca898da2-edfe-4e8e-9448-9520e0c1e07d">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id9c10cd18-bddd-4775-b6af-5460d2486d71"/>
<star:DomaineTension>BT</star:DomaineTension>
<star:TypeJonction>Derivation</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idff7e3e76-6b1f-4dc2-951f-227a1aecd24a">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="ida2849593-4e3b-4905-9e77-3ff82c8b9198"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:TypeJonction>RemonteeAeroSouterraine</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idce49d753-970f-41de-886b-472d741daec2">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id0535c79e-043a-457d-8c1f-b1d0bbd98705"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:TypeJonction>Derivation</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idf148f290-fda8-4459-b5a3-9cce93a5a102">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id1bfee6be-544f-4656-ab53-2abf9aed4c53"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:TypeJonction>RemonteeAeroSouterraine</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id40976a3a-b495-4677-b3d6-7a6a5fd1521a">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id1a2a53c3-05f8-4d27-9a52-82cb4d1c1693"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id52b71919-91d7-4d64-905b-852b9a064e2e">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id80a45a22-0927-4c8d-a92a-7259c2586614"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="idea4a9295-cc95-4300-93dd-1ae9b2f701d0">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id70047720-e8be-4524-8617-23e6fbfc327e"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:TypeJonction>Derivation</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:Jonction gml:id="id976bd13c-218b-4da4-bacc-ae62af4a8494">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id881533b9-5f60-40b6-b78b-4e11dd0dd79a"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:TypeJonction>Jonction</star:TypeJonction>
</star:Jonction>
</gml:featureMember>
<gml:featureMember><star:JeuBarres gml:id="idedf311aa-797c-41f1-b1d2-97461a0e4b1e">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id0d1e1686-6a3f-4079-82dd-d219d0c4f253"/>
</star:JeuBarres>
</gml:featureMember>
<gml:featureMember><star:JeuBarres gml:id="idd72fb309-2b9f-4f51-a7f8-f084c183c347">
<star:reseau xlink:href="Reseau"/>
<star:Statut>Functional</star:Statut>
<star:conteneur xlink:href="id0a600134-c92f-4caf-b2e8-23ffdb6d2645"/>
</star:JeuBarres>
</gml:featureMember>
<gml:featureMember><star:GeometrieSupplementaire gml:id="idcaa8adb8-536a-4899-825c-984cadcb60c1">
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Surface2D>
<gml:Surface srsName="EPSG:2154" srsDimension="2">
<gml:patches>
<gml:PolygonPatch>
<gml:exterior>
<gml:LinearRing>
<gml:posList>634311.788785655 6313937.117007521 634312.8668579349 6313938.450587155 634313.0223919823 6313938.324852673 634311.9443197025 6313936.991273039 634311.788785655 6313937.117007521</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:PolygonPatch>
</gml:patches>
</gml:Surface>
</star:Surface2D>
</star:GeometrieSupplementaire>
</gml:featureMember>
<gml:featureMember><star:GeometrieSupplementaire gml:id="id9fc6ff3d-5ffd-487e-9292-e204d8272386">
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Surface2D>
<gml:Surface srsName="EPSG:2154" srsDimension="2">
<gml:patches>
<gml:PolygonPatch>
<gml:exterior>
<gml:LinearRing>
<gml:posList>634759.6611256278 6314020.85398401 634761.6024174234 6314021.366157063 634760.2717054063 6314022.190486117 634759.6611256278 6314020.85398401</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:PolygonPatch>
</gml:patches>
</gml:Surface>
</star:Surface2D>
</star:GeometrieSupplementaire>
</gml:featureMember>
<gml:featureMember><star:GeometrieSupplementaire gml:id="id701a3f21-930c-4f5f-a470-67bd9015163e">
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Surface2D>
<gml:Surface srsName="EPSG:2154" srsDimension="2">
<gml:patches>
<gml:PolygonPatch>
<gml:exterior>
<gml:LinearRing>
<gml:posList>634882.796692836 6314312.552848425 634882.8131834401 6314312.017259105 634885.3187776873 6314312.873340836 634882.796692836 6314312.552848425</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:PolygonPatch>
</gml:patches>
</gml:Surface>
</star:Surface2D>
</star:GeometrieSupplementaire>
</gml:featureMember>
<gml:featureMember><star:GeometrieSupplementaire gml:id="iddee4f946-9c09-4706-9fc6-e539cb97c4dc">
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Surface2D>
<gml:Surface srsName="EPSG:2154" srsDimension="2">
<gml:patches>
<gml:PolygonPatch>
<gml:exterior>
<gml:LinearRing>
<gml:posList>634612.7219712172 6314212.200001211 634612.9993315156 6314210.727182417 634612.8027863096 6314210.690169149 634612.5254260112 6314212.162987943 634612.7219712172 6314212.200001211</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:PolygonPatch>
</gml:patches>
</gml:Surface>
</star:Surface2D>
</star:GeometrieSupplementaire>
</gml:featureMember>
<gml:featureMember><star:GeometrieSupplementaire gml:id="id3d1c74c6-223f-4e70-a4de-fd009628b33f">
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Surface2D>
<gml:Surface srsName="EPSG:2154" srsDimension="2">
<gml:patches>
<gml:PolygonPatch>
<gml:exterior>
<gml:LinearRing>
<gml:posList>634919.5509727153 6314157.352347771 634921.4351762872 6314158.562284553 634919.8886080019 6314158.423414182 634919.5509727153 6314157.352347771</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:PolygonPatch>
</gml:patches>
</gml:Surface>
</star:Surface2D>
</star:GeometrieSupplementaire>
</gml:featureMember>
<gml:featureMember><star:GeometrieSupplementaire gml:id="ida045fd9b-5e82-44d7-8eca-40402a6ba1b0">
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Surface2D>
<gml:Surface srsName="EPSG:2154" srsDimension="2">
<gml:patches>
<gml:PolygonPatch>
<gml:exterior>
<gml:LinearRing>
<gml:posList>634820.4563266367 6314102.7554734675 634823.105126144 6314103.176798433 634821.0775059034 6314104.880078438 634820.4563266367 6314102.7554734675</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:PolygonPatch>
</gml:patches>
</gml:Surface>
</star:Surface2D>
</star:GeometrieSupplementaire>
</gml:featureMember>
<gml:featureMember><star:GeometrieSupplementaire gml:id="id95109930-892f-4670-8514-028e41ec6c0c">
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Surface2D>
<gml:Surface srsName="EPSG:2154" srsDimension="2">
<gml:patches>
<gml:PolygonPatch>
<gml:exterior>
<gml:LinearRing>
<gml:posList>634753.7776118946 6314178.515708764 634755.9493778658 6314176.728304613 634756.637195532 6314179.294896901 634755.9644957926 6314179.486349459 634755.475068977 6314179.494451027 634755.0168452603 6314179.41138406 634753.7776118946 6314178.515708764</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:PolygonPatch>
</gml:patches>
</gml:Surface>
</star:Surface2D>
</star:GeometrieSupplementaire>
</gml:featureMember>
<gml:featureMember><star:GeometrieSupplementaire gml:id="id3ad94eb1-5ea6-47c6-bd79-1bc4b6f1beed">
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Surface2D>
<gml:Surface srsName="EPSG:2154" srsDimension="2">
<gml:patches>
<gml:PolygonPatch>
<gml:exterior>
<gml:LinearRing>
<gml:posList>634742.8034005335 6314023.223114291 634745.5541663698 6314022.818430231 634746.5237296319 6314024.793263202 634743.4919951174 6314024.213316437 634743.0311629407 6314023.699712921 634742.8034005335 6314023.223114291</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:PolygonPatch>
</gml:patches>
</gml:Surface>
</star:Surface2D>
</star:GeometrieSupplementaire>
</gml:featureMember>
<gml:featureMember><star:GeometrieSupplementaire gml:id="idc8570190-77f4-415c-95b8-a19ed6f296bf">
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Surface2D>
<gml:Surface srsName="EPSG:2154" srsDimension="2">
<gml:patches>
<gml:PolygonPatch>
<gml:exterior>
<gml:LinearRing>
<gml:posList>634870.3757031409 6314101.872539405 634870.6837434117 6314101.7960311165 634870.6355339265 6314101.601928449 634870.3274936557 6314101.6784367375 634870.3757031409 6314101.872539405</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:PolygonPatch>
</gml:patches>
</gml:Surface>
</star:Surface2D>
</star:GeometrieSupplementaire>
</gml:featureMember>
<gml:featureMember><star:GeometrieSupplementaire gml:id="idbcb4e297-d64c-49bd-a400-202829bd90d9">
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Surface2D>
<gml:Surface srsName="EPSG:2154" srsDimension="2">
<gml:patches>
<gml:PolygonPatch>
<gml:exterior>
<gml:LinearRing>
<gml:posList>634828.9867351683 6314112.025345685 634829.5435955655 6314112.55412726 634829.6813131743 6314112.409096711 634829.1244527771 6314111.880315136 634828.9867351683 6314112.025345685</gml:posList>
</gml:LinearRing>
</gml:exterior>
</gml:PolygonPatch>
</gml:patches>
</gml:Surface>
</star:Surface2D>
</star:GeometrieSupplementaire>
</gml:featureMember>
<gml:featureMember><star:Coffret gml:id="id0d1e1686-6a3f-4079-82dd-d219d0c4f253">
<star:reseau xlink:href="Reseau"/>
<star:Code>2576112010</star:Code>
<star:geometriesupplementaire xlink:href="idbcb4e297-d64c-49bd-a400-202829bd90d9"/>
<star:PositionVerticale>OnGroundSurface</star:PositionVerticale>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634829.6124543699 6314112.481611986</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Coffret>
</gml:featureMember>
<gml:featureMember><star:Coffret gml:id="id0a600134-c92f-4caf-b2e8-23ffdb6d2645">
<star:reseau xlink:href="Reseau"/>
<star:Code>2576112195</star:Code>
<star:geometriesupplementaire xlink:href="idc8570190-77f4-415c-95b8-a19ed6f296bf"/>
<star:PositionVerticale>OnGroundSurface</star:PositionVerticale>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634870.6596386691 6314101.698979783</gml:pos>
</gml:Point>
</star:Geometrie>
</star:Coffret>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id6a2566fd-366b-4ceb-b144-01605c3b178b">
<star:cheminement xlink:href="id10176432-3d76-4d00-ad22-7f787e4b0d1b"/>
<star:cables xlink:href="id7da65535-8c03-4456-9532-4c9ad428c7a8"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id20b1ee23-acea-413d-8a1f-7d2a54078cc2">
<star:cheminement xlink:href="idef740676-cb1a-4e05-834a-41ab9d708505"/>
<star:cables xlink:href="idc3982db6-118b-4114-8bfc-97ab27febdba"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="idd69f6bb8-5626-4380-81a9-b8560f0075ce">
<star:cheminement xlink:href="id8eac80bd-2551-4169-97d0-3e38cf8a08ef"/>
<star:cables xlink:href="ida4e0a972-f6f9-4ad2-b586-4c1d284e4031"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id3acbc8cc-139c-44fe-bfc8-88b68eaba6c6">
<star:cheminement xlink:href="id8c982388-c8b7-428e-914c-326a42d88c04"/>
<star:cables xlink:href="id2ba97ef0-7c0b-4fb3-af2a-dafce5a520db"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id1a8e1e7a-5dd9-46fc-bf3e-5595df16d604">
<star:cheminement xlink:href="id139d22fa-7b9b-4f8b-bbd5-aa74d8fd33a5"/>
<star:cables xlink:href="id6c6429ff-b71b-4a5e-ba08-51f5d63d8516"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="ide4a81fc5-984d-4ad7-9550-860d5f99a20c">
<star:cheminement xlink:href="id794f4595-b880-4def-8a4b-8da8a625c877"/>
<star:cables xlink:href="id8343e111-c2be-4d17-94c9-dc668f203370"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id20ed30e4-66a6-48c6-aadb-9394411c4f4a">
<star:cheminement xlink:href="id3094fcf7-4bfd-45a3-b333-1bd9d4e6379a"/>
<star:cables xlink:href="ide91168b0-2479-45ae-bfbc-f46da004b75b"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id142c4bba-22f5-456e-920f-028ecc1c5486">
<star:cheminement xlink:href="id2c630eb5-6121-4ade-8c6a-9b8b9d2088e7"/>
<star:cables xlink:href="idc346a984-4743-42ae-89ac-0b0355b5dbcd"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id36bfe165-42bf-4812-aa9e-ca9288d0fa20">
<star:cheminement xlink:href="idefa908a0-805e-45e6-98a4-d8678ac40b9c"/>
<star:cables xlink:href="id885f133e-09fd-4c82-ac87-6e6ec840a864"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id0c810c39-f45c-4f29-ab9f-3d2b8c2c6f41">
<star:cheminement xlink:href="idd2e8343c-4e9c-45d5-93da-d1b2b40189d1"/>
<star:cables xlink:href="idd7f392c4-9bfc-4cf0-a4ac-8affbe677fb3"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id5f8d55a7-a6b9-4181-b10b-a3ddba657eb1">
<star:cheminement xlink:href="idb02f31e8-dec5-4d80-8784-9b9213f355a6"/>
<star:cables xlink:href="id745f7f12-e39c-43d6-a739-19ed7e8086ac"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id76a8f658-d07a-4355-8067-156fb7929cd3">
<star:cheminement xlink:href="id7d1ae235-0e72-480c-9dc1-9f879c9cf2cc"/>
<star:cables xlink:href="iddd99ef8b-ad50-409b-b842-6b3bbf089e4d"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="idcf62fb26-5bc9-4685-a5cf-84eb07f067cf">
<star:cheminement xlink:href="id779203f2-f5ab-454e-b6f0-a974f3ad0da8"/>
<star:cables xlink:href="idceb44bbc-b96c-475a-851b-03fcb85a1cbb"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id90d486fd-b4da-4cb5-b3ec-0927d4dbdf7b">
<star:cheminement xlink:href="id1c9edea4-b327-4512-8c28-b05297cc0e52"/>
<star:cables xlink:href="idf2a05922-b273-4709-afc2-903d19621f26"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id880d46ee-9132-4546-8f67-311f42535c2f">
<star:cheminement xlink:href="idd9111e79-6fef-406f-988f-acfd3bce294a"/>
<star:cables xlink:href="id9125ba6d-3404-4f39-a297-8ad7eda50935"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id6e58b237-0652-4a79-bd38-34419638b803">
<star:cheminement xlink:href="id7bdbb082-55d2-44eb-8f5d-ff0d5f8d0796"/>
<star:cables xlink:href="id7eed3b6c-40af-4e85-8f5b-1a126285ceb9"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="ide9f55ae2-4dbf-4064-a68f-f37ec6dbda34">
<star:cheminement xlink:href="id30dbb4c8-33b8-4a1c-ac50-acbecb20eb39"/>
<star:cables xlink:href="idf7abda61-8217-4322-b58f-30a29f6a80dc"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id63c37a0c-06e6-4829-a633-2b6a5bf8a73f">
<star:cheminement xlink:href="idd88abc5c-9d71-4a8b-9914-b6e8dd083001"/>
<star:cables xlink:href="id5835a601-0200-425b-87a6-c5e1bd7c3096"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="idb54a72c7-e0cc-47bd-a7dc-de9c89c9bcaf">
<star:cheminement xlink:href="id7d5f8f1c-d37a-4ad2-8cea-e634abadbcc0"/>
<star:cables xlink:href="idefe041fc-cc55-4ef3-895f-9af4718a8d42"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="idfe63b959-a685-4cd7-b895-b2a5b9379b99">
<star:cheminement xlink:href="id335f3c01-31a3-4187-aea0-540b8dd83b13"/>
<star:cables xlink:href="idf1e7c898-f2a3-428b-9e85-c572b6d4c5ed"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="ide2125d2f-f7ab-4ffd-b72c-fba6624cb0e6">
<star:cheminement xlink:href="idabc630ce-4eea-4d8c-9828-462de6ccea5b"/>
<star:cables xlink:href="idc4b3419a-56c2-49fd-b23b-8c14c5064279"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id91602cbd-645f-4b90-b220-669feee9a5f0">
<star:cheminement xlink:href="idcdbb5b6b-7922-4f14-80e1-7ca11c54f215"/>
<star:cables xlink:href="id0581a5e6-ce91-4030-b2b1-6de1536a720a"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id2fe18293-d4c0-4816-a4a6-4527c29973c0">
<star:cheminement xlink:href="id9c0fe002-eb6b-4f24-a0bb-18ff69a362a2"/>
<star:cables xlink:href="id5d012944-f040-4ef9-87ab-15dcb3332536"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="idf6a83766-3e76-4174-b3f5-640dc2321323">
<star:cheminement xlink:href="id56363a28-2c8f-4d55-951e-1ee54fdb79d6"/>
<star:cables xlink:href="id506e0ad9-77c3-46c5-aa9e-2f04ca553a52"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id7aee26de-d2cd-498f-9374-638beadab577">
<star:cheminement xlink:href="iddbedb5eb-8fb0-4eae-b265-5000ba4462d2"/>
<star:cables xlink:href="idbe7d6f6a-8181-4bf0-9000-f7165ca38d9f"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="idc24e7e35-6e39-4e71-ae56-d6829b92c2c9">
<star:cheminement xlink:href="idb4ebb6bf-2a3e-4ab6-a218-aa9feab8cbe5"/>
<star:cables xlink:href="idf300936f-c181-423e-9b86-2e173555596f"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="ide69f34ea-c671-4a76-a19d-49da6f6c3bcd">
<star:cheminement xlink:href="id3e8bc62f-ff16-4393-9036-f2b97b78e64b"/>
<star:cables xlink:href="idf344e40a-0644-4ce0-9ee5-706e11328bd2"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id9de4159e-21cc-4482-a097-520c34116e50">
<star:cheminement xlink:href="id52db09b6-42eb-4cb4-9b16-d14e5f1f7f47"/>
<star:cables xlink:href="idfc1de54f-fb76-4f7c-8510-766c30f084b0"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id3b26f675-e049-43da-89f6-6ac0f7778581">
<star:cheminement xlink:href="idacf8d136-e995-43e1-b76c-d1e001dce96a"/>
<star:cables xlink:href="ida81828d3-aafb-405b-9184-e5165c21ccd0"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="idcdbb43a4-aca6-42fe-bba9-014b0f6b8e83">
<star:cheminement xlink:href="id4dfe735f-fdfe-4cb8-851c-f63972a4f9f7"/>
<star:cables xlink:href="iddec68f48-f836-425e-91c2-1e64ad71f5e1"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id254ff52b-a54a-48bf-93db-0595aaf165c0">
<star:cheminement xlink:href="idd7235fa4-c292-414c-ada7-c343c775375d"/>
<star:cables xlink:href="id7d3b89c3-8c75-4419-bfca-650bf5506de7"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id8f70e2c7-408a-4c6d-9d87-3521df8626e1">
<star:cheminement xlink:href="id268a5bef-b5e0-423c-9250-567599332763"/>
<star:cables xlink:href="ide2489a74-2779-439e-988e-b2af513a42e9"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="idea6b89a5-d9f4-43cc-9ceb-cb9383b1b0df">
<star:cheminement xlink:href="id73e727f9-4ccc-48c2-83f3-9680c9cfd410"/>
<star:cables xlink:href="id06b4e677-62e6-4830-91a3-ee0c3b1f774a"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="idde470f9b-2194-46b1-a628-a158b336b935">
<star:cheminement xlink:href="id4c204eba-a29f-45eb-8246-40bef9f5b18e"/>
<star:cables xlink:href="id98d9fd8d-9830-40c6-8086-917d2f3788fc"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="idd6476175-c944-4670-ab98-240b5f816b3b">
<star:cheminement xlink:href="id18db566d-42bd-48e6-9924-e0a1776d5339"/>
<star:cables xlink:href="idcf217558-23c6-495b-bf69-f90f2086d99b"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id79ba310b-6a38-4b59-b6c9-ffe4630eb5b1">
<star:cheminement xlink:href="ided89ea9f-f193-444f-a59b-be65a09d2620"/>
<star:cables xlink:href="id6de0ca3b-1ef5-486e-bfe6-83dc8bc0eba6"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id6e156f99-2635-49fe-9728-2aa7df1ab56e">
<star:cheminement xlink:href="ida1a8babe-5028-4916-a487-c65fbd155874"/>
<star:cables xlink:href="id5b57139d-6fb1-4981-a759-3368dc28948b"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id02e39dff-432c-47a6-a668-dc1172f39dac">
<star:cheminement xlink:href="id08abc826-cebc-42df-816c-08b6c2232686"/>
<star:cables xlink:href="idd7ec38a9-34ac-43dc-962b-7ebdec10d289"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="idc6511de6-18c6-4c1d-94b8-62d1c3dc0e11">
<star:cheminement xlink:href="idda68f735-4d55-4a20-8ea3-f0fc75ce6a98"/>
<star:cables xlink:href="id9a129487-70d0-4212-809a-6e190e9bfbed"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="idb5ca69fe-a251-4238-9944-945d229c6b3e">
<star:cheminement xlink:href="ideb903d38-ea13-4408-9c47-785890bf63da"/>
<star:cables xlink:href="idb92d5f59-13d3-492e-ab64-ca13888ea0eb"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id75ba9842-e4a0-46ce-b360-46b39d7e04fa">
<star:cheminement xlink:href="id9ad58fbf-ca23-4fb6-8000-8be2544ff5fb"/>
<star:cables xlink:href="id0e002480-f216-4534-8745-773c257325db"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id3ae4deff-c166-48c5-81e8-c71a0a00867a">
<star:cheminement xlink:href="id33252433-1d13-42ef-9ae6-4183b7ad2422"/>
<star:cables xlink:href="id3b9350f5-132c-4c1f-8b94-7fff86f2d2ad"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id0bee9035-bd8c-45ea-b5d5-c2bf1fc72636">
<star:cheminement xlink:href="id40498fb3-7baa-41a3-a51d-19bcf2c9f620"/>
<star:cables xlink:href="id20c3aeab-eddd-4822-89a1-7e7804ae6f4f"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="idc84ff9ee-9927-46b8-bfbd-fd27bff76444">
<star:cheminement xlink:href="id4c52668a-04f8-431d-a770-48d972fa2ba9"/>
<star:cables xlink:href="idbaa10fb9-c518-4e68-81de-e7829b5caa98"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id10acf0f5-2de4-48ec-ba70-4acfe4ad2953">
<star:cheminement xlink:href="idc12b7ecd-4a14-45f8-b906-85a50cb931c8"/>
<star:cables xlink:href="idb0cf8723-ec14-4e2f-9760-36aef558d1a0"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id9d1daee0-54e4-44e7-b95d-15bc7e864056">
<star:cheminement xlink:href="id4c3f6a8d-2d3a-48b9-b9fa-8a32c0b9c14a"/>
<star:cables xlink:href="id6554c1b8-7bc0-4e01-a794-5f9abfd3ca2d"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="ide835daba-8227-496c-8f13-b6ccf4ef8533">
<star:cheminement xlink:href="idf65cfb2c-95bf-4579-b1c4-f2e5414b6e63"/>
<star:cables xlink:href="id2a7ce2d8-ab9f-48bb-b6ad-53571a6efba7"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:Cheminement_Cables gml:id="id3e2a5caa-c868-430d-adac-2169ba48b695">
<star:cheminement xlink:href="id876f1283-d9a7-4192-9586-6077d73688d9"/>
<star:cables xlink:href="idea9105df-786b-4cfc-a048-53cccf5bb03a"/>
</star:Cheminement_Cables>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="idf9ebdd97-29c5-4b19-9727-54d06c3bd47e">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>4</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idc1a982ce-14f5-41eb-a30d-106f8ffbbfcc"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom></star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="idcc1db0c2-0d7b-429e-81c5-b41e0a5959e6">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>1</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idc1a982ce-14f5-41eb-a30d-106f8ffbbfcc"/>
<star:Fonction xlink:href="Liaison Transfo"/>
<star:Nom>Liaison Transfo</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="idffbbfd8d-2341-4878-9cd7-ba0f0b2e1614">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>2</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idc1a982ce-14f5-41eb-a30d-106f8ffbbfcc"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom>VERS CALIPORC</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="idb249048e-117c-427b-8291-ae04d6fefdc2">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>3</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idc1a982ce-14f5-41eb-a30d-106f8ffbbfcc"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom>VERS MOLINIER</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="idde61932a-92ca-43d7-9c9d-3f1c23ff9fba">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>1</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id2ee3be85-58f3-4876-8a0b-fb5a6a0f4975"/>
<star:Fonction xlink:href="Liaison Transfo"/>
<star:Nom>Protection Transfo</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="idb294d246-b5c0-4511-88a0-af8260a23b78">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>2</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id2ee3be85-58f3-4876-8a0b-fb5a6a0f4975"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom>VERS AUROCK</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="ide8d566e9-d0f1-48cb-b992-b2183434d4d7">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>3</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id2ee3be85-58f3-4876-8a0b-fb5a6a0f4975"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom>VERS INNOPROD 3</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="ide199d024-0681-43aa-9b3c-c52d0a6bad63">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>4</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id2ee3be85-58f3-4876-8a0b-fb5a6a0f4975"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom></star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="id9bed6f2d-967c-4b6b-84d1-2bdab4df9c56">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>9</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idc2bffd87-7abd-47e5-8b0d-b2be597037de"/>
<star:Fonction xlink:href="Liaison Transfo"/>
<star:Nom>P.TRANSFO</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="id6169432d-d1ea-4d9c-8401-d755a5a78273">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>1</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idc2bffd87-7abd-47e5-8b0d-b2be597037de"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom>81004P0233</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="id01abd778-d49f-4864-ad41-b71b27af8ed0">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>2</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idc2bffd87-7abd-47e5-8b0d-b2be597037de"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom>81004P0080</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="id03945f17-0009-4f67-961e-18aa3be8bb52">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>3</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idc2bffd87-7abd-47e5-8b0d-b2be597037de"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom></star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="id671d818e-c527-4804-b2dd-cbed6730a049">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>9</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id159dbfbd-2a22-459f-b152-ef526d5d349b"/>
<star:Fonction xlink:href="Liaison Transfo"/>
<star:Nom>P.TRANSFO</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="id631aaa54-4ac1-453b-90b9-e9c5a7e841ee">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>1</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id159dbfbd-2a22-459f-b152-ef526d5d349b"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom>81004P6132</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="id62fb7756-0939-4a30-9b01-f0129fd908da">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>2</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id159dbfbd-2a22-459f-b152-ef526d5d349b"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom>81004P0249</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="id884ae871-989d-44ea-acd1-cf1db340197f">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>1</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idf5a4c4d4-769f-4a7b-83f1-bb32f346d1d6"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom>VERS ENEDIS DR NMP P4000</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="id56001491-4ce8-488b-9531-daf1526e904f">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>3</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idf5a4c4d4-769f-4a7b-83f1-bb32f346d1d6"/>
<star:Fonction xlink:href="Liaison Transfo"/>
<star:Nom>Protection Transfo</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="id511d93ad-664a-4908-a1a1-2ba9682b876f">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>2</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="idf5a4c4d4-769f-4a7b-83f1-bb32f346d1d6"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom>VERS AUROCK P0083</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="id22f03517-6048-43b2-a8aa-1002c288e4fd">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>3</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id1381bb36-6394-4808-b119-60588cb4e815"/>
<star:Fonction xlink:href="Client HTA"/>
<star:Nom>Client HTA</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="ide497d0b8-986f-4378-82f0-38feddd57944">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>2</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id1381bb36-6394-4808-b119-60588cb4e815"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom>VERS INNOPROD 2</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="idfc9d0296-99a0-4376-a576-a3847cde9c92">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>1</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id1381bb36-6394-4808-b119-60588cb4e815"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom>VERS INNOPROD 4</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="id0cbf7c66-3adc-4322-a25e-66130174341b">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>2</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id57d00150-1c9a-4008-8d0b-e411d7a14c6d"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom>VERS INOPROD1</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="id51de86d1-0838-44c8-a53a-53627f6d88ae">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>1</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id57d00150-1c9a-4008-8d0b-e411d7a14c6d"/>
<star:Fonction xlink:href="Liaison Transfo"/>
<star:Nom>Protection Transfo</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CelluleHTA gml:id="ida6b3ebac-91b6-45a5-b997-afab7d526f17">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>3</star:Commentaire>
<star:Statut>Functional</star:Statut>
<star:noeudParent xlink:href="id57d00150-1c9a-4008-8d0b-e411d7a14c6d"/>
<star:Fonction xlink:href="ArriveeHTA"/>
<star:Nom>VERS INOPROD 4</star:Nom>
</star:CelluleHTA>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idddb56226-62e3-480b-ad57-693395369716">
<star:cableelectrique xlink:href="id7da65535-8c03-4456-9532-4c9ad428c7a8"/>
<star:noeudreseau xlink:href="id278a7c0f-5886-4436-9165-a3f065836ac6"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idba09c4bb-b622-4714-8b0e-951a85ab15b5">
<star:cableelectrique xlink:href="id7da65535-8c03-4456-9532-4c9ad428c7a8"/>
<star:noeudreseau xlink:href="idca898da2-edfe-4e8e-9448-9520e0c1e07d"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id4ab949ea-26fb-48de-a691-b78bdfccce80">
<star:cableelectrique xlink:href="idc3982db6-118b-4114-8bfc-97ab27febdba"/>
<star:noeudreseau xlink:href="id9d2a1aa2-3d81-4a5f-b44e-f0cb18df9eae"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id7362cf91-6efd-4393-a331-c8b9b25e8e31">
<star:cableelectrique xlink:href="idc3982db6-118b-4114-8bfc-97ab27febdba"/>
<star:noeudreseau xlink:href="idd274adae-6518-4ef8-a5ff-6ecb53423945"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id866e325f-caef-46ff-b37b-2e3b6f4ef63c">
<star:cableelectrique xlink:href="ida4e0a972-f6f9-4ad2-b586-4c1d284e4031"/>
<star:noeudreseau xlink:href="id49729d8f-7af5-4671-8af2-77c036feb4bb"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idd156e652-4c10-4970-8ab1-401c59430fd4">
<star:cableelectrique xlink:href="ida4e0a972-f6f9-4ad2-b586-4c1d284e4031"/>
<star:noeudreseau xlink:href="idd274adae-6518-4ef8-a5ff-6ecb53423945"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id8dbe484e-f28a-4bbf-a111-b22929a0925b">
<star:cableelectrique xlink:href="id2ba97ef0-7c0b-4fb3-af2a-dafce5a520db"/>
<star:noeudreseau xlink:href="id732c19fc-4c73-405b-833a-87d5c1c269af"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id23549d9e-a43b-4dda-9a62-ee31b51d1218">
<star:cableelectrique xlink:href="id2ba97ef0-7c0b-4fb3-af2a-dafce5a520db"/>
<star:noeudreseau xlink:href="id278a7c0f-5886-4436-9165-a3f065836ac6"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id7a4c85c0-6d89-444e-9f90-551e714b8f6b">
<star:cableelectrique xlink:href="id6c6429ff-b71b-4a5e-ba08-51f5d63d8516"/>
<star:noeudreseau xlink:href="idb16f8d19-a879-4121-8450-739c04dac8f4"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="ide06df809-5537-4100-883e-61590c4fd3d8">
<star:cableelectrique xlink:href="id6c6429ff-b71b-4a5e-ba08-51f5d63d8516"/>
<star:noeudreseau xlink:href="id977ba275-9e6e-4da9-a25b-794536ee1df7"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id1123749a-838e-4058-a6db-bc849c16f1ab">
<star:cableelectrique xlink:href="id8343e111-c2be-4d17-94c9-dc668f203370"/>
<star:noeudreseau xlink:href="id7a5f04f7-1fac-46e0-8250-13a82d0ca1b4"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id01e61107-fb93-4dc5-9dd4-131f8bb48dbf">
<star:cableelectrique xlink:href="id8343e111-c2be-4d17-94c9-dc668f203370"/>
<star:noeudreseau xlink:href="id732c19fc-4c73-405b-833a-87d5c1c269af"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id8c7e1e94-cdd6-490a-9c0b-93b19178fa70">
<star:cableelectrique xlink:href="ide91168b0-2479-45ae-bfbc-f46da004b75b"/>
<star:noeudreseau xlink:href="idce49d753-970f-41de-886b-472d741daec2"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id52e040a6-12cd-4f93-b366-3a6386f6f259">
<star:cableelectrique xlink:href="ide91168b0-2479-45ae-bfbc-f46da004b75b"/>
<star:noeudreseau xlink:href="idea4a9295-cc95-4300-93dd-1ae9b2f701d0"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idd57446cc-5419-4ea7-bf72-cd62b066b70e">
<star:cableelectrique xlink:href="idc346a984-4743-42ae-89ac-0b0355b5dbcd"/>
<star:noeudreseau xlink:href="id742f7f40-3ee6-4dab-883c-efc596a93f60"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id998508e3-01a7-48fa-9c8c-56d83815e910">
<star:cableelectrique xlink:href="idc346a984-4743-42ae-89ac-0b0355b5dbcd"/>
<star:noeudreseau xlink:href="id976bd13c-218b-4da4-bacc-ae62af4a8494"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id731db273-2ea7-43cd-b1fc-a53a639cedc4">
<star:cableelectrique xlink:href="idc346a984-4743-42ae-89ac-0b0355b5dbcd"/>
<star:noeudreseau xlink:href="idce49d753-970f-41de-886b-472d741daec2"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id3107fd07-f7b1-4dc0-9694-bb75205cc1b4">
<star:cableelectrique xlink:href="id885f133e-09fd-4c82-ac87-6e6ec840a864"/>
<star:noeudreseau xlink:href="id40976a3a-b495-4677-b3d6-7a6a5fd1521a"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id0f0c9779-398d-49e5-97c7-f7a44a4b373f">
<star:cableelectrique xlink:href="id885f133e-09fd-4c82-ac87-6e6ec840a864"/>
<star:noeudreseau xlink:href="id52b71919-91d7-4d64-905b-852b9a064e2e"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id29a28500-bcc2-43c8-930a-7041b8d6fc1b">
<star:cableelectrique xlink:href="idd7f392c4-9bfc-4cf0-a4ac-8affbe677fb3"/>
<star:noeudreseau xlink:href="id742f7f40-3ee6-4dab-883c-efc596a93f60"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idf77b91b3-62c2-4e83-b998-b676da499a4b">
<star:cableelectrique xlink:href="idd7f392c4-9bfc-4cf0-a4ac-8affbe677fb3"/>
<star:noeudreseau xlink:href="id976bd13c-218b-4da4-bacc-ae62af4a8494"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id248b9cc1-6964-45cb-bddb-eac2df774952">
<star:cableelectrique xlink:href="idd7f392c4-9bfc-4cf0-a4ac-8affbe677fb3"/>
<star:noeudreseau xlink:href="id8d55c38d-6059-475a-8b1a-dfeca52fd01e"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id06f83e59-67c9-452f-8a47-3ba724a1a1c7">
<star:cableelectrique xlink:href="id745f7f12-e39c-43d6-a739-19ed7e8086ac"/>
<star:noeudreseau xlink:href="idff7e3e76-6b1f-4dc2-951f-227a1aecd24a"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idce339696-98a4-4214-b4e8-3db93877bfb4">
<star:cableelectrique xlink:href="id745f7f12-e39c-43d6-a739-19ed7e8086ac"/>
<star:noeudreseau xlink:href="idce49d753-970f-41de-886b-472d741daec2"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idc4ebb7a9-f201-4137-a6f5-046a9ef780d7">
<star:cableelectrique xlink:href="iddd99ef8b-ad50-409b-b842-6b3bbf089e4d"/>
<star:noeudreseau xlink:href="idf148f290-fda8-4459-b5a3-9cce93a5a102"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id9a97fc94-b191-4362-af18-f526aab0ef25">
<star:cableelectrique xlink:href="iddd99ef8b-ad50-409b-b842-6b3bbf089e4d"/>
<star:noeudreseau xlink:href="id40976a3a-b495-4677-b3d6-7a6a5fd1521a"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idd5c96cba-998a-482e-b725-5ae30fbb4c7e">
<star:cableelectrique xlink:href="idceb44bbc-b96c-475a-851b-03fcb85a1cbb"/>
<star:noeudreseau xlink:href="idf1d92668-d9ef-4050-80ff-b4af8fa823b2"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="ide0e5d972-62b8-4a2f-b144-b47851d9ff96">
<star:cableelectrique xlink:href="idceb44bbc-b96c-475a-851b-03fcb85a1cbb"/>
<star:noeudreseau xlink:href="id7e9eed79-4d51-467d-a567-90b4ed9b41e1"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id937281fb-c1ad-44c8-a1fd-1d53a6991a69">
<star:cableelectrique xlink:href="idf2a05922-b273-4709-afc2-903d19621f26"/>
<star:noeudreseau xlink:href="id57aeff01-fbc7-4be8-9556-394e7fbac484"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id65013891-58f7-4e4b-b976-4a61479a059b">
<star:cableelectrique xlink:href="idf2a05922-b273-4709-afc2-903d19621f26"/>
<star:noeudreseau xlink:href="id7e9eed79-4d51-467d-a567-90b4ed9b41e1"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id91265415-f2d2-421c-9f93-43a098eb34d4">
<star:cableelectrique xlink:href="id9125ba6d-3404-4f39-a297-8ad7eda50935"/>
<star:noeudreseau xlink:href="idc202fe3f-65af-4771-ad3e-161a102b0ef3"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id358d3605-8154-4d23-ab88-b9625eec3dee">
<star:cableelectrique xlink:href="id9125ba6d-3404-4f39-a297-8ad7eda50935"/>
<star:noeudreseau xlink:href="id3e2c90c4-e693-46e7-9d6a-4527557cb07b"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="ide051699e-9112-4785-89f3-916c90d11c24">
<star:cableelectrique xlink:href="id7eed3b6c-40af-4e85-8f5b-1a126285ceb9"/>
<star:noeudreseau xlink:href="id52b6e261-949f-4ea1-8331-6072183bfe1c"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id0a66543d-e88b-4ade-8e96-b5707475ec97">
<star:cableelectrique xlink:href="id7eed3b6c-40af-4e85-8f5b-1a126285ceb9"/>
<star:noeudreseau xlink:href="id693d94b9-bf44-48eb-9495-8107563cbf29"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id1f2d04de-e413-48ac-9b2a-12d9f778e88d">
<star:cableelectrique xlink:href="idf7abda61-8217-4322-b58f-30a29f6a80dc"/>
<star:noeudreseau xlink:href="id4ee847c7-0c7a-49ab-9c4b-d19825b17103"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id2870820f-44ae-4b03-a036-b4ec2ba2f78a">
<star:cableelectrique xlink:href="idf7abda61-8217-4322-b58f-30a29f6a80dc"/>
<star:noeudreseau xlink:href="id693d94b9-bf44-48eb-9495-8107563cbf29"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id2c82fe2e-dc3c-4bee-a836-3b2784d998e0">
<star:cableelectrique xlink:href="id5835a601-0200-425b-87a6-c5e1bd7c3096"/>
<star:noeudreseau xlink:href="idc29b426b-7446-46ee-b4e2-242638fd7d51"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id4acb5f9f-27e0-4dc0-b766-7cea037c6505">
<star:cableelectrique xlink:href="id5835a601-0200-425b-87a6-c5e1bd7c3096"/>
<star:noeudreseau xlink:href="id4ee847c7-0c7a-49ab-9c4b-d19825b17103"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id5220f603-fad6-438f-aaed-2dab10b0ecd9">
<star:cableelectrique xlink:href="idefe041fc-cc55-4ef3-895f-9af4718a8d42"/>
<star:noeudreseau xlink:href="id693d94b9-bf44-48eb-9495-8107563cbf29"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id442a8303-970c-41b9-808d-48fdb920e391">
<star:cableelectrique xlink:href="idefe041fc-cc55-4ef3-895f-9af4718a8d42"/>
<star:noeudreseau xlink:href="idc927f6cb-18f5-40a4-9e85-8ecf6277f106"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="ide5ce9e55-0f35-4f00-8219-a92617ff76c0">
<star:cableelectrique xlink:href="idf1e7c898-f2a3-428b-9e85-c572b6d4c5ed"/>
<star:noeudreseau xlink:href="idc927f6cb-18f5-40a4-9e85-8ecf6277f106"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idd8126f78-65a3-4cff-8fb3-03fbaf6ee22b">
<star:cableelectrique xlink:href="idf1e7c898-f2a3-428b-9e85-c572b6d4c5ed"/>
<star:noeudreseau xlink:href="idfe8261b3-2913-45b1-b74a-bfa1bc3aa187"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="ideaac2b76-43e7-436a-9bfa-40346738120b">
<star:cableelectrique xlink:href="idc4b3419a-56c2-49fd-b23b-8c14c5064279"/>
<star:noeudreseau xlink:href="idb2e8bddb-68ee-4ced-8dad-f71d5da3c0e2"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idb9625de0-dfee-4b93-a62e-91de6984cc22">
<star:cableelectrique xlink:href="idc4b3419a-56c2-49fd-b23b-8c14c5064279"/>
<star:noeudreseau xlink:href="id5dff1e9f-1e5c-43a6-95e5-b55ae8c4433e"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idaf3d161c-c400-4065-b33d-89daedc38513">
<star:cableelectrique xlink:href="id0581a5e6-ce91-4030-b2b1-6de1536a720a"/>
<star:noeudreseau xlink:href="id4066b565-c795-45e3-a7ce-680702e4f176"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id04d86099-9db2-4ca5-9e08-3b1b82c2d92f">
<star:cableelectrique xlink:href="id0581a5e6-ce91-4030-b2b1-6de1536a720a"/>
<star:noeudreseau xlink:href="id5dff1e9f-1e5c-43a6-95e5-b55ae8c4433e"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idfc2f5a03-beef-4fdd-a95e-510c5c29539e">
<star:cableelectrique xlink:href="id5d012944-f040-4ef9-87ab-15dcb3332536"/>
<star:noeudreseau xlink:href="id5dff1e9f-1e5c-43a6-95e5-b55ae8c4433e"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="ida219f906-e4da-4e98-9f74-23e88b2cfb21">
<star:cableelectrique xlink:href="id5d012944-f040-4ef9-87ab-15dcb3332536"/>
<star:noeudreseau xlink:href="id0f5d6d4a-5e18-46e8-b97a-3cdf426bddf2"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id6d37284c-61f4-41d5-8565-131c706f861d">
<star:cableelectrique xlink:href="id506e0ad9-77c3-46c5-aa9e-2f04ca553a52"/>
<star:noeudreseau xlink:href="id847f2184-96f2-475b-9832-9f948909e5a2"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id25c105b9-2420-4d4f-ab70-f15c1d3a7bf7">
<star:cableelectrique xlink:href="id506e0ad9-77c3-46c5-aa9e-2f04ca553a52"/>
<star:noeudreseau xlink:href="idf72de956-6c8d-4a3a-8c0d-36e79a41533b"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id6458b6f1-91b9-467e-863f-65e1d3961a73">
<star:cableelectrique xlink:href="idbe7d6f6a-8181-4bf0-9000-f7165ca38d9f"/>
<star:noeudreseau xlink:href="idf72de956-6c8d-4a3a-8c0d-36e79a41533b"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id1cb96d41-8264-4785-928a-97be2b24790e">
<star:cableelectrique xlink:href="idbe7d6f6a-8181-4bf0-9000-f7165ca38d9f"/>
<star:noeudreseau xlink:href="idbdc186fc-c3e4-4e1d-a900-c680cdc0cc2a"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id68fbb971-1348-4b55-be0c-e18bb3063457">
<star:cableelectrique xlink:href="idf300936f-c181-423e-9b86-2e173555596f"/>
<star:noeudreseau xlink:href="idbdc186fc-c3e4-4e1d-a900-c680cdc0cc2a"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id361a8aac-fc59-4b23-b7ab-928a40b3bd2b">
<star:cableelectrique xlink:href="idf300936f-c181-423e-9b86-2e173555596f"/>
<star:noeudreseau xlink:href="idccd7d3c6-fc41-49b6-b3a2-487ea5c23f6a"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id71d5a6de-74cb-4df6-9d48-1c7089a8154e">
<star:cableelectrique xlink:href="idf344e40a-0644-4ce0-9ee5-706e11328bd2"/>
<star:noeudreseau xlink:href="idc8b09f15-5dca-4d38-8577-b4fe2e156d89"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id5850647e-4ec0-417c-964a-a8b5887810ef">
<star:cableelectrique xlink:href="idf344e40a-0644-4ce0-9ee5-706e11328bd2"/>
<star:noeudreseau xlink:href="idccd7d3c6-fc41-49b6-b3a2-487ea5c23f6a"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id616b1d67-d49a-4929-814a-aa282a503068">
<star:cableelectrique xlink:href="idfc1de54f-fb76-4f7c-8510-766c30f084b0"/>
<star:noeudreseau xlink:href="id561e748b-049f-43b6-bb7a-8fe34adc7dbf"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id95555c32-ba13-4297-aaea-618655a4031a">
<star:cableelectrique xlink:href="idfc1de54f-fb76-4f7c-8510-766c30f084b0"/>
<star:noeudreseau xlink:href="ide8c35b31-8f25-490c-96f4-7ee80b7fa694"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id59357d25-ccd6-4984-9c40-46403d0f753c">
<star:cableelectrique xlink:href="ida81828d3-aafb-405b-9184-e5165c21ccd0"/>
<star:noeudreseau xlink:href="ide8c35b31-8f25-490c-96f4-7ee80b7fa694"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idea4d4b04-a9bb-4b3c-8c5b-778420ce23e4">
<star:cableelectrique xlink:href="ida81828d3-aafb-405b-9184-e5165c21ccd0"/>
<star:noeudreseau xlink:href="id05e05c88-5af2-42c9-b1b4-bfa81713efd3"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id51ba1d58-658f-4b05-848e-47078c57e02b">
<star:cableelectrique xlink:href="iddec68f48-f836-425e-91c2-1e64ad71f5e1"/>
<star:noeudreseau xlink:href="ide2c8d5f2-9f07-422e-98b2-74ac8630fa75"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id9a373102-81a0-44c3-b076-26116afd8e05">
<star:cableelectrique xlink:href="iddec68f48-f836-425e-91c2-1e64ad71f5e1"/>
<star:noeudreseau xlink:href="idba634d9b-8a73-40e6-9f22-db90bb1dcccc"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id4ef033d9-7f11-4250-9cd5-1cc5f2b901a7">
<star:cableelectrique xlink:href="id7d3b89c3-8c75-4419-bfca-650bf5506de7"/>
<star:noeudreseau xlink:href="id68fb6494-fe70-48a0-a225-08cf6bdea613"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id9da21ebe-40b8-4a2f-bdee-fdbbbe3b9956">
<star:cableelectrique xlink:href="id7d3b89c3-8c75-4419-bfca-650bf5506de7"/>
<star:noeudreseau xlink:href="ide8c35b31-8f25-490c-96f4-7ee80b7fa694"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="ida7c4fada-f9e3-42ab-9b5c-726ca7791ed4">
<star:cableelectrique xlink:href="ide2489a74-2779-439e-988e-b2af513a42e9"/>
<star:noeudreseau xlink:href="id53ccda7d-0f3a-4327-918f-b53fb7f6d401"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idcfcfeeeb-2559-4f9d-8f7b-f945017a4901">
<star:cableelectrique xlink:href="ide2489a74-2779-439e-988e-b2af513a42e9"/>
<star:noeudreseau xlink:href="idba634d9b-8a73-40e6-9f22-db90bb1dcccc"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id77356a9c-e1c1-4c8d-a4c9-0218c80499db">
<star:cableelectrique xlink:href="id06b4e677-62e6-4830-91a3-ee0c3b1f774a"/>
<star:noeudreseau xlink:href="id53ccda7d-0f3a-4327-918f-b53fb7f6d401"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idc25d11f4-1c37-44d7-8618-fad488c24ce3">
<star:cableelectrique xlink:href="id06b4e677-62e6-4830-91a3-ee0c3b1f774a"/>
<star:noeudreseau xlink:href="idd11da1ae-690a-4e5b-8513-d7eabb6d7707"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id1804ffc4-bdb4-4c92-b1fa-cf5025355893">
<star:cableelectrique xlink:href="id98d9fd8d-9830-40c6-8086-917d2f3788fc"/>
<star:noeudreseau xlink:href="idc2e7a464-aa52-4ba3-8deb-98368aba1796"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idc358cde9-058f-4e06-a226-bbe590da75f4">
<star:cableelectrique xlink:href="id98d9fd8d-9830-40c6-8086-917d2f3788fc"/>
<star:noeudreseau xlink:href="idd11da1ae-690a-4e5b-8513-d7eabb6d7707"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id9031817d-2b17-4c1e-a228-d7142c996130">
<star:cableelectrique xlink:href="idcf217558-23c6-495b-bf69-f90f2086d99b"/>
<star:noeudreseau xlink:href="id7cc748d8-c4f5-468c-9655-5ae95c2cd804"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id7a7ec4aa-ea3f-471f-8afd-fb6f6f1527e3">
<star:cableelectrique xlink:href="idcf217558-23c6-495b-bf69-f90f2086d99b"/>
<star:noeudreseau xlink:href="id605baf95-b9cf-4c1c-856a-3025bc3b7094"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id4d6838fe-05d6-491f-ac83-d140006b8eea">
<star:cableelectrique xlink:href="id6de0ca3b-1ef5-486e-bfe6-83dc8bc0eba6"/>
<star:noeudreseau xlink:href="idb96a532e-daaa-443a-8bc7-1fb3320cfeb9"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idbad7974d-4b80-4825-b516-7826dc018ba2">
<star:cableelectrique xlink:href="id6de0ca3b-1ef5-486e-bfe6-83dc8bc0eba6"/>
<star:noeudreseau xlink:href="ide91f670c-66dc-4493-a6ab-92c5bb6b9bf1"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idffd881fb-7c1d-4d1c-8dd9-a2a275fd727b">
<star:cableelectrique xlink:href="id5b57139d-6fb1-4981-a759-3368dc28948b"/>
<star:noeudreseau xlink:href="idf148f290-fda8-4459-b5a3-9cce93a5a102"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idc7d2ced8-ab4d-404b-a707-5499d2142d51">
<star:cableelectrique xlink:href="id5b57139d-6fb1-4981-a759-3368dc28948b"/>
<star:noeudreseau xlink:href="id2d32513a-3739-4f7f-aa07-8f100ad40d4c"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id449709c9-3993-431d-9718-1202aad31337">
<star:cableelectrique xlink:href="idd7ec38a9-34ac-43dc-962b-7ebdec10d289"/>
<star:noeudreseau xlink:href="idff7e3e76-6b1f-4dc2-951f-227a1aecd24a"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id5170d591-e374-4937-b762-a96f7d79ac7c">
<star:cableelectrique xlink:href="idd7ec38a9-34ac-43dc-962b-7ebdec10d289"/>
<star:noeudreseau xlink:href="idffbbfd8d-2341-4878-9cd7-ba0f0b2e1614"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id7be7a493-cf98-4889-bc47-6d76d0c008b5">
<star:cableelectrique xlink:href="id9a129487-70d0-4212-809a-6e190e9bfbed"/>
<star:noeudreseau xlink:href="idb249048e-117c-427b-8291-ae04d6fefdc2"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id8b5707a4-aa9c-41dd-9e9d-634b45e1db2c">
<star:cableelectrique xlink:href="id9a129487-70d0-4212-809a-6e190e9bfbed"/>
<star:noeudreseau xlink:href="id553c015f-4d3b-4fd8-a940-f223a4b555dd"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idd9cea129-b1f2-4deb-8748-e2031ab1fb19">
<star:cableelectrique xlink:href="idb92d5f59-13d3-492e-ab64-ca13888ea0eb"/>
<star:noeudreseau xlink:href="id884ae871-989d-44ea-acd1-cf1db340197f"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idd5646698-609a-44c5-94d4-5b044000285d">
<star:cableelectrique xlink:href="idb92d5f59-13d3-492e-ab64-ca13888ea0eb"/>
<star:noeudreseau xlink:href="idcf2ed0e7-445c-4dfa-8e73-e634c461f7c8"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id956e4534-cab7-4bf5-a9b5-5eec4f45136c">
<star:cableelectrique xlink:href="id0e002480-f216-4534-8745-773c257325db"/>
<star:noeudreseau xlink:href="ida6b3ebac-91b6-45a5-b997-afab7d526f17"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id9740a9bf-3519-4e55-9d8d-13f4438dd686">
<star:cableelectrique xlink:href="id0e002480-f216-4534-8745-773c257325db"/>
<star:noeudreseau xlink:href="id4aba5036-813e-4d23-9ccc-7c6c666e2e25"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idf02c7172-a068-4640-84a5-0a44d66c7722">
<star:cableelectrique xlink:href="id3b9350f5-132c-4c1f-8b94-7fff86f2d2ad"/>
<star:noeudreseau xlink:href="ide8d566e9-d0f1-48cb-b992-b2183434d4d7"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id7ba198b9-8e66-44d2-8350-8a96045c7ab2">
<star:cableelectrique xlink:href="id3b9350f5-132c-4c1f-8b94-7fff86f2d2ad"/>
<star:noeudreseau xlink:href="id4aba5036-813e-4d23-9ccc-7c6c666e2e25"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="iddad2ed9e-ab92-4787-bef2-495942e3f5aa">
<star:cableelectrique xlink:href="id20c3aeab-eddd-4822-89a1-7e7804ae6f4f"/>
<star:noeudreseau xlink:href="idb294d246-b5c0-4511-88a0-af8260a23b78"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id6e0e445c-1ccb-421a-bc7a-9d61892a8e39">
<star:cableelectrique xlink:href="id20c3aeab-eddd-4822-89a1-7e7804ae6f4f"/>
<star:noeudreseau xlink:href="id287513ff-53c6-43e1-9788-bd21ddbb6277"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id4a4649c5-f048-41aa-bab3-641ad9b33884">
<star:cableelectrique xlink:href="idbaa10fb9-c518-4e68-81de-e7829b5caa98"/>
<star:noeudreseau xlink:href="idfc9d0296-99a0-4376-a576-a3847cde9c92"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id66d9e993-add4-4298-87de-c9244b69f85a">
<star:cableelectrique xlink:href="idbaa10fb9-c518-4e68-81de-e7829b5caa98"/>
<star:noeudreseau xlink:href="id8720189f-6b5e-46ad-8282-48665c614b9b"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id3b80b85f-c290-45df-926b-68f0a47c80f7">
<star:cableelectrique xlink:href="idb0cf8723-ec14-4e2f-9760-36aef558d1a0"/>
<star:noeudreseau xlink:href="id511d93ad-664a-4908-a1a1-2ba9682b876f"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id1e190868-7960-4be7-a8f2-70eac906f1c5">
<star:cableelectrique xlink:href="idb0cf8723-ec14-4e2f-9760-36aef558d1a0"/>
<star:noeudreseau xlink:href="ide497d0b8-986f-4378-82f0-38feddd57944"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id2f48850a-82ab-4a88-872d-f08dc9f7bdf2">
<star:cableelectrique xlink:href="id6554c1b8-7bc0-4e01-a794-5f9abfd3ca2d"/>
<star:noeudreseau xlink:href="id631aaa54-4ac1-453b-90b9-e9c5a7e841ee"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idd7fd402b-1529-44af-8069-68cd031088be">
<star:cableelectrique xlink:href="id6554c1b8-7bc0-4e01-a794-5f9abfd3ca2d"/>
<star:noeudreseau xlink:href="id8720189f-6b5e-46ad-8282-48665c614b9b"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id20af70a0-dd77-4fdb-8228-dba63df726fc">
<star:cableelectrique xlink:href="id2a7ce2d8-ab9f-48bb-b6ad-53571a6efba7"/>
<star:noeudreseau xlink:href="id6169432d-d1ea-4d9c-8401-d755a5a78273"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idca0b9e53-c9c0-494e-b0e7-caf5497ced1f">
<star:cableelectrique xlink:href="id2a7ce2d8-ab9f-48bb-b6ad-53571a6efba7"/>
<star:noeudreseau xlink:href="id62fb7756-0939-4a30-9b01-f0129fd908da"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="idda80f531-d5d2-4002-9821-3ae60950f951">
<star:cableelectrique xlink:href="idea9105df-786b-4cfc-a048-53cccf5bb03a"/>
<star:noeudreseau xlink:href="id01abd778-d49f-4864-ad41-b71b27af8ed0"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique_NoeudReseau gml:id="id96219bf4-3938-4dff-99b2-ec89598de1ef">
<star:cableelectrique xlink:href="idea9105df-786b-4cfc-a048-53cccf5bb03a"/>
<star:noeudreseau xlink:href="id287513ff-53c6-43e1-9788-bd21ddbb6277"/>
</star:CableElectrique_NoeudReseau>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="ide91168b0-2479-45ae-bfbc-f46da004b75b">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>148 AM</star:Commentaire>
<star:DateConstruction>1978-01-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Nu"/>
<star:Materiau xlink:href="AM"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">148.10000610351562</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idc346a984-4743-42ae-89ac-0b0355b5dbcd">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>34 AM</star:Commentaire>
<star:DateConstruction>1985-01-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Nu"/>
<star:Materiau xlink:href="AM"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">34.400001525878906</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id885f133e-09fd-4c82-ac87-6e6ec840a864">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>148 AM</star:Commentaire>
<star:DateConstruction>1978-01-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Nu"/>
<star:Materiau xlink:href="AM"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">148.10000610351562</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idd7f392c4-9bfc-4cf0-a4ac-8affbe677fb3">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>34 AM</star:Commentaire>
<star:DateConstruction>1982-01-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Nu"/>
<star:Materiau xlink:href="AM"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">34.400001525878906</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id745f7f12-e39c-43d6-a739-19ed7e8086ac">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>148 AM</star:Commentaire>
<star:DateConstruction>1978-01-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Nu"/>
<star:Materiau xlink:href="AM"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">148.10000610351562</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="iddd99ef8b-ad50-409b-b842-6b3bbf089e4d">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>148 AM</star:Commentaire>
<star:DateConstruction>1978-01-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Nu"/>
<star:Materiau xlink:href="AM"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">148.10000610351562</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id5b57139d-6fb1-4981-a759-3368dc28948b">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>240 AL S6</star:Commentaire>
<star:DateConstruction>2012-03-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="S6 - Synthétique NF C33-226"/>
<star:Materiau xlink:href="AL"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idd7ec38a9-34ac-43dc-962b-7ebdec10d289">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>240 AL S6</star:Commentaire>
<star:DateConstruction>2012-03-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="S6 - Synthétique NF C33-226"/>
<star:Materiau xlink:href="AL"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id9a129487-70d0-4212-809a-6e190e9bfbed">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>240 AL S6</star:Commentaire>
<star:DateConstruction>2012-03-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="S6 - Synthétique NF C33-226"/>
<star:Materiau xlink:href="AL"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idb92d5f59-13d3-492e-ab64-ca13888ea0eb">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>240 AL S6</star:Commentaire>
<star:DateConstruction>2012-12-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="S6 - Synthétique NF C33-226"/>
<star:Materiau xlink:href="AL"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id0e002480-f216-4534-8745-773c257325db">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>240 AL S6</star:Commentaire>
<star:DateConstruction>2012-12-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="S6 - Synthétique NF C33-226"/>
<star:Materiau xlink:href="AL"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id3b9350f5-132c-4c1f-8b94-7fff86f2d2ad">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>240 AL S6</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="S6 - Synthétique NF C33-226"/>
<star:Materiau xlink:href="AL"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id20c3aeab-eddd-4822-89a1-7e7804ae6f4f">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>240 AL S6</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="S6 - Synthétique NF C33-226"/>
<star:Materiau xlink:href="AL"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idbaa10fb9-c518-4e68-81de-e7829b5caa98">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>240 AL S6</star:Commentaire>
<star:DateConstruction>2017-06-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="S6 - Synthétique NF C33-226"/>
<star:Materiau xlink:href="AL"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idb0cf8723-ec14-4e2f-9760-36aef558d1a0">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>240 AL S6</star:Commentaire>
<star:DateConstruction>2017-06-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="S6 - Synthétique NF C33-226"/>
<star:Materiau xlink:href="AL"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id6554c1b8-7bc0-4e01-a794-5f9abfd3ca2d">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>240 AL S6</star:Commentaire>
<star:DateConstruction>2023-02-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="S6 - Synthétique NF C33-226"/>
<star:Materiau xlink:href="AL"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id2a7ce2d8-ab9f-48bb-b6ad-53571a6efba7">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>240 AL S6</star:Commentaire>
<star:DateConstruction>2023-02-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="S6 - Synthétique NF C33-226"/>
<star:Materiau xlink:href="AL"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idea9105df-786b-4cfc-a048-53cccf5bb03a">
<star:reseau xlink:href="Reseau"/>
<star:Commentaire>240 AL S6</star:Commentaire>
<star:DateConstruction>2023-02-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="S6 - Synthétique NF C33-226"/>
<star:Materiau xlink:href="AL"/>
<star:DomaineTension>HTA</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>3</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">20000</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id7da65535-8c03-4456-9532-4c9ad428c7a8">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100412038</star:Code>
<star:Commentaire>T 70 AL</star:Commentaire>
<star:DateConstruction>1946-01-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">54</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">70</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idc3982db6-118b-4114-8bfc-97ab27febdba">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100406435</star:Code>
<star:Commentaire>T 70 AL</star:Commentaire>
<star:DateConstruction>1946-01-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">54</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">70</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="ida4e0a972-f6f9-4ad2-b586-4c1d284e4031">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100406436</star:Code>
<star:Commentaire>T 70 AL</star:Commentaire>
<star:DateConstruction>1946-01-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">54</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">70</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id2ba97ef0-7c0b-4fb3-af2a-dafce5a520db">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100406438</star:Code>
<star:Commentaire>T 70 AL</star:Commentaire>
<star:DateConstruction>1946-01-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">54</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">70</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id6c6429ff-b71b-4a5e-ba08-51f5d63d8516">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100409840</star:Code>
<star:Commentaire>T 70 AL</star:Commentaire>
<star:DateConstruction>1946-01-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">54</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">70</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id8343e111-c2be-4d17-94c9-dc668f203370">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100402615</star:Code>
<star:Commentaire>T 70 AL</star:Commentaire>
<star:DateConstruction>1946-01-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">54</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">70</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idceb44bbc-b96c-475a-851b-03fcb85a1cbb">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403366</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL</star:Commentaire>
<star:DateConstruction>2012-12-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idf2a05922-b273-4709-afc2-903d19621f26">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403367</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL</star:Commentaire>
<star:DateConstruction>2012-12-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id9125ba6d-3404-4f39-a297-8ad7eda50935">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403373</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL</star:Commentaire>
<star:DateConstruction>2012-12-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id7eed3b6c-40af-4e85-8f5b-1a126285ceb9">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403253</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idf7abda61-8217-4322-b58f-30a29f6a80dc">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403255</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id5835a601-0200-425b-87a6-c5e1bd7c3096">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403256</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idefe041fc-cc55-4ef3-895f-9af4718a8d42">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403257</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idf1e7c898-f2a3-428b-9e85-c572b6d4c5ed">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403259</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idc4b3419a-56c2-49fd-b23b-8c14c5064279">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403321</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id0581a5e6-ce91-4030-b2b1-6de1536a720a">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403317</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id5d012944-f040-4ef9-87ab-15dcb3332536">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403330</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id506e0ad9-77c3-46c5-aa9e-2f04ca553a52">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403354</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idbe7d6f6a-8181-4bf0-9000-f7165ca38d9f">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403374</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idf300936f-c181-423e-9b86-2e173555596f">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403395</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idf344e40a-0644-4ce0-9ee5-706e11328bd2">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403423</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idfc1de54f-fb76-4f7c-8510-766c30f084b0">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403428</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="ida81828d3-aafb-405b-9184-e5165c21ccd0">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403430</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="iddec68f48-f836-425e-91c2-1e64ad71f5e1">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403435</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id7d3b89c3-8c75-4419-bfca-650bf5506de7">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403439</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="ide2489a74-2779-439e-988e-b2af513a42e9">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403440</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id06b4e677-62e6-4830-91a3-ee0c3b1f774a">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403444</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id98d9fd8d-9830-40c6-8086-917d2f3788fc">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403476</star:Code>
<star:Commentaire>3 x 240 AL + 95 AL NM</star:Commentaire>
<star:DateConstruction>2014-09-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">95</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="idcf217558-23c6-495b-bf69-f90f2086d99b">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403485</star:Code>
<star:Commentaire>3 x 240 AL + 115M (95E) NM +AD</star:Commentaire>
<star:DateConstruction>2023-02-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">115</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:CableElectrique gml:id="id6de0ca3b-1ef5-486e-bfe6-83dc8bc0eba6">
<star:reseau xlink:href="Reseau"/>
<star:Code>8100403487</star:Code>
<star:Commentaire>3 x 240 AL + 115M (95E) NM +AD</star:Commentaire>
<star:DateConstruction>2023-02-01</star:DateConstruction>
<star:Statut>Functional</star:Statut>
<star:Isolant xlink:href="Isole"/>
<star:Materiau xlink:href="AL"/>
<star:SectionNeutre uom="mm-2">115</star:SectionNeutre>
<star:DomaineTension>BT</star:DomaineTension>
<star:FonctionCable xlink:href="DistributionEnergie"/>
<star:NombreConducteurs>4</star:NombreConducteurs>
<star:Section uom="mm-2">240</star:Section>
<star:TensionNominale uom="V">400</star:TensionNominale>
</star:CableElectrique>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id41cfd515-fac9-44ec-8603-d10b1c2d5b19">
<star:cheminement xlink:href="id8c982388-c8b7-428e-914c-326a42d88c04"/>
<star:support xlink:href="idf0c7290c-9885-461e-85f9-a960300c7035"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id6681dbae-4b40-47e6-a229-c4dbf36d9564">
<star:cheminement xlink:href="id10176432-3d76-4d00-ad22-7f787e4b0d1b"/>
<star:support xlink:href="id64500598-d690-4c4c-b9d9-bceb242a6f5b"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id250ab3a0-c765-4b4d-beb3-e56bd6f1cada">
<star:cheminement xlink:href="id8eac80bd-2551-4169-97d0-3e38cf8a08ef"/>
<star:support xlink:href="idc28345c7-bc0e-4d8c-b6ce-092a7395b9b1"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id754db32b-bd45-4e40-858f-607b51841c9a">
<star:cheminement xlink:href="idef740676-cb1a-4e05-834a-41ab9d708505"/>
<star:support xlink:href="id846e5aed-a34b-4dfd-8268-a3e54b396bb2"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id02d444e0-3f06-415d-b4b5-f2ad05f1a9dd">
<star:cheminement xlink:href="idef740676-cb1a-4e05-834a-41ab9d708505"/>
<star:support xlink:href="id28e3a1d6-f0ba-49f4-aa41-c7a50a595583"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id43789a9a-3007-45c4-a7b5-a3e2aec4a03d">
<star:cheminement xlink:href="id139d22fa-7b9b-4f8b-bbd5-aa74d8fd33a5"/>
<star:support xlink:href="idef78bb67-55f4-4408-ae54-aadb198e8d7a"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id9475b2e5-1ecf-4cb4-84d0-e0f804eac172">
<star:cheminement xlink:href="id139d22fa-7b9b-4f8b-bbd5-aa74d8fd33a5"/>
<star:support xlink:href="id83a6bdc4-8bdd-4b9c-a627-30cd851ddcc6"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id7cefadf1-d0af-4dd7-923b-514d915b570d">
<star:cheminement xlink:href="id794f4595-b880-4def-8a4b-8da8a625c877"/>
<star:support xlink:href="ida6fc77ab-a7c3-4f34-ab36-98978cbe95e7"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="ida0e647cc-b91e-4235-8970-832fbcaa507a">
<star:cheminement xlink:href="id10176432-3d76-4d00-ad22-7f787e4b0d1b"/>
<star:support xlink:href="id9c10cd18-bddd-4775-b6af-5460d2486d71"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id88ad0917-ba68-42e1-a028-b7b6e2697376">
<star:cheminement xlink:href="id2c630eb5-6121-4ade-8c6a-9b8b9d2088e7"/>
<star:support xlink:href="id881533b9-5f60-40b6-b78b-4e11dd0dd79a"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id3d535a18-4bd6-44f1-9133-b1228dac4fbd">
<star:cheminement xlink:href="idb02f31e8-dec5-4d80-8784-9b9213f355a6"/>
<star:support xlink:href="ida2849593-4e3b-4905-9e77-3ff82c8b9198"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id5a1c7211-614c-40e8-93ff-4887996e7fba">
<star:cheminement xlink:href="id7d1ae235-0e72-480c-9dc1-9f879c9cf2cc"/>
<star:support xlink:href="id1bfee6be-544f-4656-ab53-2abf9aed4c53"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="idb70417cf-b1c0-4989-b3ed-639535661c85">
<star:cheminement xlink:href="id3094fcf7-4bfd-45a3-b333-1bd9d4e6379a"/>
<star:support xlink:href="id0535c79e-043a-457d-8c1f-b1d0bbd98705"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="idc43a1aec-658b-41fa-a49d-3d6d182ea8c6">
<star:cheminement xlink:href="idefa908a0-805e-45e6-98a4-d8678ac40b9c"/>
<star:support xlink:href="id1a2a53c3-05f8-4d27-9a52-82cb4d1c1693"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id8c105a9a-2311-4a90-8e4c-b488934fa9b6">
<star:cheminement xlink:href="id3094fcf7-4bfd-45a3-b333-1bd9d4e6379a"/>
<star:support xlink:href="id70047720-e8be-4524-8617-23e6fbfc327e"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id8320ab4a-6546-431e-abca-3f3008d16c44">
<star:cheminement xlink:href="idefa908a0-805e-45e6-98a4-d8678ac40b9c"/>
<star:support xlink:href="id80a45a22-0927-4c8d-a92a-7259c2586614"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id2e5e932c-dc28-40bc-9343-71a6e9ff7550">
<star:cheminement xlink:href="id8eac80bd-2551-4169-97d0-3e38cf8a08ef"/>
<star:support xlink:href="id846e5aed-a34b-4dfd-8268-a3e54b396bb2"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="ida61e80f2-5c54-48f1-a09d-9b517c103730">
<star:cheminement xlink:href="id8c982388-c8b7-428e-914c-326a42d88c04"/>
<star:support xlink:href="id64500598-d690-4c4c-b9d9-bceb242a6f5b"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="idcb74e126-12cc-4ed4-89a7-573fc8dc5844">
<star:cheminement xlink:href="id794f4595-b880-4def-8a4b-8da8a625c877"/>
<star:support xlink:href="idf0c7290c-9885-461e-85f9-a960300c7035"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="ida3ad2c9c-dbe8-4a0f-b617-ac4f9ff65dbd">
<star:cheminement xlink:href="id2c630eb5-6121-4ade-8c6a-9b8b9d2088e7"/>
<star:support xlink:href="id0535c79e-043a-457d-8c1f-b1d0bbd98705"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id76ebf953-d9ef-412e-bfa6-215fb6a8b62b">
<star:cheminement xlink:href="idd2e8343c-4e9c-45d5-93da-d1b2b40189d1"/>
<star:support xlink:href="id881533b9-5f60-40b6-b78b-4e11dd0dd79a"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id0413acd8-04ac-48ae-8fa7-4468263da72b">
<star:cheminement xlink:href="idb02f31e8-dec5-4d80-8784-9b9213f355a6"/>
<star:support xlink:href="id0535c79e-043a-457d-8c1f-b1d0bbd98705"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien_Support gml:id="id06956912-8ca6-4efd-9f6b-00fb78b74055">
<star:cheminement xlink:href="id7d1ae235-0e72-480c-9dc1-9f879c9cf2cc"/>
<star:support xlink:href="id1a2a53c3-05f8-4d27-9a52-82cb4d1c1693"/>
</star:Aerien_Support>
</gml:featureMember>
<gml:featureMember><star:Aerien gml:id="id10176432-3d76-4d00-ad22-7f787e4b0d1b">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634505.4797312899 6314185.884168164 634537.3996085727 6314187.413563695 634537.4086153992 6314187.415486089 634564.0047457457 6314191.896558613 634616.5277560396 6314200.746437175 634672.0615106856 6314203.855918193 634678.5698535125 6314204.220891655 634701.8053364159 6314207.376541016 634705.8197157662 6314209.108055593</gml:posList>
</gml:LineString>
</star:Geometrie>
<star:ModePose>Supporte</star:ModePose>
</star:Aerien>
</gml:featureMember>
<gml:featureMember><star:Aerien gml:id="idef740676-cb1a-4e05-834a-41ab9d708505">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634871.2901016492 6314307.489665393 634881.23594514 6314278.531837993 634914.2321507887 6314277.252067167</gml:posList>
</gml:LineString>
</star:Geometrie>
<star:ModePose>Supporte</star:ModePose>
</star:Aerien>
</gml:featureMember>
<gml:featureMember><star:Aerien gml:id="id8eac80bd-2551-4169-97d0-3e38cf8a08ef">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634914.2321507887 6314277.252067167 634914.711811752 6314277.389897921 634951.3295017536 6314287.939585669 634997.5877562722 6314304.988652005 635104.0615278806 6314346.200280536 635110.6705192614 6314347.847092607 635167.2743127531 6314362.656573123 635198.730633151 6314371.692150872</gml:posList>
</gml:LineString>
</star:Geometrie>
<star:ModePose>Supporte</star:ModePose>
</star:Aerien>
</gml:featureMember>
<gml:featureMember><star:Aerien gml:id="id8c982388-c8b7-428e-914c-326a42d88c04">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634866.9655275227 6314265.87664864 634860.9539000996 6314260.889244606 634761.5004864872 6314227.021118817 634750.1537914581 6314223.157362589 634728.4061238377 6314215.751578477 634727.5605759692 6314215.462974737 634705.8197157662 6314209.108055593</gml:posList>
</gml:LineString>
</star:Geometrie>
<star:ModePose>Supporte</star:ModePose>
</star:Aerien>
</gml:featureMember>
<gml:featureMember><star:Aerien gml:id="id139d22fa-7b9b-4f8b-bbd5-aa74d8fd33a5">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634879.2277718488 6314277.548709505 634877.9168314707 6314281.686475315 634877.7659778874 6314282.162254252</gml:posList>
</gml:LineString>
</star:Geometrie>
<star:ModePose>Supporte</star:ModePose>
</star:Aerien>
</gml:featureMember>
<gml:featureMember><star:Aerien gml:id="id794f4595-b880-4def-8a4b-8da8a625c877">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634866.9655275227 6314265.87664864 634876.9361162016 6314275.042523219 634879.0542541001 6314276.988740775</gml:posList>
</gml:LineString>
</star:Geometrie>
<star:ModePose>Supporte</star:ModePose>
</star:Aerien>
</gml:featureMember>
<gml:featureMember><star:Aerien gml:id="id3094fcf7-4bfd-45a3-b333-1bd9d4e6379a">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634512.1012678836 6314174.2445021 634617.04748199 6314191.239821343</gml:posList>
</gml:LineString>
</star:Geometrie>
<star:ModePose>Supporte</star:ModePose>
</star:Aerien>
</gml:featureMember>
<gml:featureMember><star:Aerien gml:id="id2c630eb5-6121-4ade-8c6a-9b8b9d2088e7">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634617.04748199 6314191.239821343 634614.0603587572 6314204.568237465</gml:posList>
</gml:LineString>
</star:Geometrie>
<star:ModePose>Supporte</star:ModePose>
</star:Aerien>
</gml:featureMember>
<gml:featureMember><star:Aerien gml:id="idefa908a0-805e-45e6-98a4-d8678ac40b9c">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634616.6195715844 6314184.521304049 634539.4430854147 6314174.36878107 634512.2271266922 6314170.904869904 634427.297903033 6314182.839791844 634373.8563855302 6314197.661345003 634256.874770253 6314248.526480904</gml:posList>
</gml:LineString>
</star:Geometrie>
<star:ModePose>Supporte</star:ModePose>
</star:Aerien>
</gml:featureMember>
<gml:featureMember><star:Aerien gml:id="idd2e8343c-4e9c-45d5-93da-d1b2b40189d1">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634614.0603587572 6314204.568237465 634612.9010589126 6314210.708675783</gml:posList>
</gml:LineString>
</star:Geometrie>
<star:ModePose>Supporte</star:ModePose>
</star:Aerien>
</gml:featureMember>
<gml:featureMember><star:Aerien gml:id="idb02f31e8-dec5-4d80-8784-9b9213f355a6">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634617.04748199 6314191.239821343 634701.1888431343 6314194.127303999 634815.1098364544 6314229.726168815</gml:posList>
</gml:LineString>
</star:Geometrie>
<star:ModePose>Supporte</star:ModePose>
</star:Aerien>
</gml:featureMember>
<gml:featureMember><star:Aerien gml:id="id7d1ae235-0e72-480c-9dc1-9f879c9cf2cc">
<star:reseau xlink:href="Reseau"/>
<star:PrecisionXY>C</star:PrecisionXY>
<star:PrecisionZ>C</star:PrecisionZ>
<star:Geometrie>
<gml:LineString srsName="EPSG:2154" srsDimension="2">
<gml:posList>634616.6195715844 6314184.521304049 634698.7865273869 6314192.005652538 634735.8077753674 6314202.314203474 634815.9063089796 6314227.734522774</gml:posList>
</gml:LineString>
</star:Geometrie>
<star:ModePose>Supporte</star:ModePose>
</star:Aerien>
</gml:featureMember>
<gml:featureMember><star:BatimentTechnique gml:id="iddcca61e7-c850-4bb4-92e6-ce1ba701a60a">
<star:reseau xlink:href="Reseau"/>
<star:Code>1383150731</star:Code>
<star:geometriesupplementaire xlink:href="id701a3f21-930c-4f5f-a470-67bd9015163e"/>
<star:Commentaire>LEBON PHILLIPE</star:Commentaire>
<star:PositionVerticale>OnGroundSurface</star:PositionVerticale>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634885.3187776873 6314312.873340836</gml:pos>
</gml:Point>
</star:Geometrie>
</star:BatimentTechnique>
</gml:featureMember>
<gml:featureMember><star:BatimentTechnique gml:id="id1e7b1781-2915-40dd-836c-4721f495e55c">
<star:reseau xlink:href="Reseau"/>
<star:Code>2576111891</star:Code>
<star:geometriesupplementaire xlink:href="id3d1c74c6-223f-4e70-a4de-fd009628b33f"/>
<star:Commentaire>COUSTOU</star:Commentaire>
<star:PositionVerticale>OnGroundSurface</star:PositionVerticale>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634921.4351762872 6314158.562284553</gml:pos>
</gml:Point>
</star:Geometrie>
</star:BatimentTechnique>
</gml:featureMember>
<gml:featureMember><star:BatimentTechnique gml:id="id0bfdbf4b-809b-46d3-ae10-68f0f7f6ab6e">
<star:reseau xlink:href="Reseau"/>
<star:Code>184088831</star:Code>
<star:geometriesupplementaire xlink:href="iddee4f946-9c09-4706-9fc6-e539cb97c4dc"/>
<star:Commentaire>STIMIP</star:Commentaire>
<star:PositionVerticale>OnGroundSurface</star:PositionVerticale>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634612.6236986142 6314212.181494577</gml:pos>
</gml:Point>
</star:Geometrie>
</star:BatimentTechnique>
</gml:featureMember>
<gml:featureMember><star:BatimentTechnique gml:id="idad8670ab-73a7-4594-b120-c2f3ebf903ff">
<star:reseau xlink:href="Reseau"/>
<star:Code>1608900132</star:Code>
<star:geometriesupplementaire xlink:href="id95109930-892f-4670-8514-028e41ec6c0c"/>
<star:Commentaire>INOPROD4</star:Commentaire>
<star:PositionVerticale>OnGroundSurface</star:PositionVerticale>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634755.9493778658 6314176.728304613</gml:pos>
</gml:Point>
</star:Geometrie>
</star:BatimentTechnique>
</gml:featureMember>
<gml:featureMember><star:BatimentTechnique gml:id="id8dad5015-0c4a-41aa-a518-6a7f798befa1">
<star:reseau xlink:href="Reseau"/>
<star:Code>1868805238</star:Code>
<star:geometriesupplementaire xlink:href="id9fc6ff3d-5ffd-487e-9292-e204d8272386"/>
<star:Commentaire>AUROCK</star:Commentaire>
<star:PositionVerticale>OnGroundSurface</star:PositionVerticale>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634761.6024174234 6314021.366157063</gml:pos>
</gml:Point>
</star:Geometrie>
</star:BatimentTechnique>
</gml:featureMember>
<gml:featureMember><star:BatimentTechnique gml:id="idad93cb22-ef1e-4b52-a04f-ce763565d9fd">
<star:reseau xlink:href="Reseau"/>
<star:Code>1465988054</star:Code>
<star:geometriesupplementaire xlink:href="idcaa8adb8-536a-4899-825c-984cadcb60c1"/>
<star:Commentaire>INOPROD 3</star:Commentaire>
<star:PositionVerticale>OnGroundSurface</star:PositionVerticale>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634312.9446249586 6313938.387719914</gml:pos>
</gml:Point>
</star:Geometrie>
</star:BatimentTechnique>
</gml:featureMember>
<gml:featureMember><star:BatimentTechnique gml:id="id0232e615-0515-4309-814e-2c89ea9fe662">
<star:reseau xlink:href="Reseau"/>
<star:Code>1507361502</star:Code>
<star:geometriesupplementaire xlink:href="id3ad94eb1-5ea6-47c6-bd79-1bc4b6f1beed"/>
<star:Commentaire>INOPROD2</star:Commentaire>
<star:PositionVerticale>OnGroundSurface</star:PositionVerticale>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634745.5541663698 6314022.818430231</gml:pos>
</gml:Point>
</star:Geometrie>
</star:BatimentTechnique>
</gml:featureMember>
<gml:featureMember><star:BatimentTechnique gml:id="ide2b4fe7b-b477-4648-9de7-eff2beefdc56">
<star:reseau xlink:href="Reseau"/>
<star:Code>2576112055</star:Code>
<star:geometriesupplementaire xlink:href="ida045fd9b-5e82-44d7-8eca-40402a6ba1b0"/>
<star:Commentaire>MOISSAN</star:Commentaire>
<star:PositionVerticale>OnGroundSurface</star:PositionVerticale>
<star:Statut>Functional</star:Statut>
<star:Geometrie>
<gml:Point srsName="EPSG:2154" srsDimension="2">
<gml:pos>634820.4563266367 6314102.7554734675</gml:pos>
</gml:Point>
</star:Geometrie>
</star:BatimentTechnique>
</gml:featureMember>
</gml:FeatureCollection>
