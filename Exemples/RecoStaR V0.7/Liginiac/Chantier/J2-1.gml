<?xml version="1.0" encoding="utf-8" ?>
<gml:FeatureCollection
    xmlns:ogr_gmlas="http://gdal.org/ogr/gmlas"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:RecoStaR="http://StaR-Elec.com"
    xmlns:gco="http://www.isotc211.org/2005/gco"
    xmlns:gmd="http://www.isotc211.org/2005/gmd"
    xmlns:gsr="http://www.isotc211.org/2005/gsr"
    xmlns:gss="http://www.isotc211.org/2005/gss"
    xmlns:gts="http://www.isotc211.org/2005/gts"
    xmlns:gml="http://www.opengis.net/gml/3.2"
    xmlns:gmlexr="http://www.opengis.net/gml/3.3/exr"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xsi:schemaLocation="http://StaR-Elec.com https://gitlab.com/StaR-Elec/StaR-Elec/-/raw/main/RecoStaR/SchemaStarElecRecoStar.xsd" >
<!-- GML au format OpenRecoStar v0.7 via GDAL / QGIS. -->
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idb6f92681-a38c-4e9f-80b4-d358223b5ffc">
      <RecoStaR:noeudreseau xlink:href="idad94456c-bd12-4302-9d20-c1ec7ecd9f47" />
      <RecoStaR:cableelectrique xlink:href="id649a929f-e1bc-441a-b103-0cf71f72a6eb" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idf4c9d1ca-c22f-4881-83d2-ce9ade2e2372">
      <RecoStaR:noeudreseau xlink:href="id144796c1-869c-47c6-b49e-b5979164ecae" />
      <RecoStaR:cableelectrique xlink:href="id649a929f-e1bc-441a-b103-0cf71f72a6eb" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idfb364c52-6758-41b4-8db6-18f3d60d8f4c">
      <RecoStaR:noeudreseau xlink:href="id64b16fc1-7cd9-4107-bba0-f10e8e30dcc6" />
      <RecoStaR:cableelectrique xlink:href="id93304e8c-f2cc-40db-bd09-d8e250e1344f" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idf8ac9308-295f-43bc-8abc-93e70c0ad39d">
      <RecoStaR:noeudreseau xlink:href="idad94456c-bd12-4302-9d20-c1ec7ecd9f47" />
      <RecoStaR:cableelectrique xlink:href="ide6442815-249a-4660-adb9-4591885c3815" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id7a6dad01-db4b-4800-8f5d-5c25cd7a140d">
      <RecoStaR:noeudreseau xlink:href="id64ee37ae-e895-415f-89d3-dccfd9ba8493" />
      <RecoStaR:cableelectrique xlink:href="ideb2c23b9-9253-4bbb-88fa-f3a5ba42e2a6" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id6148fdbf-6ec4-4330-8161-72e5bde179cc">
      <RecoStaR:noeudreseau xlink:href="ide7732f16-be6d-4f7a-906b-3f1bc32b66e3" />
      <RecoStaR:cableelectrique xlink:href="ideb2c23b9-9253-4bbb-88fa-f3a5ba42e2a6" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id144abdc2-cb1e-40ad-8fcf-f75a6d847980">
      <RecoStaR:noeudreseau xlink:href="ide7732f16-be6d-4f7a-906b-3f1bc32b66e3" />
      <RecoStaR:cableelectrique xlink:href="idf538040a-cd17-4587-acba-12c3f1af214a" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id4c3e03cc-b319-43db-8864-fec026e5f50a">
      <RecoStaR:noeudreseau xlink:href="id88c1acc7-e87d-4180-9c62-98c22aa4fbc3" />
      <RecoStaR:cableelectrique xlink:href="id7aa3064d-6c94-4173-8016-7e111d0e3fae" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idfa166a01-e96f-4f9a-83dc-ff3f9d5caabd">
      <RecoStaR:noeudreseau xlink:href="id88c1acc7-e87d-4180-9c62-98c22aa4fbc3" />
      <RecoStaR:cableelectrique xlink:href="ide6442815-249a-4660-adb9-4591885c3815" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id09da1541-fd74-4f92-8866-c577b12d6f46">
      <RecoStaR:noeudreseau xlink:href="id88c1acc7-e87d-4180-9c62-98c22aa4fbc3" />
      <RecoStaR:cableelectrique xlink:href="idf538040a-cd17-4587-acba-12c3f1af214a" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id4a10a576-f16a-4759-828d-732e55cd85ea">
      <RecoStaR:noeudreseau xlink:href="id49ca5edd-2836-458f-9c85-6d5b0a979fb4" />
      <RecoStaR:cableelectrique xlink:href="id7aa3064d-6c94-4173-8016-7e111d0e3fae" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id991f05d0-9d9f-4cea-8826-e3b0723ea5eb">
      <RecoStaR:noeudreseau xlink:href="id49ca5edd-2836-458f-9c85-6d5b0a979fb4" />
      <RecoStaR:cableelectrique xlink:href="id93304e8c-f2cc-40db-bd09-d8e250e1344f" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id97e43023-e1c4-4fe1-899b-464b3f392d06">
      <RecoStaR:cables xlink:href="id7aa3064d-6c94-4173-8016-7e111d0e3fae" />
      <RecoStaR:cheminement xlink:href="id470bee41-f616-4afa-aadf-f4e983d5fffd" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id0ea69256-853b-478a-8a9a-44a30429ae22">
      <RecoStaR:cables xlink:href="id93304e8c-f2cc-40db-bd09-d8e250e1344f" />
      <RecoStaR:cheminement xlink:href="id060bdd30-66cf-46bd-81d1-09924a98f36b" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="idfa40ee2d-8900-4cf1-80ae-3df12853c6f1">
      <RecoStaR:cables xlink:href="id649a929f-e1bc-441a-b103-0cf71f72a6eb" />
      <RecoStaR:cheminement xlink:href="idcb6d89f5-e09f-4d55-8927-d18e2cb49caf" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id8f41c974-16d0-40a5-8f7c-47ed45631e1a">
      <RecoStaR:cables xlink:href="ide6442815-249a-4660-adb9-4591885c3815" />
      <RecoStaR:cheminement xlink:href="id026297de-cc5c-45df-8db8-94a6025b9a54" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id1edd57ff-f463-4bf3-8086-5fa74444b74d">
      <RecoStaR:cables xlink:href="id7aa3064d-6c94-4173-8016-7e111d0e3fae" />
      <RecoStaR:cheminement xlink:href="id2947d314-63c9-48e7-843b-5d861bbbf234" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="idaf81c9e6-c322-4b9d-8c6e-7f9fd02659ac">
      <RecoStaR:cables xlink:href="id7aa3064d-6c94-4173-8016-7e111d0e3fae" />
      <RecoStaR:cheminement xlink:href="idb659ed22-4eb2-4d6b-8608-a9f7e4b344a8" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="idb5a58116-8a56-44df-80c5-193e616f9d15">
      <RecoStaR:cables xlink:href="id93304e8c-f2cc-40db-bd09-d8e250e1344f" />
      <RecoStaR:cheminement xlink:href="id43dc83ca-c0ca-4152-8296-3674a5a4c235" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id59cf6fb6-b30b-4cae-8476-ef02d1eb630a">
      <RecoStaR:cables xlink:href="idf538040a-cd17-4587-acba-12c3f1af214a" />
      <RecoStaR:cheminement xlink:href="id389ce7e8-c872-44a5-842a-40269b2fb3e2" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id722cfba7-eb0f-43c6-8c4a-eba46ce06c87">
      <RecoStaR:cables xlink:href="ide2d9d807-29ee-41e9-9516-b3a638f0aad7" />
      <RecoStaR:cheminement xlink:href="id9a9014d7-cf13-465b-85c7-e4c88782c2d6" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id5606d7ba-a367-49e4-8575-525caff1ff96">
      <RecoStaR:cables xlink:href="id13da8487-020c-4221-8ca9-a5f9bd7101bc" />
      <RecoStaR:cheminement xlink:href="id870b0cfa-6c00-470a-8a60-77bfb7e89bb1" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id5b9cf44a-e032-44e9-8840-fba2dfb3cb44">
      <RecoStaR:cables xlink:href="ideb2c23b9-9253-4bbb-88fa-f3a5ba42e2a6" />
      <RecoStaR:cheminement xlink:href="id6f9595e0-5698-473c-8e6f-de0f91b3f56d" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Metadata gml:id="idc7bd51cd-e8a2-481b-b7bc-c7cf4e21d662">
      <RecoStaR:Datecreation>2025-01-28</RecoStaR:Datecreation>
      <RecoStaR:Logiciel>QGIS OpenRecoStar Plugin</RecoStaR:Logiciel>
      <RecoStaR:Producteur>Enedis</RecoStaR:Producteur>
      <RecoStaR:Responsable>DT-CEN</RecoStaR:Responsable>
      <RecoStaR:SRS>EPSG:2154</RecoStaR:SRS>
    </RecoStaR:Metadata>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="id2ee62025-fe13-424c-870f-66180f2c67d5">
      <RecoStaR:ouvrage xlink:href="idad94456c-bd12-4302-9d20-c1ec7ecd9f47" />
      <RecoStaR:materiel xlink:href="id0c8bf02f-a38b-47e6-8101-2a45667b786f" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:ReseauUtilite gml:id="Reseau">
      <RecoStaR:Mention>Test export GML OpenRecoStar</RecoStaR:Mention>
      <RecoStaR:Nom>Réseau public de distribution</RecoStaR:Nom>
      <RecoStaR:Responsable>Enedis</RecoStaR:Responsable>
      <RecoStaR:Theme>ELECTRD</RecoStaR:Theme>
    </RecoStaR:ReseauUtilite>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Aerien_Reco gml:id="id6f9595e0-5698-473c-8e6f-de0f91b3f56d">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_Aerien_Reco_virt1.geom0"><gml:posList srsDimension="3">648294.2446 6479597.736 574.63 648282.3127 6479578.709 574.64</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:ModePose>Supporte</RecoStaR:ModePose>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_Aerien_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_BatimentTechnique_Reco gml:id="idad5fa9b7-2181-43f9-9b92-4548492da166">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="id03b4080d-edfe-45e9-967d-4103801e8855" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_1.geom0"><gml:pos>648293.839961249 6479610.81283971</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_BatimentTechnique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id649a929f-e1bc-441a-b103-0cf71f72a6eb">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>3</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="ide6442815-249a-4660-adb9-4591885c3815">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>3</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id7aa3064d-6c94-4173-8016-7e111d0e3fae">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">150</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">95</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id93304e8c-f2cc-40db-bd09-d8e250e1344f">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>LiaisonReseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">35</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idf538040a-cd17-4587-acba-12c3f1af214a">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">120</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="ideb2c23b9-9253-4bbb-88fa-f3a5ba42e2a6">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">70</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">54</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="ide2d9d807-29ee-41e9-9516-b3a638f0aad7">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>DerivationIndividuelle</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">35</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>Projected</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableTerre_Reco gml:id="id13da8487-020c-4221-8ca9-a5f9bd7101bc">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:noeudReseau xlink:href="id7e7b1321-f4a5-4e33-bb76-940edd770ba7" />
      <RecoStaR:FonctionCable xlink:href="MaltEquipot" />
      <RecoStaR:Materiau>Cuivre</RecoStaR:Materiau>
      <RecoStaR:NatureCableTerre xlink:href="CuivreNu" />
      <RecoStaR:Section uom="mm-2">25</RecoStaR:Section>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="idc7b792f7-94e5-4ce0-b210-87e9854d8640">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="idbb9d56fe-abfd-4e85-b154-c7053b69ea19" />
      <RecoStaR:FonctionCoffret xlink:href="Separable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_1.geom0"><gml:pos>648308.2338 6479626.216 574.95</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="SurSoclePolyester" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="RMBT450" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="id017f1016-7d77-481c-a1c9-8b46aca6d9e8">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="id622a1cc1-765b-48b4-863d-1e4faa2a56af" />
      <RecoStaR:FonctionCoffret xlink:href="Separable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_2.geom0"><gml:pos>648313.1267 6479623.274 574.94</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="SurSoclePolyester" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="CIBE" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CoupeCircuitAFusibles_Reco gml:id="id64b16fc1-7cd9-4107-bba0-f10e8e30dcc6">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id017f1016-7d77-481c-a1c9-8b46aca6d9e8" />
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CoupeCircuitAFusibles_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Fourreau_Reco gml:id="id470bee41-f616-4afa-aadf-f4e983d5fffd">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DiametreDuFourreau uom="mm">160</RecoStaR:DiametreDuFourreau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_Fourreau_Reco_1.geom0"><gml:posList srsDimension="3">648294.5501 6479610.759 574.99 648294.8559 6479610.847 574.1 648295.4066 6479611.137 573.81 648297.0956 6479612.165 573.55 648299.6446 6479615.543 573.46 648303.4427 6479619.209 573.44 648306.7611 6479622.676 573.42 648308.86 6479624.874 573.41 648308.7403 6479625.653 573.69 648308.3107 6479626.043 574.8</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:Materiau>PE</RecoStaR:Materiau>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_Fourreau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Fourreau_Reco gml:id="id060bdd30-66cf-46bd-81d1-09924a98f36b">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DiametreDuFourreau uom="mm">90</RecoStaR:DiametreDuFourreau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_Fourreau_Reco_2.geom0"><gml:posList srsDimension="3">648308.4606 6479626.223 574.91 648309.2999 6479625.743 574.92 648310.349 6479624.954 574.93 648313.1267 6479623.274 574.94</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:Materiau>PE</RecoStaR:Materiau>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_Fourreau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="id03b4080d-edfe-45e9-967d-4103801e8855">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_geomsupp_1.geom0"><gml:curveMember><gml:LineString gml:id="RPD_BatimentTechnique_Reco_geomsupp_1.geom0.0"><gml:posList srsDimension="3">648293.8165 6479610.173 574.69 648293.278 6479610.577 575.16 648293.8578 6479611.437 574.68 648294.4013 6479611.09 574.68 648293.8165 6479610.173 574.69</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_geomsupp_1.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">648293.8165 6479610.173 574.69 648293.278 6479610.577 575.16 648293.8578 6479611.437 574.68 648294.4013 6479611.09 574.68 648293.8165 6479610.173 574.69</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="idbb9d56fe-abfd-4e85-b154-c7053b69ea19">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_1.geom0"><gml:curveMember><gml:LineString gml:id="RPD_Coffret_Reco_geomsupp_1.geom0.0"><gml:posList srsDimension="3">648308.2338 6479626.216 574.95 648308.430617709 6479626.44241287 574.95 648308.279675793 6479626.57362468 574.95 648307.886040375 6479626.12079893 574.95 648308.036982291 6479625.98958713 574.95 648308.2338 6479626.216 574.95</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_1.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">648308.2338 6479626.216 574.95 648308.430617709 6479626.44241287 574.95 648308.279675793 6479626.57362468 574.95 648307.886040375 6479626.12079893 574.95 648308.036982291 6479625.98958713 574.95 648308.2338 6479626.216 574.95</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="id622a1cc1-765b-48b4-863d-1e4faa2a56af">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_2.geom0"><gml:curveMember><gml:LineString gml:id="RPD_Coffret_Reco_geomsupp_2.geom0.0"><gml:posList srsDimension="3">648313.1267 6479623.274 574.94 648312.918302489 6479623.05819806 574.94 648313.062170449 6479622.91926639 574.94 648313.478965471 6479623.35087027 574.94 648313.335097511 6479623.48980194 574.94 648313.1267 6479623.274 574.94</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_2.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">648313.1267 6479623.274 574.94 648312.918302489 6479623.05819806 574.94 648313.062170449 6479622.91926639 574.94 648313.478965471 6479623.35087027 574.94 648313.335097511 6479623.48980194 574.94 648313.1267 6479623.274 574.94</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="idad94456c-bd12-4302-9d20-c1ec7ecd9f47">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_1.geom0"><gml:pos>648282.5214 6479590.864 573.88</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Jonction</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="id144796c1-869c-47c6-b49e-b5979164ecae">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id2e6718e9-9ee1-4b82-954c-53121ca2ce66" />
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_2.geom0"><gml:pos>648272.5949 6479574.317 574.26</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>RemonteeAeroSouterraine</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="ide7732f16-be6d-4f7a-906b-3f1bc32b66e3">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id722f4806-d72f-4892-bd3a-ff584655bb70" />
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_3.geom0"><gml:pos>648294.2446 6479597.736 574.63</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>RemonteeAeroSouterraine</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="id64ee37ae-e895-415f-89d3-dccfd9ba8493">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_4.geom0"><gml:pos>648282.3127 6479578.709 574.64</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>ExtremiteReseau</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="id0c8bf02f-a38b-47e6-8101-2a45667b786f">
      <RecoStaR:Fabricant>SICAME</RecoStaR:Fabricant>
      <RecoStaR:Modele>M920660</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>SC24M920660021</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id870b0cfa-6c00-470a-8a60-77bfb7e89bb1">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_1.geom0"><gml:posList srsDimension="3">648293.0431 6479610.517 574.87 648293.8008 6479611.632 574.88 648294.5692 6479611.114 574.89 648293.9214 6479609.904 574.9 648293.0431 6479610.517 574.87</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="idcb6d89f5-e09f-4d55-8927-d18e2cb49caf">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_2.geom0"><gml:posList srsDimension="3">648272.5949 6479574.317 574.26 648273.9344 6479576.685 574.22 648275.3739 6479579.073 574.24 648277.8829 6479582.65 574.16 648279.7123 6479585.938 574.1 648282.5214 6479590.864 573.88</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id2947d314-63c9-48e7-843b-5d861bbbf234">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_3.geom0"><gml:posList srsDimension="3">648294.222319201 6479610.80950044 0 648294.2274 6479610.807 574.97 648294.5501 6479610.759 574.99</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="idb659ed22-4eb2-4d6b-8608-a9f7e4b344a8">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_4.geom0"><gml:posList srsDimension="3">648308.3107 6479626.043 574.8 648308.197666932 6479626.17440067 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id43dc83ca-c0ca-4152-8296-3674a5a4c235">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_5.geom0"><gml:posList srsDimension="3">648308.354841864 6479626.35619888 0 648308.4606 6479626.223 574.91</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id9a9014d7-cf13-465b-85c7-e4c88782c2d6">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_6.geom0"><gml:posList srsDimension="3">648313.36566413 6479623.03586455 0 648319.402888501 6479617.53630647 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>C</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>C</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id026297de-cc5c-45df-8db8-94a6025b9a54">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_7.geom0"><gml:posList srsDimension="3">648282.5214 6479590.864 573.88 648285.3602 6479594.72 573.88 648288.0092 6479598.777 573.74 648290.1584 6479602.404 573.63 648293.3572 6479607.26 573.51 648294.7067 6479609.088 573.59 648294.6408 6479609.596 574.101 648294.0293 6479610.394 574.09 648293.997 6479610.456 574.96</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id389ce7e8-c872-44a5-842a-40269b2fb3e2">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_8.geom0"><gml:posList srsDimension="3">648294.154676293 6479610.70363192 0 648294.1584 6479610.701 574.98 648294.3173 6479610.577 574.08 648294.9956 6479609.801 574.102 648295.1164 6479609.048 573.51 648293.607 6479606.89 573.49 648290.9581 6479603.213 573.62 648290.8979 6479602.114 573.68 648293.2155 6479598.916 573.69 648294.2446 6479597.736 574.63</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointDeComptage_Reco gml:id="id7b089c89-0d18-4e2a-980a-8262cbd74fec">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointDeComptage_Reco_1.geom0"><gml:pos>648321.492106095 6479616.08460748 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:NumeroPRM>12345678910</RecoStaR:NumeroPRM>
      <RecoStaR:PrecisionXY>C</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>C</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>Projected</RecoStaR:Statut>
    </RecoStaR:RPD_PointDeComptage_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id1cd39638-e3a8-40ae-b0f9-fde5620595cd">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_1.geom0"><gml:pos>648272.5949 6479574.317 574.26</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.26</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>HTA-1</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ide44f6ccd-db95-41cd-b60f-5d4cd55082e7">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_2.geom0"><gml:pos>648273.9344 6479576.685 574.22</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.22</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>HTA-2</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id1f9ac043-9522-424d-aa02-f7647bac5997">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_3.geom0"><gml:pos>648275.3739 6479579.073 574.24</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.24</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>HTA-3</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id1c12f989-d2c7-4a32-a9a8-92f36cd8753a">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_4.geom0"><gml:pos>648277.8829 6479582.65 574.16</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.16</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>HTA-5</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id4d928642-5a4a-41f3-8bba-d54a72354136">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_5.geom0"><gml:pos>648279.7123 6479585.938 574.1</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.1</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>HTA-6</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id889df5e8-d65e-4ebf-84a0-f8e2366ac3e7">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_6.geom0"><gml:pos>648282.5214 6479590.864 573.88</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.88</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>HTA-7</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id38d15f59-5415-4b73-a885-6692cb301775">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_7.geom0"><gml:pos>648285.3602 6479594.72 573.88</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.88</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>HTA-8</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idac66fc06-a82a-4677-a834-d7f45cfffc95">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_8.geom0"><gml:pos>648288.0092 6479598.777 573.74</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.74</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>HTA-9</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ida76c106a-aeb1-454b-9f63-7bdd3e674832">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_9.geom0"><gml:pos>648290.1584 6479602.404 573.63</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.63</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>HTA-10</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id8b76b76a-ecca-4ed7-bf1e-6a50756c5137">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_10.geom0"><gml:pos>648293.3572 6479607.26 573.51</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.51</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>HTA-11</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id8b5b25ac-5a27-47c5-b6dc-a8ec38b7067a">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_11.geom0"><gml:pos>648294.7067 6479609.088 573.59</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.59</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>HTA-12</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id49bf1ecf-f4ff-4886-bdeb-80f4e4e5d89f">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_12.geom0"><gml:pos>648294.0293 6479610.394 574.09</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.09</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>HTA-14</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idbd023701-53cc-45e5-9e4d-b76f58ed5104">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_13.geom0"><gml:pos>648294.4013 6479611.09 574.68</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.6799999999999</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>POSTE-1-4</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id73925c20-bf84-4f96-bcd7-b269644cc2c0">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_14.geom0"><gml:pos>648293.2155 6479598.916 573.69</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.6900000000001</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-3-8</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id703257dd-5de5-4c63-bdf5-91ee05e361c2">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_15.geom0"><gml:pos>648290.8979 6479602.114 573.68</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.6799999999999</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-3-7</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id7bdb148b-f80d-4346-927a-bc487c8d0a5c">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_16.geom0"><gml:pos>648290.9581 6479603.213 573.62</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.62</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-3-6</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id26319e2e-e125-4bc1-be8b-d81b4f07bb26">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_17.geom0"><gml:pos>648293.607 6479606.89 573.49</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.49</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-3-5</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idf7943810-79be-4c68-be00-83cf6e9dce2c">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_18.geom0"><gml:pos>648295.1164 6479609.048 573.51</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.51</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-3-4</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ida430bcfd-175d-4c08-8949-9e519d7a5792">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_19.geom0"><gml:pos>648294.3173 6479610.577 574.08</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.08</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-3-2</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id845354dc-67e5-475d-a571-97d05d14befe">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_20.geom0"><gml:pos>648293.8165 6479610.173 574.69</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.6900000000001</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>POSTE-1-1</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id8dddbdcd-aa9d-48f3-825e-8fa48329541c">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_21.geom0"><gml:pos>648293.8578 6479611.437 574.68</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.6799999999999</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>POSTE-1-3</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idbf3de663-e85f-4dd0-9e3b-c3647c1185d7">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_22.geom0"><gml:pos>648295.4066 6479611.137 573.81</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.8099999999999</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-1-4</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id5f1cb5a5-4e3f-4911-88f3-c249e798b533">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_23.geom0"><gml:pos>648297.0956 6479612.165 573.55</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.55</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-1-5</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id030510d5-4f15-4512-b02f-030fc9462b83">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_24.geom0"><gml:pos>648299.6446 6479615.543 573.46</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.46</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-1-6</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id6be645f8-fd80-4694-9d7d-2fc692ed825c">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_25.geom0"><gml:pos>648303.4427 6479619.209 573.44</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.4400000000001</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-1-7</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id2e6c38b8-1d8e-459f-aba3-31545a807262">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_26.geom0"><gml:pos>648306.7611 6479622.676 573.42</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.42</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-1-8</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idf7858397-e357-4b7a-bd03-fe597d5954ab">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_27.geom0"><gml:pos>648308.86 6479624.874 573.41</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.41</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-1-9</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id980365a2-acf5-42c8-b453-9acd3abb806a">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_28.geom0"><gml:pos>648308.7403 6479625.653 573.69</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">573.6900000000001</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-1-10</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id03ac5199-4c30-4bbe-8c78-ce164b16af45">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_29.geom0"><gml:pos>648293.278 6479610.577 575.16</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">575.16</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>POSTE 1-2</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id18b97491-d170-41e2-a386-b9989f11f7a8">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_30.geom0"><gml:pos>648308.3107 6479626.043 574.8</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.8</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-1-11</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idb013ba39-2665-4adc-92f6-0fa430b31744">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_31.geom0"><gml:pos>648293.0431 6479610.517 574.87</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.87</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>MALT-1-2</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id0eb002a5-34e4-4b40-be9e-a83a681187e6">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_32.geom0"><gml:pos>648293.8008 6479611.632 574.88</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.88</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>MAT-1-3</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idcb15a294-44cf-42c0-bbbc-d37a734a2cda">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_33.geom0"><gml:pos>648294.5692 6479611.114 574.89</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.89</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>MALT-1-4</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idd523b556-4e75-461b-a904-0e60cdf9a124">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_34.geom0"><gml:pos>648293.9214 6479609.904 574.9</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.9</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>MALT-1-1</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id7f375d67-2c8e-4679-8745-ad6d3b5fcbe5">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_35.geom0"><gml:pos>648308.4606 6479626.223 574.91</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.91</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-2-1</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id5d5d8899-9f0c-4b37-b831-efc790570318">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_36.geom0"><gml:pos>648309.2999 6479625.743 574.92</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.92</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-2-2</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id902bb777-bef5-4887-b855-98d76e18c70f">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_37.geom0"><gml:pos>648310.349 6479624.954 574.93</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.9299999999999</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-2-3</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idebfae600-22c0-43d4-9c7e-5ca1449ab51f">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_38.geom0"><gml:pos>648313.1267 6479623.274 574.94</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.9400000000001</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>CIBE-1</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id9eb610b1-4782-46b0-9763-c1a99b9df1e7">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_39.geom0"><gml:pos>648308.2338 6479626.216 574.95</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.95</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>REMBT450-1</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id60cdecb6-597f-41e0-bf72-2c1fa9afc8a5">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_40.geom0"><gml:pos>648293.997 6479610.456 574.96</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.96</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>HTA-15</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id3a102915-4f18-4c5f-9576-9d0d94cbc244">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_41.geom0"><gml:pos>648294.2274 6479610.807 574.97</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.97</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-1-1</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idedb5c6f3-d854-420b-8f4b-5b6dc5b7990a">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_42.geom0"><gml:pos>648294.1584 6479610.701 574.98</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.98</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-3-1</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id2c9b2590-e6f7-44a8-a99a-8530dc0c3998">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_43.geom0"><gml:pos>648294.5501 6479610.759 574.99</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.99</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-1-2</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idbe2f17a1-4234-469e-9cbd-96e6f44d8d9f">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_44.geom0"><gml:pos>648294.8559 6479610.847 574.1</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.1</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-1-3</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id762678a1-148f-4422-aed2-1e77d8ff468c">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_45.geom0"><gml:pos>648294.6408 6479609.596 574.101</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.101</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>HTA-13</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id9e00d5fb-28d9-4924-81cb-3283daed8509">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_46.geom0"><gml:pos>648294.9956 6479609.801 574.102</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.102</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-3-3</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id814e4393-4246-493a-b7a1-e27dcbfcc784">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_47.geom0"><gml:pos>648294.2446 6479597.736 574.63</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.63</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-3-9</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ide1404b1e-3653-4c14-a82c-c72a8a628c6f">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_48.geom0"><gml:pos>648282.3127 6479578.709 574.64</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">574.64</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>BT-3-10</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PosteElectrique_Reco gml:id="id88c1acc7-e87d-4180-9c62-98c22aa4fbc3">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idad5fa9b7-2181-43f9-9b92-4548492da166" />
      <RecoStaR:Categorie xlink:href="Distribution" />
      <RecoStaR:Code>19456P0023</RecoStaR:Code>
      <RecoStaR:InformationSupplementaire>HECTOR</RecoStaR:InformationSupplementaire>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypePoste xlink:href="UP" />
    </RecoStaR:RPD_PosteElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Support_Reco gml:id="id2e6718e9-9ee1-4b82-954c-53121ca2ce66">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Classe xlink:href="D" />
      <RecoStaR:Effort uom="kN">10</RecoStaR:Effort>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Support_Reco_1.geom0"><gml:pos>648272.5949 6479574.317 574.26</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:HauteurPoteau uom="m">12</RecoStaR:HauteurPoteau>
      <RecoStaR:Matiere xlink:href="Beton" />
      <RecoStaR:NatureSupport xlink:href="Autre" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_Support_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Support_Reco gml:id="id722f4806-d72f-4892-bd3a-ff584655bb70">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Support_Reco_2.geom0"><gml:pos>648294.2446 6479597.736 574.63</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:NatureSupport xlink:href="Facade" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_Support_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_SupportModules_Reco gml:id="id49ca5edd-2836-458f-9c85-6d5b0a979fb4">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idc7b792f7-94e5-4ce0-b210-87e9854d8640" />
      <RecoStaR:NombrePlages>9</RecoStaR:NombrePlages>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_SupportModules_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Terre_Reco gml:id="id7e7b1321-f4a5-4e33-bb76-940edd770ba7">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:NatureTerre xlink:href="TerreMasses" />
      <RecoStaR:Resistance uom="ohms">13</RecoStaR:Resistance>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_Terre_Reco>
  </gml:featureMember>
</gml:FeatureCollection>
