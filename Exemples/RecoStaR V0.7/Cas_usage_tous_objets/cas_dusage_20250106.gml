<?xml version="1.0" encoding="utf-8" ?>
<gml:FeatureCollection
    xmlns:ogr_gmlas="http://gdal.org/ogr/gmlas"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:RecoStaR="http://StaR-Elec.com"
    xmlns:gco="http://www.isotc211.org/2005/gco"
    xmlns:gmd="http://www.isotc211.org/2005/gmd"
    xmlns:gsr="http://www.isotc211.org/2005/gsr"
    xmlns:gss="http://www.isotc211.org/2005/gss"
    xmlns:gts="http://www.isotc211.org/2005/gts"
    xmlns:gml="http://www.opengis.net/gml/3.2"
    xmlns:gmlexr="http://www.opengis.net/gml/3.3/exr"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xsi:schemaLocation="http://StaR-Elec.com https://gitlab.com/StaR-Elec/StaR-Elec/-/raw/main/RecoStaR/SchemaStarElecRecoStar.xsd" >
<!-- GML au format OpenRecoStar v0.7 via GDAL / QGIS. -->
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id4eb4c2ff-aa19-40c4-82cb-a844226c02e5">
      <RecoStaR:noeudreseau xlink:href="id42fd1b78-6d77-4bd4-ba4d-8c7ce570f8ce" />
      <RecoStaR:cableelectrique xlink:href="id06eaaa70-79eb-4117-ae62-1f1eac839bbf" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id0c4f8af4-3c20-469f-8c82-16b2266791c0">
      <RecoStaR:noeudreseau xlink:href="id0e46458b-9f1c-4205-a9c5-bd47b549be9b" />
      <RecoStaR:cableelectrique xlink:href="id0c1f7895-218a-4037-ae54-b205600dd513" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id4199d9a3-a741-4e5f-8f17-2c3ed36ab188">
      <RecoStaR:noeudreseau xlink:href="id72070f65-c788-4852-acf8-e51890105137" />
      <RecoStaR:cableelectrique xlink:href="id1265e1fa-804c-48c7-b094-c5873f4ae4ac" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idd800f0f0-306f-4b7f-855e-9dcbc7220f31">
      <RecoStaR:noeudreseau xlink:href="ided3a479b-8cce-4caa-9fa7-7c5685970811" />
      <RecoStaR:cableelectrique xlink:href="id184e70cf-4662-4ff4-9a0e-6f9ac56c0a8e" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id8789bbe6-a624-45da-87ff-16653b525025">
      <RecoStaR:noeudreseau xlink:href="id0e46458b-9f1c-4205-a9c5-bd47b549be9b" />
      <RecoStaR:cableelectrique xlink:href="id208103a4-8aae-42f5-8e6a-a1993e4f3342" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id8dde2f4c-8ae0-4e0b-82b7-8f8dd4cac5bc">
      <RecoStaR:noeudreseau xlink:href="id0e46458b-9f1c-4205-a9c5-bd47b549be9b" />
      <RecoStaR:cableelectrique xlink:href="id29ef0bb6-95fa-4b68-b1fe-cfef2b32b20d" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id0fac444c-18c7-447e-8c12-e5b59a54e05a">
      <RecoStaR:noeudreseau xlink:href="idd595b5c1-34e0-4018-b555-827e6a3c08c3" />
      <RecoStaR:cableelectrique xlink:href="id2ac82f92-9128-4697-a1c6-8fa0de7061d8" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idc1013b50-4032-45a3-8bef-1e526a1ad0da">
      <RecoStaR:noeudreseau xlink:href="id88f9d91c-c399-4fae-8ab8-bbe357f88487" />
      <RecoStaR:cableelectrique xlink:href="id2ac82f92-9128-4697-a1c6-8fa0de7061d8" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idfe2135c5-2c63-4274-8f25-3bb098e0ecc6">
      <RecoStaR:noeudreseau xlink:href="id7783e3d9-164a-4e44-80ba-28ef30e55cc8" />
      <RecoStaR:cableelectrique xlink:href="id37e6418a-3085-44c4-b8c6-55e99887c90b" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id5c56f6d2-7987-4243-87f9-101158249f89">
      <RecoStaR:noeudreseau xlink:href="id3def5f9c-7e91-4397-b636-7ceb74a8587b" />
      <RecoStaR:cableelectrique xlink:href="id7af7785b-d42c-43db-b39f-01e0e5910adc" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idf1b0d6fb-12ba-4e2d-8b36-05707d059319">
      <RecoStaR:noeudreseau xlink:href="idfb1e6c98-581f-4937-9180-497b36aaf4de" />
      <RecoStaR:cableelectrique xlink:href="id7cdaff9b-4a75-491a-83d8-8b4464e59796" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id74456308-cc03-45c1-8801-8eb5fc308db1">
      <RecoStaR:noeudreseau xlink:href="id5d514584-e861-4b7f-8cdd-af7282f7ed34" />
      <RecoStaR:cableelectrique xlink:href="id8828897c-9ae8-433d-9508-feb7b924563d" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="ide2761d45-add7-44af-8916-80e3777399e9">
      <RecoStaR:noeudreseau xlink:href="iddbbc8aae-65ac-4774-b0e6-eb4e46156caa" />
      <RecoStaR:cableelectrique xlink:href="id8b72cef8-bfb1-4a0d-86dc-191e9d55cb41" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="ide330bb7b-818b-4b40-8664-7cc3bf0d5bb1">
      <RecoStaR:noeudreseau xlink:href="idf4ec2054-9ce9-47fe-9bf4-a26fc72deaf3" />
      <RecoStaR:cableelectrique xlink:href="id97b41743-bb44-4856-b13e-7579412fa184" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id0e88cfd7-a406-4afa-8015-ee0896384591">
      <RecoStaR:noeudreseau xlink:href="ida7b8bed2-e36d-41f4-b730-b1ffe10f930b" />
      <RecoStaR:cableelectrique xlink:href="id9e5a4895-2755-4472-98e6-1bdd09b3b264" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id43fc2c2c-bb8b-4317-834e-4e8101da0902">
      <RecoStaR:noeudreseau xlink:href="ida7b8bed2-e36d-41f4-b730-b1ffe10f930b" />
      <RecoStaR:cableelectrique xlink:href="idadfae3ad-d4c1-40af-b052-59abe24676ee" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id9afc7cbc-008d-44f4-8b19-2edb5ba00a7c">
      <RecoStaR:noeudreseau xlink:href="id88f9d91c-c399-4fae-8ab8-bbe357f88487" />
      <RecoStaR:cableelectrique xlink:href="idafb87fa5-75eb-4b38-bdf0-5c8576922dfd" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idf048ebdb-76c7-43ea-8b85-04c9c4c5771b">
      <RecoStaR:noeudreseau xlink:href="id8864bf27-5431-4f4a-a869-579872ac3c20" />
      <RecoStaR:cableelectrique xlink:href="idc2c492ea-3fb5-458d-9348-fc5d996768ed" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id1f0db376-54e5-416c-8f09-e791fd1ddc94">
      <RecoStaR:noeudreseau xlink:href="ida483077c-8c17-4559-9253-f46fb64997f6" />
      <RecoStaR:cableelectrique xlink:href="idc2c492ea-3fb5-458d-9348-fc5d996768ed" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id2046e783-179c-4404-8cb7-a3fa1d382ced">
      <RecoStaR:noeudreseau xlink:href="id12adaa6a-fa05-4789-9897-543810b783f5" />
      <RecoStaR:cableelectrique xlink:href="idd2382a89-60c3-4ef2-90f7-5b8ac58b3f20" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id4211ac39-5695-49fd-8a58-e8304b968871">
      <RecoStaR:noeudreseau xlink:href="idae8810b3-6697-4d4a-8aea-03ce8343d766" />
      <RecoStaR:cableelectrique xlink:href="iddab5b6bb-89fe-4c62-8f66-a654a740ace5" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id39341e76-5381-4699-8c48-6d071c626078">
      <RecoStaR:noeudreseau xlink:href="id5d514584-e861-4b7f-8cdd-af7282f7ed34" />
      <RecoStaR:cableelectrique xlink:href="idf2506dca-7a2c-4871-9149-04169738882b" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id099be9bc-dc9a-4e8c-86c4-56fdfa33c57b">
      <RecoStaR:noeudreseau xlink:href="ided3a479b-8cce-4caa-9fa7-7c5685970811" />
      <RecoStaR:cableelectrique xlink:href="idf450688b-47be-4f79-bd20-9d4ff94af9e4" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id17453588-a03e-49c7-8183-82a573549954">
      <RecoStaR:noeudreseau xlink:href="idae8810b3-6697-4d4a-8aea-03ce8343d766" />
      <RecoStaR:cableelectrique xlink:href="idf45fb6ff-f103-4d79-8f92-39d17e48abf8" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id6ac083ca-a61c-4f80-8975-d5a3cfa3731b">
      <RecoStaR:noeudreseau xlink:href="ided3a479b-8cce-4caa-9fa7-7c5685970811" />
      <RecoStaR:cableelectrique xlink:href="idf6c8f835-fd4b-4f2b-ab63-248713ad279a" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id4b316d2c-6915-489d-8093-307573942abe">
      <RecoStaR:noeudreseau xlink:href="id40b07043-0a50-4923-ab28-c226c916abed" />
      <RecoStaR:cableelectrique xlink:href="id4abb14ce-ed92-411c-808c-a5e62cc13766" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id5d649097-ba03-4927-8e53-e6adc41d4945">
      <RecoStaR:noeudreseau xlink:href="idcd346965-1a53-4b0d-a05c-96bf632eda8a" />
      <RecoStaR:cableelectrique xlink:href="id8b72cef8-bfb1-4a0d-86dc-191e9d55cb41" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id568ee726-ff84-4890-8476-2af487335f2c">
      <RecoStaR:noeudreseau xlink:href="idcd346965-1a53-4b0d-a05c-96bf632eda8a" />
      <RecoStaR:cableelectrique xlink:href="id97b41743-bb44-4856-b13e-7579412fa184" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idc9610384-e2a1-4b2c-88ef-e99cbad6649f">
      <RecoStaR:noeudreseau xlink:href="ida7278456-a3cc-41d6-bb3f-8379bd39be93" />
      <RecoStaR:cableelectrique xlink:href="id9e5a4895-2755-4472-98e6-1bdd09b3b264" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idafb9c57e-c9b5-4b48-8662-fe17957d118b">
      <RecoStaR:noeudreseau xlink:href="ida7278456-a3cc-41d6-bb3f-8379bd39be93" />
      <RecoStaR:cableelectrique xlink:href="idc070fca8-05fd-457a-b964-4941b1aa0e99" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id2d63687a-02cd-4dc4-8c02-12ea2320af01">
      <RecoStaR:noeudreseau xlink:href="ida7278456-a3cc-41d6-bb3f-8379bd39be93" />
      <RecoStaR:cableelectrique xlink:href="idf2506dca-7a2c-4871-9149-04169738882b" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idd7ea0978-ed95-4879-8e8f-53cf42630ba1">
      <RecoStaR:noeudreseau xlink:href="ide2b736ca-7b56-4b81-8492-8c6b10e22360" />
      <RecoStaR:cableelectrique xlink:href="idc070fca8-05fd-457a-b964-4941b1aa0e99" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idaf16a6a8-fe5d-43c0-8239-d76040d2584a">
      <RecoStaR:noeudreseau xlink:href="idb2223b97-9609-4d77-96e0-c52eecf1f898" />
      <RecoStaR:cableelectrique xlink:href="id4abb14ce-ed92-411c-808c-a5e62cc13766" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id456c6dba-926b-4e7e-8989-1e16031d0f48">
      <RecoStaR:noeudreseau xlink:href="idb2223b97-9609-4d77-96e0-c52eecf1f898" />
      <RecoStaR:cableelectrique xlink:href="iddab5b6bb-89fe-4c62-8f66-a654a740ace5" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idb53e312c-05a8-41b8-8696-01dfe4b07510">
      <RecoStaR:noeudreseau xlink:href="id849e314b-30f0-488a-9ce3-45f54843ff1c" />
      <RecoStaR:cableelectrique xlink:href="id1265e1fa-804c-48c7-b094-c5873f4ae4ac" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idff09d4ea-58f9-472a-8da0-6563caf74c93">
      <RecoStaR:noeudreseau xlink:href="id849e314b-30f0-488a-9ce3-45f54843ff1c" />
      <RecoStaR:cableelectrique xlink:href="id37e6418a-3085-44c4-b8c6-55e99887c90b" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id46bbdade-bc2c-4cae-85a7-751ace0cc9b9">
      <RecoStaR:noeudreseau xlink:href="id849e314b-30f0-488a-9ce3-45f54843ff1c" />
      <RecoStaR:cableelectrique xlink:href="id70f4eb55-e987-47bb-810c-c8797308ce3d" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id9293a7d6-c58f-4937-8c30-9b9ba5254f43">
      <RecoStaR:noeudreseau xlink:href="idd6520913-02fc-44d2-96d7-b70e2b4dd9b6" />
      <RecoStaR:cableelectrique xlink:href="id06eaaa70-79eb-4117-ae62-1f1eac839bbf" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id5b4f1400-9e2b-406c-8fdd-4b35468cd579">
      <RecoStaR:noeudreseau xlink:href="idd6520913-02fc-44d2-96d7-b70e2b4dd9b6" />
      <RecoStaR:cableelectrique xlink:href="id637fce18-64ee-4972-a3cc-d2833430ef48" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id1daafca9-c628-486c-84a1-eacaf2193e38">
      <RecoStaR:noeudreseau xlink:href="idd6520913-02fc-44d2-96d7-b70e2b4dd9b6" />
      <RecoStaR:cableelectrique xlink:href="id7cdaff9b-4a75-491a-83d8-8b4464e59796" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idfcedceb2-ef6d-4827-820d-8ac222719401">
      <RecoStaR:noeudreseau xlink:href="ide60da224-0312-4201-985e-b8f01a79a046" />
      <RecoStaR:cableelectrique xlink:href="id29ef0bb6-95fa-4b68-b1fe-cfef2b32b20d" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id431270a4-3116-43d1-8d57-bcb771a0007e">
      <RecoStaR:noeudreseau xlink:href="ide60da224-0312-4201-985e-b8f01a79a046" />
      <RecoStaR:cableelectrique xlink:href="id7af7785b-d42c-43db-b39f-01e0e5910adc" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id4fa0ae54-ac74-4a1f-81c8-bbfc062f374f">
      <RecoStaR:noeudreseau xlink:href="idff66d7aa-7bb9-47bf-8e0c-9bf1c0e78b7e" />
      <RecoStaR:cableelectrique xlink:href="id70f4eb55-e987-47bb-810c-c8797308ce3d" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id79a0654e-66e3-4e98-80f5-0615ccd4392f">
      <RecoStaR:noeudreseau xlink:href="idff66d7aa-7bb9-47bf-8e0c-9bf1c0e78b7e" />
      <RecoStaR:cableelectrique xlink:href="idd2382a89-60c3-4ef2-90f7-5b8ac58b3f20" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id78dd28cd-8f57-49ba-89c4-eb961a267d49">
      <RecoStaR:noeudreseau xlink:href="id3f2c665d-92ff-44db-b45a-4809f376f87a" />
      <RecoStaR:cableelectrique xlink:href="id637fce18-64ee-4972-a3cc-d2833430ef48" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id415aff25-8d6c-4aab-8d77-064cb04e6610">
      <RecoStaR:noeudreseau xlink:href="id3f2c665d-92ff-44db-b45a-4809f376f87a" />
      <RecoStaR:cableelectrique xlink:href="idc2c492ea-3fb5-458d-9348-fc5d996768ed" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idee8be4e2-88dc-415f-83b8-93259c6d56bb">
      <RecoStaR:noeudreseau xlink:href="id88f9d91c-c399-4fae-8ab8-bbe357f88487" />
      <RecoStaR:cableelectrique xlink:href="idafb87fa5-75eb-4b38-bdf0-5c8576922dfd" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idb1657153-e0c2-4daa-896c-5845436aa550">
      <RecoStaR:noeudreseau xlink:href="idd9460d88-c1e9-4e84-bec8-54690c62502d" />
      <RecoStaR:cableelectrique xlink:href="idf450688b-47be-4f79-bd20-9d4ff94af9e4" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="ide0ac1194-6175-4398-82e1-b87d834ae016">
      <RecoStaR:cables xlink:href="idc070fca8-05fd-457a-b964-4941b1aa0e99" />
      <RecoStaR:cheminement xlink:href="id0d2d7f8f-f05c-419c-b90c-9a9aa8b0b38c" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id3e865f7c-e9dc-4cc9-871a-97e7a2ed2f4b">
      <RecoStaR:cables xlink:href="idf45fb6ff-f103-4d79-8f92-39d17e48abf8" />
      <RecoStaR:cheminement xlink:href="id9f2af28e-b8b6-4907-83ae-add9490bff79" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id6d776ba6-ed18-48c5-8a85-52a47885dfd2">
      <RecoStaR:cables xlink:href="iddab5b6bb-89fe-4c62-8f66-a654a740ace5" />
      <RecoStaR:cheminement xlink:href="idb7229766-83a0-4e4d-848c-8eacc5499fd8" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id2bdbf9ee-328f-42ff-806d-920bdb6fbdff">
      <RecoStaR:cables xlink:href="id4abb14ce-ed92-411c-808c-a5e62cc13766" />
      <RecoStaR:cheminement xlink:href="id6be51ee7-37b6-4e47-8355-dc83109794ef" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="idabe8f622-9255-4eed-8b3f-59381e42f868">
      <RecoStaR:cables xlink:href="idf450688b-47be-4f79-bd20-9d4ff94af9e4" />
      <RecoStaR:cheminement xlink:href="id4d435ac4-281e-4c17-8c7f-55955c7a192d" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id2ada1628-a7f0-4b9d-80f9-48a6e0e17176">
      <RecoStaR:cables xlink:href="idf6c8f835-fd4b-4f2b-ab63-248713ad279a" />
      <RecoStaR:cheminement xlink:href="id564e1280-3a42-43a6-8a1b-fcefdfef6611" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="idd52ce004-c86d-45ba-80a3-2e8a8b35e8cc">
      <RecoStaR:cables xlink:href="id184e70cf-4662-4ff4-9a0e-6f9ac56c0a8e" />
      <RecoStaR:cheminement xlink:href="idaf1b2f96-0a45-4714-879a-d1077a8045ce" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="idf6979627-5b87-4611-8881-2baf3659934b">
      <RecoStaR:cables xlink:href="id208103a4-8aae-42f5-8e6a-a1993e4f3342" />
      <RecoStaR:cheminement xlink:href="id581ef232-7b37-4ff4-847d-e87f075a4be5" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id77360dd8-017d-49a4-8906-99645b662a56">
      <RecoStaR:cables xlink:href="id0c1f7895-218a-4037-ae54-b205600dd513" />
      <RecoStaR:cheminement xlink:href="idc821ee06-0d54-4010-80d2-988f7d181c36" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id386c7739-27de-44ce-80be-1ecac5d42c4a">
      <RecoStaR:cables xlink:href="id29ef0bb6-95fa-4b68-b1fe-cfef2b32b20d" />
      <RecoStaR:cheminement xlink:href="idd3872a31-45e4-46f6-88d1-9d3ab98354ce" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="idc288a399-9d72-445d-8094-8d81431bb043">
      <RecoStaR:cables xlink:href="id7af7785b-d42c-43db-b39f-01e0e5910adc" />
      <RecoStaR:cheminement xlink:href="id9f30c69d-7302-4ccd-8602-7e061123c36f" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id5ccdb0b8-67a7-4c30-8873-f56fa8019a14">
      <RecoStaR:cables xlink:href="idadfae3ad-d4c1-40af-b052-59abe24676ee" />
      <RecoStaR:cheminement xlink:href="id229ebec0-86c0-4086-8c0e-a76fd777a3f8" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="ideebb4d7a-7347-490e-8dac-7fe2321c6a09">
      <RecoStaR:cables xlink:href="id8828897c-9ae8-433d-9508-feb7b924563d" />
      <RecoStaR:cheminement xlink:href="id46beaa40-8f3f-4a03-8fa8-f1ece6c60f5b" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="idc57e6a9c-379d-4f0b-816e-6816169a16c4">
      <RecoStaR:cables xlink:href="id9e5a4895-2755-4472-98e6-1bdd09b3b264" />
      <RecoStaR:cheminement xlink:href="id3ae51080-1fa1-4e11-8fe7-3d06881983ce" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="idf8b60110-b46a-4e0d-8f72-53e1eb04118f">
      <RecoStaR:cables xlink:href="idf2506dca-7a2c-4871-9149-04169738882b" />
      <RecoStaR:cheminement xlink:href="idcf59aee1-6893-4064-8564-7ba29894bdeb" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id930c93a7-5c5b-4388-840b-c21aefa6b976">
      <RecoStaR:cables xlink:href="idc070fca8-05fd-457a-b964-4941b1aa0e99" />
      <RecoStaR:cheminement xlink:href="idcc0e0da9-9a01-4c5a-8fe9-16cbbf69b47e" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id61dd3f4a-7eb6-4ef0-8a97-def131b8d7a6">
      <RecoStaR:cables xlink:href="idc070fca8-05fd-457a-b964-4941b1aa0e99" />
      <RecoStaR:cheminement xlink:href="ide8dda9e9-a9d7-45a1-81be-d1a9c9d730e7" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id9696330c-9417-4e32-8e85-341b6f0efc66">
      <RecoStaR:cables xlink:href="id37e6418a-3085-44c4-b8c6-55e99887c90b" />
      <RecoStaR:cheminement xlink:href="idd0cb082e-7647-48fb-8128-8a0e85ade40a" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id4864e634-9d8c-4965-8ba2-f7b658491fae">
      <RecoStaR:cables xlink:href="id1265e1fa-804c-48c7-b094-c5873f4ae4ac" />
      <RecoStaR:cheminement xlink:href="id66191251-e128-4f1b-8244-cce8bbb1ed3f" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="ide2c6495e-0112-47e7-8c64-55eb9c0ac1f7">
      <RecoStaR:cables xlink:href="id70f4eb55-e987-47bb-810c-c8797308ce3d" />
      <RecoStaR:cheminement xlink:href="id6c44af58-aa7e-4bbf-8bf8-d199bcdbdeeb" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id940fe0fc-13e8-4b04-8746-aae702e5b6a2">
      <RecoStaR:cables xlink:href="idd2382a89-60c3-4ef2-90f7-5b8ac58b3f20" />
      <RecoStaR:cheminement xlink:href="ida33e3923-4ed6-4fa5-84b2-0ff4148dd62d" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id27bcb4fb-1de3-4454-8c4e-ccd31e448051">
      <RecoStaR:cables xlink:href="id97b41743-bb44-4856-b13e-7579412fa184" />
      <RecoStaR:cheminement xlink:href="id9483f95a-6f00-44a8-81f8-0c3f7f65ae15" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id8c35f10e-b031-4e08-885b-4ddfe3ccff78">
      <RecoStaR:cables xlink:href="id8b72cef8-bfb1-4a0d-86dc-191e9d55cb41" />
      <RecoStaR:cheminement xlink:href="id781a2686-5b8b-44ba-898b-44429197715b" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="idc1720bf1-9fad-4559-8654-9999638f632b">
      <RecoStaR:cables xlink:href="id7cdaff9b-4a75-491a-83d8-8b4464e59796" />
      <RecoStaR:cheminement xlink:href="id9f169f43-35fc-46e0-8ec9-20e1573a00da" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id073bd471-6851-4d59-8865-6dc20266b31b">
      <RecoStaR:cables xlink:href="id06eaaa70-79eb-4117-ae62-1f1eac839bbf" />
      <RecoStaR:cheminement xlink:href="id9e4cab11-a024-48bc-83da-38729078acd8" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="idc889f65d-683a-44f9-82ac-d19f2b58e751">
      <RecoStaR:cables xlink:href="id637fce18-64ee-4972-a3cc-d2833430ef48" />
      <RecoStaR:cheminement xlink:href="idb33f2d57-7e31-4af6-88ca-53b2ab43cd61" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="idd2b9fe95-230f-4d68-8211-d77ff78059ac">
      <RecoStaR:cables xlink:href="idc2c492ea-3fb5-458d-9348-fc5d996768ed" />
      <RecoStaR:cheminement xlink:href="id3de8ef41-78f1-4a8d-8250-ba1b9d260fe3" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id2479f8e0-a8d8-40ea-8afa-7f84b1229ee9">
      <RecoStaR:cables xlink:href="idafb87fa5-75eb-4b38-bdf0-5c8576922dfd" />
      <RecoStaR:cheminement xlink:href="id4179a5e8-da74-4875-829a-2c8736f1a843" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id3a33678a-15f8-46ed-87ac-e3b394da6d99">
      <RecoStaR:cables xlink:href="id2ac82f92-9128-4697-a1c6-8fa0de7061d8" />
      <RecoStaR:cheminement xlink:href="id109882f6-a626-4f22-8b18-6f81c3392044" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="ida2e13b56-ca0a-41a8-8705-3b2d7c8663d8">
      <RecoStaR:cables xlink:href="idb585dcea-c8f0-4d99-b821-03b118900025" />
      <RecoStaR:cheminement xlink:href="id2fb5f4b5-1d21-446f-8a2d-7565fa5ad320" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="idb391de65-08ae-419b-8401-523c54eeeb60">
      <RecoStaR:cables xlink:href="id12f5544d-5e20-440e-8058-73209353b51d" />
      <RecoStaR:cheminement xlink:href="id948fae7a-2c91-4775-8b56-d420ca32a23e" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id25ae4ae4-8dbe-42d6-8502-1860e79db632">
      <RecoStaR:cables xlink:href="idd46675da-8528-4301-b232-c233ffa1d83b" />
      <RecoStaR:cheminement xlink:href="id185a64f9-5f50-491c-8f8f-55d68b94e271" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Metadata gml:id="id24030577-5f36-4d79-8bff-ac2756285319">
      <RecoStaR:Datecreation>2025-01-06</RecoStaR:Datecreation>
      <RecoStaR:Logiciel>QGIS OpenRecoStar Plugin</RecoStaR:Logiciel>
      <RecoStaR:Producteur>GM</RecoStaR:Producteur>
      <RecoStaR:Responsable>GM</RecoStaR:Responsable>
      <RecoStaR:SRS>EPSG:2154</RecoStaR:SRS>
    </RecoStaR:Metadata>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="idcfe13dca-5537-4761-8999-699ca9fd4f17">
      <RecoStaR:ouvrage xlink:href="ided3a479b-8cce-4caa-9fa7-7c5685970811" />
      <RecoStaR:materiel xlink:href="idf469a17e-171f-4374-8a1b-85302189b0ff" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="idcb1903eb-5af5-4e0b-8a0a-73090fbc45fc">
      <RecoStaR:ouvrage xlink:href="id0e46458b-9f1c-4205-a9c5-bd47b549be9b" />
      <RecoStaR:materiel xlink:href="id842c0f72-1757-4a6f-8080-4f4e35a1ff99" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="id4cad8bc6-2062-46b9-878b-97d60fb98072">
      <RecoStaR:ouvrage xlink:href="ida7b8bed2-e36d-41f4-b730-b1ffe10f930b" />
      <RecoStaR:materiel xlink:href="id44b8c864-f1aa-490d-b11e-5d0d5aea34bd" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="id4af62252-a1ee-4fe2-88aa-905fdbe2b65a">
      <RecoStaR:ouvrage xlink:href="id5d514584-e861-4b7f-8cdd-af7282f7ed34" />
      <RecoStaR:materiel xlink:href="idbd09e65f-aaa0-414d-ad04-3c51861a9848" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="id003b0fc7-448f-4e87-883c-31bda25159dc">
      <RecoStaR:ouvrage xlink:href="id7783e3d9-164a-4e44-80ba-28ef30e55cc8" />
      <RecoStaR:materiel xlink:href="id0674551c-a0d2-4b95-9367-955127cfb88c" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="id4f323082-ff6c-4007-84ce-5d5a46c8300d">
      <RecoStaR:ouvrage xlink:href="id72070f65-c788-4852-acf8-e51890105137" />
      <RecoStaR:materiel xlink:href="id0ef8ce58-ecb3-4ea1-8507-bcff6b3858cf" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="id0b6743f7-4049-46d6-84b9-13d2d1f9598a">
      <RecoStaR:ouvrage xlink:href="idf4ec2054-9ce9-47fe-9bf4-a26fc72deaf3" />
      <RecoStaR:materiel xlink:href="id5519d5e1-2a87-4db2-bc28-1019e4e728f7" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="id05eccf2d-37ce-43d2-86d8-dc611c9b40bd">
      <RecoStaR:ouvrage xlink:href="iddbbc8aae-65ac-4774-b0e6-eb4e46156caa" />
      <RecoStaR:materiel xlink:href="id4d40d163-2ae9-4b25-bc97-5fb8b7a9d32d" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="ide3fd60e6-025c-44dd-8078-a8c6dbcb1269">
      <RecoStaR:ouvrage xlink:href="idfb1e6c98-581f-4937-9180-497b36aaf4de" />
      <RecoStaR:materiel xlink:href="idcb415308-812d-45fd-8099-92a5d9a9fdab" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="id5eae4157-f25e-423d-814a-9a00d8652249">
      <RecoStaR:ouvrage xlink:href="id42fd1b78-6d77-4bd4-ba4d-8c7ce570f8ce" />
      <RecoStaR:materiel xlink:href="id963b16f0-ce66-4e43-9abb-864eb7b8e5c2" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="id8d37f224-103a-4ca1-8470-6145349604fb">
      <RecoStaR:ouvrage xlink:href="id88f9d91c-c399-4fae-8ab8-bbe357f88487" />
      <RecoStaR:materiel xlink:href="id6330f5a6-9202-4c9c-a8ca-c3e0b48faa18" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="id26350d8f-3753-441f-8421-5c8467b240ca">
      <RecoStaR:ouvrage xlink:href="idd595b5c1-34e0-4018-b555-827e6a3c08c3" />
      <RecoStaR:materiel xlink:href="id7fa6d071-576f-45c2-af8d-3a2522613fe9" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:ReseauUtilite gml:id="Reseau">
      <RecoStaR:Mention>Test export GML OpenRecoStar</RecoStaR:Mention>
      <RecoStaR:Nom>Réseau public de distribution</RecoStaR:Nom>
      <RecoStaR:Responsable>Enedis</RecoStaR:Responsable>
      <RecoStaR:Theme>ELECTRD</RecoStaR:Theme>
    </RecoStaR:ReseauUtilite>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_BatimentTechnique_Reco gml:id="idfbdfb2c7-70e9-44bf-a86d-a9779bbc15e2">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="id936d99d9-12da-46ef-aee4-935ebbfcfd13" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_1.geom0"><gml:pos>535196.989746793 6948779.67922332</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_BatimentTechnique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_BatimentTechnique_Reco gml:id="id0c722a92-692f-4883-9aac-fb9e90019ade">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="ida5b6732a-e937-4d58-89b0-a7292142cf5f" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_2.geom0"><gml:pos>535196.231000945 6948800.60137368</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_BatimentTechnique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_BatimentTechnique_Reco gml:id="id4c0eeaca-2a66-4518-85ff-2fc17bee6ddf">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="idb9082949-5f51-4e5e-824a-659944ba7929" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_3.geom0"><gml:pos>535785.496586875 6948755.70463853</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_BatimentTechnique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_BatimentTechnique_Reco gml:id="id501d5106-0ead-4ebe-9cbc-4e3c36376c17">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="idf175bae9-08ea-48dc-b68d-6447ecc93e3b" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_4.geom0"><gml:pos>535384.943648199 6948739.35366862</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_BatimentTechnique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idf45fb6ff-f103-4d79-8f92-39d17e48abf8">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">70</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">54</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>Functional</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="iddab5b6bb-89fe-4c62-8f66-a654a740ace5">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">150</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">95</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id4abb14ce-ed92-411c-808c-a5e62cc13766">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>LiaisonReseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">35</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idf450688b-47be-4f79-bd20-9d4ff94af9e4">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>LiaisonReseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">150</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">95</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idf6c8f835-fd4b-4f2b-ab63-248713ad279a">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">115</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>Functional</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id184e70cf-4662-4ff4-9a0e-6f9ac56c0a8e">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">115</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>Functional</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id208103a4-8aae-42f5-8e6a-a1993e4f3342">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">115</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>Functional</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id0c1f7895-218a-4037-ae54-b205600dd513">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">115</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>Functional</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id29ef0bb6-95fa-4b68-b1fe-cfef2b32b20d">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>LiaisonReseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">150</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">95</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id7af7785b-d42c-43db-b39f-01e0e5910adc">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>TronconCommun</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">95</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">75</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idadfae3ad-d4c1-40af-b052-59abe24676ee">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>3</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">0</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>Functional</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id8828897c-9ae8-433d-9508-feb7b924563d">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>3</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">0</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>Functional</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id9e5a4895-2755-4472-98e6-1bdd09b3b264">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>3</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">0</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idf2506dca-7a2c-4871-9149-04169738882b">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>3</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">0</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idc070fca8-05fd-457a-b964-4941b1aa0e99">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>3</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">95</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">0</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id37e6418a-3085-44c4-b8c6-55e99887c90b">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">150</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">95</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id1265e1fa-804c-48c7-b094-c5873f4ae4ac">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">150</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">95</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id70f4eb55-e987-47bb-810c-c8797308ce3d">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>LiaisonReseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">35</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idd2382a89-60c3-4ef2-90f7-5b8ac58b3f20">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>DerivationIndividuelle</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>2</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">35</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id97b41743-bb44-4856-b13e-7579412fa184">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">150</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">95</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id8b72cef8-bfb1-4a0d-86dc-191e9d55cb41">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">150</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">95</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id7cdaff9b-4a75-491a-83d8-8b4464e59796">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">115</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id06eaaa70-79eb-4117-ae62-1f1eac839bbf">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">115</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id637fce18-64ee-4972-a3cc-d2833430ef48">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>LiaisonReseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">35</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idc2c492ea-3fb5-458d-9348-fc5d996768ed">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>DerivationIndividuelle</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>2</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">35</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idafb87fa5-75eb-4b38-bdf0-5c8576922dfd">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>3</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">0</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id2ac82f92-9128-4697-a1c6-8fa0de7061d8">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>3</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">240</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">0</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableTerre_Reco gml:id="idb585dcea-c8f0-4d99-b821-03b118900025">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:noeudReseau xlink:href="ide8b98221-6e68-4c48-a8c4-07ccaff4472c" />
      <RecoStaR:FonctionCable xlink:href="MaltEquipot" />
      <RecoStaR:Materiau>Cuivre</RecoStaR:Materiau>
      <RecoStaR:NatureCableTerre xlink:href="CuivreNu" />
      <RecoStaR:Section uom="mm-2">25</RecoStaR:Section>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableTerre_Reco gml:id="id12f5544d-5e20-440e-8058-73209353b51d">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:noeudReseau xlink:href="id9fce9b89-6483-40be-b42e-1bea06be0040" />
      <RecoStaR:FonctionCable xlink:href="MiseTerre" />
      <RecoStaR:Materiau>Cuivre</RecoStaR:Materiau>
      <RecoStaR:NatureCableTerre xlink:href="CuivreNu" />
      <RecoStaR:Section uom="mm-2">25</RecoStaR:Section>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableTerre_Reco gml:id="idd46675da-8528-4301-b232-c233ffa1d83b">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:noeudReseau xlink:href="id4b87e98c-2e82-45ca-b9dd-de45952152ed" />
      <RecoStaR:FonctionCable xlink:href="MaltEquipot" />
      <RecoStaR:Materiau>Cuivre</RecoStaR:Materiau>
      <RecoStaR:NatureCableTerre xlink:href="CuivreNu" />
      <RecoStaR:Section uom="mm-2">25</RecoStaR:Section>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="idcfcc3830-1c3d-408d-82f7-b5ec0ee827ba">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="id6d19a8cf-fc23-461d-b6c0-7446b5b37ca5" />
      <RecoStaR:FonctionCoffret xlink:href="Separable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_1.geom0"><gml:pos>535588.779801824 6948801.44527379 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="Saillie" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="RMBT300" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="id850096b7-26f4-4fac-af65-22b9f8156f66">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="idbb029c6e-0c9c-4e93-893d-5c748b255559" />
      <RecoStaR:FonctionCoffret xlink:href="Manoeuvrable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_2.geom0"><gml:pos>535589.924783173 6948801.52967045 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="Saillie" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="CIBE" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="id9eb1cb4d-0ac4-43a7-9b7c-57a6ce950221">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="ide0dd02b0-79c7-4fb2-b0b2-4945099c07b8" />
      <RecoStaR:FonctionCoffret xlink:href="Manoeuvrable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_3.geom0"><gml:pos>535012.419255373 6948770.02182599 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="Encastree" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="ECP" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="idc3b01631-75cd-46b5-b981-5221323706e1">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="id2c21aa0f-82c4-4606-b2d6-64bbab623973" />
      <RecoStaR:FonctionCoffret xlink:href="Manoeuvrable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_4.geom0"><gml:pos>535066.195224397 6948791.74869105 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="Saillie" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="ECP" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="id75f2acf3-22c0-4a07-a60c-6b769341cf14">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="id54fe2ff4-e0bf-4190-9e5e-6620d8358819" />
      <RecoStaR:FonctionCoffret xlink:href="Separable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_5.geom0"><gml:pos>535464.236772232 6948795.52280418 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="Saillie" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="RMBT450" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="id362dc37b-5e62-4a22-8baa-3c1182d4d58a">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="id15704127-5065-4c1b-8738-29245d000183" />
      <RecoStaR:FonctionCoffret xlink:href="Manoeuvrable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_6.geom0"><gml:pos>535468.243503644 6948795.3329117 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="Saillie" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="CIBE" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="idab15f3d6-caa9-42ae-9710-d02d93c62a10">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="id775b6e10-e88d-462a-be59-b64f6f80f299" />
      <RecoStaR:FonctionCoffret xlink:href="Separable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_7.geom0"><gml:pos>535068.550000109 6948774.30580149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="Saillie" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="RMBT450" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="id8cf7d98a-8fc3-4c5e-b5fb-0de9b2021277">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="idfdb83775-ebce-4139-8e82-2b4866731300" />
      <RecoStaR:FonctionCoffret xlink:href="Manoeuvrable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_8.geom0"><gml:pos>535812.893614735 6948786.07624652 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="Saillie" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="RMBT450" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="idd35339e5-edc9-4f2d-9763-f407f5ff152a">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="id92ba46c1-60b2-4211-b6b1-e414d55b1ef4" />
      <RecoStaR:FonctionCoffret xlink:href="Manoeuvrable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_9.geom0"><gml:pos>535807.690560675 6948782.78161193 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="Saillie" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="CIBE" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CoupeCircuitAFusibles_Reco gml:id="idcb6ff40c-b316-43ba-bd5a-6f76e7930a91">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id850096b7-26f4-4fac-af65-22b9f8156f66" />
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CoupeCircuitAFusibles_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CoupeCircuitAFusibles_Reco gml:id="id81df2309-6f65-4ba2-add1-116cdd06b560">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id9eb1cb4d-0ac4-43a7-9b7c-57a6ce950221" />
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CoupeCircuitAFusibles_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CoupeCircuitAFusibles_Reco gml:id="ide60da224-0312-4201-985e-b8f01a79a046">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idc3b01631-75cd-46b5-b981-5221323706e1" />
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CoupeCircuitAFusibles_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CoupeCircuitAFusibles_Reco gml:id="idff66d7aa-7bb9-47bf-8e0c-9bf1c0e78b7e">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id362dc37b-5e62-4a22-8baa-3c1182d4d58a" />
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CoupeCircuitAFusibles_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CoupeCircuitAFusibles_Reco gml:id="idf3bfab2f-b849-4db5-8933-21028031068f">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idab15f3d6-caa9-42ae-9710-d02d93c62a10" />
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CoupeCircuitAFusibles_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CoupeCircuitAFusibles_Reco gml:id="id3f2c665d-92ff-44db-b45a-4809f376f87a">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idd35339e5-edc9-4f2d-9763-f407f5ff152a" />
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CoupeCircuitAFusibles_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_EnceinteCloturee_Reco gml:id="id930af892-c791-4690-a0b4-57e383123b40">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="idbb0a6d98-6218-41bb-a584-9562d50dd214" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_EnceinteCloturee_Reco_1.geom0"><gml:pos>535363.622318474 6948714.55690557</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_EnceinteCloturee_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Fourreau_Reco gml:id="id0d2d7f8f-f05c-419c-b90c-9a9aa8b0b38c">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DiametreDuFourreau uom="mm">160</RecoStaR:DiametreDuFourreau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_Fourreau_Reco_1.geom0"><gml:posList srsDimension="3">535196.444061589 6948794.87716973 0 535196.89980355 6948783.78744867 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:Materiau>PEX</RecoStaR:Materiau>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:ProfondeurMinNonReg uom="m">0</RecoStaR:ProfondeurMinNonReg>
    </RecoStaR:RPD_Fourreau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="id936d99d9-12da-46ef-aee4-935ebbfcfd13">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_geomsupp_1.geom0"><gml:curveMember><gml:LineString gml:id="RPD_BatimentTechnique_Reco_geomsupp_1.geom0.0"><gml:posList srsDimension="3">535195.608534659 6948780.33140546 0 535198.267029435 6948780.48331945 0 535198.406283923 6948779.04013657 0 535195.671832154 6948778.88822258 0 535195.608534659 6948780.33140546 0</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_geomsupp_1.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">535195.608534659 6948780.33140546 0 535198.267029435 6948780.48331945 0 535198.406283923 6948779.04013657 0 535195.671832154 6948778.88822258 0 535195.608534659 6948780.33140546 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="ida5b6732a-e937-4d58-89b0-a7292142cf5f">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_geomsupp_2.geom0"><gml:curveMember><gml:LineString gml:id="RPD_BatimentTechnique_Reco_geomsupp_2.geom0.0"><gml:posList srsDimension="3">535194.557796248 6948801.57404467 0 535197.773308976 6948801.76393715 0 535197.899903966 6948799.63714133 0 535194.709710235 6948799.42192985 0 535194.557796248 6948801.57404467 0</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_geomsupp_2.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">535194.557796248 6948801.57404467 0 535197.773308976 6948801.76393715 0 535197.899903966 6948799.63714133 0 535194.709710235 6948799.42192985 0 535194.557796248 6948801.57404467 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="idb9082949-5f51-4e5e-824a-659944ba7929">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_geomsupp_3.geom0"><gml:curveMember><gml:LineString gml:id="RPD_BatimentTechnique_Reco_geomsupp_3.geom0.0"><gml:posList srsDimension="3">535777.560000112 6948768.04000149 0 535782.580000112 6948741.75000149 0 535793.540000112 6948743.83000149 0 535788.900000112 6948768.17000149 0 535786.560000112 6948769.76000149 0 535777.560000112 6948768.04000149 0</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_geomsupp_3.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">535777.560000112 6948768.04000149 0 535782.580000112 6948741.75000149 0 535793.540000112 6948743.83000149 0 535788.900000112 6948768.17000149 0 535786.560000112 6948769.76000149 0 535777.560000112 6948768.04000149 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="idf175bae9-08ea-48dc-b68d-6447ecc93e3b">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_geomsupp_4.geom0"><gml:curveMember><gml:LineString gml:id="RPD_BatimentTechnique_Reco_geomsupp_4.geom0.0"><gml:posList srsDimension="3">535373.300000112 6948742.17000149 0 535375.230000112 6948732.35000149 0 535396.580000112 6948736.53000149 0 535394.660000112 6948746.36000149 0 535373.300000112 6948742.17000149 0</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_BatimentTechnique_Reco_geomsupp_4.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">535373.300000112 6948742.17000149 0 535375.230000112 6948732.35000149 0 535396.580000112 6948736.53000149 0 535394.660000112 6948746.36000149 0 535373.300000112 6948742.17000149 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="idbb0a6d98-6218-41bb-a584-9562d50dd214">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_EnceinteCloturee_Reco_geomsupp_1.geom0"><gml:curveMember><gml:LineString gml:id="RPD_EnceinteCloturee_Reco_geomsupp_1.geom0.0"><gml:posList srsDimension="3">535303.840000112 6948786.42000149 0 535340.550000112 6948633.25000149 0 535419.710000112 6948641.79000149 0 535402.159937309 6948749.23433368 0 535392.142928372 6948791.06207469 0 535303.840000112 6948786.42000149 0</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_EnceinteCloturee_Reco_geomsupp_1.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">535303.840000112 6948786.42000149 0 535340.550000112 6948633.25000149 0 535419.710000112 6948641.79000149 0 535402.159937309 6948749.23433368 0 535392.142928372 6948791.06207469 0 535303.840000112 6948786.42000149 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="id6d19a8cf-fc23-461d-b6c0-7446b5b37ca5">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_1.geom0"><gml:curveMember><gml:LineString gml:id="RPD_Coffret_Reco_geomsupp_1.geom0.0"><gml:posList srsDimension="3">535588.779801824 6948801.44527379 0 535588.480943415 6948801.41912707 0 535588.498374564 6948801.21988813 0 535589.096091382 6948801.27218158 0 535589.078660234 6948801.47142052 0 535588.779801824 6948801.44527379 0</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_1.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">535588.779801824 6948801.44527379 0 535588.480943415 6948801.41912707 0 535588.498374564 6948801.21988813 0 535589.096091382 6948801.27218158 0 535589.078660234 6948801.47142052 0 535588.779801824 6948801.44527379 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="idbb029c6e-0c9c-4e93-893d-5c748b255559">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_2.geom0"><gml:curveMember><gml:LineString gml:id="RPD_Coffret_Reco_geomsupp_2.geom0.0"><gml:posList srsDimension="3">535589.924783173 6948801.52967045 0 535589.625924763 6948801.50352373 0 535589.643355912 6948801.30428479 0 535590.241072731 6948801.35657824 0 535590.223641582 6948801.55581718 0 535589.924783173 6948801.52967045 0</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_2.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">535589.924783173 6948801.52967045 0 535589.625924763 6948801.50352373 0 535589.643355912 6948801.30428479 0 535590.241072731 6948801.35657824 0 535590.223641582 6948801.55581718 0 535589.924783173 6948801.52967045 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="ide0dd02b0-79c7-4fb2-b0b2-4945099c07b8">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_3.geom0"><gml:curveMember><gml:LineString gml:id="RPD_Coffret_Reco_geomsupp_3.geom0.0"><gml:posList srsDimension="3">535012.419255373 6948770.02182599 0 535012.119438125 6948770.01135614 0 535012.126418024 6948769.81147797 0 535012.726052521 6948769.83241767 0 535012.719072621 6948770.03229584 0 535012.419255373 6948770.02182599 0</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_3.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">535012.419255373 6948770.02182599 0 535012.119438125 6948770.01135614 0 535012.126418024 6948769.81147797 0 535012.726052521 6948769.83241767 0 535012.719072621 6948770.03229584 0 535012.419255373 6948770.02182599 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="id2c21aa0f-82c4-4606-b2d6-64bbab623973">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_4.geom0"><gml:curveMember><gml:LineString gml:id="RPD_Coffret_Reco_geomsupp_4.geom0.0"><gml:posList srsDimension="3">535066.195224397 6948791.74869105 0 535066.495224397 6948791.74869105 0 535066.495224397 6948791.94869105 0 535065.895224397 6948791.94869105 0 535065.895224397 6948791.74869105 0 535066.195224397 6948791.74869105 0</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_4.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">535066.195224397 6948791.74869105 0 535066.495224397 6948791.74869105 0 535066.495224397 6948791.94869105 0 535065.895224397 6948791.94869105 0 535065.895224397 6948791.74869105 0 535066.195224397 6948791.74869105 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="id54fe2ff4-e0bf-4190-9e5e-6620d8358819">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_5.geom0"><gml:curveMember><gml:LineString gml:id="RPD_Coffret_Reco_geomsupp_5.geom0.0"><gml:posList srsDimension="3">535464.236772232 6948795.52280418 0 535463.937503017 6948795.50187724 0 535463.951454311 6948795.30236443 0 535464.549992742 6948795.34421831 0 535464.536041447 6948795.54373112 0 535464.236772232 6948795.52280418 0</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_5.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">535464.236772232 6948795.52280418 0 535463.937503017 6948795.50187724 0 535463.951454311 6948795.30236443 0 535464.549992742 6948795.34421831 0 535464.536041447 6948795.54373112 0 535464.236772232 6948795.52280418 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="id15704127-5065-4c1b-8738-29245d000183">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_6.geom0"><gml:curveMember><gml:LineString gml:id="RPD_Coffret_Reco_geomsupp_6.geom0.0"><gml:posList srsDimension="3">535468.243503644 6948795.3329117 0 535467.944234428 6948795.31198476 0 535467.958185723 6948795.11247195 0 535468.556724153 6948795.15432583 0 535468.542772859 6948795.35383864 0 535468.243503644 6948795.3329117 0</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_6.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">535468.243503644 6948795.3329117 0 535467.944234428 6948795.31198476 0 535467.958185723 6948795.11247195 0 535468.556724153 6948795.15432583 0 535468.542772859 6948795.35383864 0 535468.243503644 6948795.3329117 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="id775b6e10-e88d-462a-be59-b64f6f80f299">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_7.geom0"><gml:curveMember><gml:LineString gml:id="RPD_Coffret_Reco_geomsupp_7.geom0.0"><gml:posList srsDimension="3">535068.550000109 6948774.30580149 0 535068.250411249 6948774.2901007 0 535068.26087844 6948774.0903748 0 535068.860056161 6948774.12177637 0 535068.849588969 6948774.32150228 0 535068.550000109 6948774.30580149 0</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_7.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">535068.550000109 6948774.30580149 0 535068.250411249 6948774.2901007 0 535068.26087844 6948774.0903748 0 535068.860056161 6948774.12177637 0 535068.849588969 6948774.32150228 0 535068.550000109 6948774.30580149 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="idfdb83775-ebce-4139-8e82-2b4866731300">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_8.geom0"><gml:curveMember><gml:LineString gml:id="RPD_Coffret_Reco_geomsupp_8.geom0.0"><gml:posList srsDimension="3">535812.893614735 6948786.07624652 0 535813.00112512 6948785.7961724 0 535813.187841206 6948785.86784599 0 535812.972820436 6948786.42799424 0 535812.78610435 6948786.35632065 0 535812.893614735 6948786.07624652 0</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_8.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">535812.893614735 6948786.07624652 0 535813.00112512 6948785.7961724 0 535813.187841206 6948785.86784599 0 535812.972820436 6948786.42799424 0 535812.78610435 6948786.35632065 0 535812.893614735 6948786.07624652 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="id92ba46c1-60b2-4211-b6b1-e414d55b1ef4">
      <RecoStaR:Ligne2.5D><gml:MultiCurve srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_9.geom0"><gml:curveMember><gml:LineString gml:id="RPD_Coffret_Reco_geomsupp_9.geom0.0"><gml:posList srsDimension="3">535807.690560675 6948782.78161193 0 535807.898958186 6948782.56580999 0 535808.042826146 6948782.70474166 0 535807.626031124 6948783.13634554 0 535807.482163163 6948782.99741387 0 535807.690560675 6948782.78161193 0</gml:posList></gml:LineString></gml:curveMember></gml:MultiCurve></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_9.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">535807.690560675 6948782.78161193 0 535807.898958186 6948782.56580999 0 535808.042826146 6948782.70474166 0 535807.626031124 6948783.13634554 0 535807.482163163 6948782.99741387 0 535807.690560675 6948782.78161193 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="idae8810b3-6697-4d4a-8aea-03ce8343d766">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id05d52f19-fafb-4596-812e-843b18eed213" />
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_1.geom0"><gml:pos>535580.97029759 6948801.34118458 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>RemonteeAeroSouterraine</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="ided3a479b-8cce-4caa-9fa7-7c5685970811">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_2.geom0"><gml:pos>535012.723083348 6948774.40201262 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Derivation</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="id0e46458b-9f1c-4205-a9c5-bd47b549be9b">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_3.geom0"><gml:pos>535064.827998512 6948788.7610493 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Derivation</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="ida7b8bed2-e36d-41f4-b730-b1ffe10f930b">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_4.geom0"><gml:pos>535191.051115044 6948796.59886159 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Jonction</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="id5d514584-e861-4b7f-8cdd-af7282f7ed34">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_5.geom0"><gml:pos>535201.229352185 6948797.51034551 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Jonction</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="id7783e3d9-164a-4e44-80ba-28ef30e55cc8">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_6.geom0"><gml:pos>535462.755610857 6948798.50411618 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Jonction</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="id72070f65-c788-4852-acf8-e51890105137">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_7.geom0"><gml:pos>535466.543965912 6948798.74148179 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Jonction</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="idf4ec2054-9ce9-47fe-9bf4-a26fc72deaf3">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_8.geom0"><gml:pos>535066.5087448 6948777.3684893 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Jonction</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="iddbbc8aae-65ac-4774-b0e6-eb4e46156caa">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_9.geom0"><gml:pos>535072.927110758 6948777.61534953 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Jonction</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="idfb1e6c98-581f-4937-9180-497b36aaf4de">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_10.geom0"><gml:pos>535812.029603933 6948786.49400999 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Jonction</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="id42fd1b78-6d77-4bd4-ba4d-8c7ce570f8ce">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_11.geom0"><gml:pos>535812.7227115 6948784.8894185 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Jonction</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="ida483077c-8c17-4559-9253-f46fb64997f6">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idf1365ae1-8fb8-44ee-b62a-1b7d73607379" />
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_12.geom0"><gml:pos>535789.214021985 6948766.62176154 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>RemonteeAeroSouterraine</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="id88f9d91c-c399-4fae-8ab8-bbe357f88487">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id930af892-c791-4690-a0b4-57e383123b40" />
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_13.geom0"><gml:pos>535377.091550853 6948763.4351283 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Jonction</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="idd595b5c1-34e0-4018-b555-827e6a3c08c3">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>HTA</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_14.geom0"><gml:pos>535355.747635655 6948791.15943096 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Jonction</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="idf469a17e-171f-4374-8a1b-85302189b0ff">
      <RecoStaR:Fabricant>maker</RecoStaR:Fabricant>
      <RecoStaR:Modele>123</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>456</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="id842c0f72-1757-4a6f-8080-4f4e35a1ff99">
      <RecoStaR:Fabricant>maker</RecoStaR:Fabricant>
      <RecoStaR:Modele>123</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>456</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="id44b8c864-f1aa-490d-b11e-5d0d5aea34bd">
      <RecoStaR:Fabricant>maker</RecoStaR:Fabricant>
      <RecoStaR:Modele>123</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>456</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="idbd09e65f-aaa0-414d-ad04-3c51861a9848">
      <RecoStaR:Fabricant>maker</RecoStaR:Fabricant>
      <RecoStaR:Modele>123</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>456</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="id0674551c-a0d2-4b95-9367-955127cfb88c">
      <RecoStaR:Fabricant>maker</RecoStaR:Fabricant>
      <RecoStaR:Modele>123</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>456</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="id0ef8ce58-ecb3-4ea1-8507-bcff6b3858cf">
      <RecoStaR:Fabricant>maker</RecoStaR:Fabricant>
      <RecoStaR:Modele>123</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>546</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="id5519d5e1-2a87-4db2-bc28-1019e4e728f7">
      <RecoStaR:Fabricant>maker</RecoStaR:Fabricant>
      <RecoStaR:Modele>123</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>1456</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="id4d40d163-2ae9-4b25-bc97-5fb8b7a9d32d">
      <RecoStaR:Fabricant>maker</RecoStaR:Fabricant>
      <RecoStaR:Modele>123</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>456</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="idcb415308-812d-45fd-8099-92a5d9a9fdab">
      <RecoStaR:Fabricant>maker</RecoStaR:Fabricant>
      <RecoStaR:Modele>123</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>546</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="id963b16f0-ce66-4e43-9abb-864eb7b8e5c2">
      <RecoStaR:Fabricant>maker</RecoStaR:Fabricant>
      <RecoStaR:Modele>123</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>456</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="id6330f5a6-9202-4c9c-a8ca-c3e0b48faa18">
      <RecoStaR:Fabricant>maker</RecoStaR:Fabricant>
      <RecoStaR:Modele>123</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>456</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="id7fa6d071-576f-45c2-af8d-3a2522613fe9">
      <RecoStaR:Fabricant>maker</RecoStaR:Fabricant>
      <RecoStaR:Modele>213</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>456</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_OuvrageCollectifBranchement_Reco gml:id="id4bab3991-29ed-42e8-a1b3-0e89c27775da">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id9eb1cb4d-0ac4-43a7-9b7c-57a6ce950221" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_OuvrageCollectifBranchement_Reco_1.geom0"><gml:pos>535012.590158609 6948769.9332095 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_OuvrageCollectifBranchement_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_OuvrageCollectifBranchement_Reco gml:id="id3def5f9c-7e91-4397-b636-7ceb74a8587b">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_OuvrageCollectifBranchement_Reco_2.geom0"><gml:pos>535064.752041518 6948814.15600416 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_OuvrageCollectifBranchement_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id9e4cab11-a024-48bc-83da-38729078acd8">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_1.geom0"><gml:posList srsDimension="3">535812.893614735 6948786.07624652 0 535812.684733003 6948785.70595618 0 535812.7227115 6948784.8894185 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="idc821ee06-0d54-4010-80d2-988f7d181c36">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_2.geom0"><gml:posList srsDimension="3">535064.827998512 6948788.7610493 0 535071.613489939 6948789.01423928 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id66191251-e128-4f1b-8244-cce8bbb1ed3f">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_3.geom0"><gml:posList srsDimension="3">535464.236772232 6948795.52280418 0 535464.626051824 6948796.88053544 0 535465.31915939 6948798.07685809 0 535466.543965912 6948798.74148179 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id948fae7a-2c91-4775-8b56-d420ca32a23e">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_4.geom0"><gml:posList srsDimension="3">535580.97029759 6948801.34118458 0 535581.430501151 6948801.53448371 0 535581.91789186 6948801.6990572 0 535582.341985074 6948801.98389593 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="idaf1b2f96-0a45-4714-879a-d1077a8045ce">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_5.geom0"><gml:posList srsDimension="3">535012.723083348 6948774.40201262 0 535021.179628634 6948774.80711659 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id581ef232-7b37-4ff4-847d-e87f075a4be5">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_6.geom0"><gml:posList srsDimension="3">535057.890593098 6948788.30530734 0 535064.827998512 6948788.7610493 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="idd3872a31-45e4-46f6-88d1-9d3ab98354ce">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_7.geom0"><gml:posList srsDimension="3">535064.827998512 6948788.7610493 0 535065.688844439 6948789.21679126 0 535066.183100112 6948790.06880149 0 535066.195224397 6948791.74869105 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id109882f6-a626-4f22-8b18-6f81c3392044">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_8.geom0"><gml:posList srsDimension="3">535377.091550853 6948763.4351283 0 535370.635206398 6948775.13250532 0 535359.013786379 6948785.91839841 0 535355.747635655 6948791.15943096 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="idd0cb082e-7647-48fb-8128-8a0e85ade40a">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_9.geom0"><gml:posList srsDimension="3">535462.755610857 6948798.50411618 0 535463.629116283 6948797.82999786 0 535464.236772232 6948795.52280418 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id6be51ee7-37b6-4e47-8355-dc83109794ef">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_10.geom0"><gml:posList srsDimension="3">535588.779801824 6948801.44527379 0 535589.004859583 6948801.63094645 0 535589.499986653 6948801.67033155 0 535589.789748517 6948801.63938611 0 535589.924783173 6948801.52967045 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="idb33f2d57-7e31-4af6-88ca-53b2ab43cd61">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_11.geom0"><gml:posList srsDimension="3">535812.893614735 6948786.07624652 0 535811.725775959 6948785.4021282 0 535809.674937132 6948784.65205289 0 535808.393162865 6948783.58865498 0 535807.690560675 6948782.78161193 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id6c44af58-aa7e-4bbf-8bf8-d199bcdbdeeb">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_12.geom0"><gml:posList srsDimension="3">535464.236772232 6948795.52280418 0 535465.366632511 6948795.9595569 0 535466.971224001 6948796.13995476 0 535467.683320816 6948795.95006227 0 535468.243503644 6948795.3329117 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id9f30c69d-7302-4ccd-8602-7e061123c36f">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_13.geom0"><gml:posList srsDimension="3">535066.195224397 6948791.74869105 0 535065.233102478 6948800.00268435 0 535065.131826487 6948809.32007556 0 535064.752041518 6948814.15600416 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id9f169f43-35fc-46e0-8ec9-20e1573a00da">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_14.geom0"><gml:posList srsDimension="3">535812.029603933 6948786.49400999 0 535812.380905029 6948786.17119277 0 535812.893614735 6948786.07624652 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id46beaa40-8f3f-4a03-8fa8-f1ece6c60f5b">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_15.geom0"><gml:posList srsDimension="3">535201.229352185 6948797.51034551 0 535209.837811458 6948797.86481148 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id781a2686-5b8b-44ba-898b-44429197715b">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_16.geom0"><gml:posList srsDimension="3">535068.550000109 6948774.30580149 0 535069.262185817 6948775.48855371 0 535070.496486963 6948776.45700538 0 535072.927110758 6948777.61534953 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id9483f95a-6f00-44a8-81f8-0c3f7f65ae15">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_17.geom0"><gml:posList srsDimension="3">535066.5087448 6948777.3684893 0 535067.913949181 6948776.00126342 0 535068.550000109 6948774.30580149 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id3ae51080-1fa1-4e11-8fe7-3d06881983ce">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_18.geom0"><gml:posList srsDimension="3">535191.051115044 6948796.59886159 0 535193.886842805 6948797.6622595 0 535195.760448646 6948799.73841732 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id229ebec0-86c0-4086-8c0e-a76fd777a3f8">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_19.geom0"><gml:posList srsDimension="3">535183.050311719 6948796.14311962 0 535191.051115044 6948796.59886159 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id4179a5e8-da74-4875-829a-2c8736f1a843">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_20.geom0"><gml:posList srsDimension="3">535383.728293168 6948744.16104118 0 535381.15525001 6948749.87680495 0 535379.636110139 6948755.155816 0 535377.091550853 6948763.4351283 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id2fb5f4b5-1d21-446f-8a2d-7565fa5ad320">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_21.geom0"><gml:posList srsDimension="3">535198.267029435 6948780.48331945 0 535198.431602921 6948781.10363489 0 535198.988620874 6948778.54641611 0 535195.102154702 6948778.15397164 0 535194.88694322 6948781.12895389 0 535199.178513358 6948781.54671736 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="idcc0e0da9-9a01-4c5a-8fe9-16cbbf69b47e">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_22.geom0"><gml:posList srsDimension="3">535196.418742591 6948799.68777933 0 535196.216190608 6948797.10524154 0 535196.444061589 6948794.87716973 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="ide8dda9e9-a9d7-45a1-81be-d1a9c9d730e7">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_23.geom0"><gml:posList srsDimension="3">535196.89980355 6948783.78744867 0 535197.001079542 6948780.36938396 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id3de8ef41-78f1-4a8d-8250-ba1b9d260fe3">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_24.geom0"><gml:posList srsDimension="3">535807.690560675 6948782.78161193 0 535802.126710895 6948776.34425672 0 535795.594409446 6948770.49556822 0 535789.214021985 6948766.62176154 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="ida33e3923-4ed6-4fa5-84b2-0ff4148dd62d">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_25.geom0"><gml:posList srsDimension="3">535468.243503644 6948795.3329117 0 535469.401847796 6948794.49738477 0 535471.24380489 6948794.1555783 0 535473.02879424 6948793.94669657 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id185a64f9-5f50-491c-8f8f-55d68b94e271">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_26.geom0"><gml:posList srsDimension="3">535194.709710235 6948799.42192985 0 535193.9374808 6948799.51054635 0 535193.785566813 6948802.06776513 0 535198.330326929 6948802.4095716 0 535198.570857409 6948799.21937787 0 535194.013437794 6948798.95352839 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="idb7229766-83a0-4e4d-848c-8eacc5499fd8">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_27.geom0"><gml:posList srsDimension="3">535580.97029759 6948801.34118458 0 535584.278646647 6948801.45371346 0 535586.956833977 6948801.81380587 0 535588.307180531 6948801.81380587 0 535588.656020057 6948801.63375967 0 535588.779801824 6948801.44527379 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="idcf59aee1-6893-4064-8564-7ba29894bdeb">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_28.geom0"><gml:posList srsDimension="3">535201.229352185 6948797.51034551 0 535198.241710437 6948798.52310542 0 535197.02639854 6948799.9409693 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id4d435ac4-281e-4c17-8c7f-55955c7a192d">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_29.geom0"><gml:posList srsDimension="3">535012.723083348 6948774.40201262 0 535012.317979382 6948774.07286565 0 535012.191384392 6948773.5158477 0 535012.419255373 6948770.02182599 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id9f2af28e-b8b6-4907-83ae-add9490bff79">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_30.geom0"><gml:posList srsDimension="3">535524.098201918 6948798.22413462 0 535580.97029759 6948801.34118458 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id564e1280-3a42-43a6-8a1b-fcefdfef6611">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_31.geom0"><gml:posList srsDimension="3">535003.962710087 6948773.84499467 0 535012.723083348 6948774.40201262 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointDeComptage_Reco gml:id="id40b07043-0a50-4923-ab28-c226c916abed">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id850096b7-26f4-4fac-af65-22b9f8156f66" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointDeComptage_Reco_1.geom0"><gml:pos>535590.094345732 6948801.4965052 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:NumeroPRM>0</RecoStaR:NumeroPRM>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_PointDeComptage_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointDeComptage_Reco gml:id="id12adaa6a-fa05-4789-9897-543810b783f5">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointDeComptage_Reco_2.geom0"><gml:pos>535473.02879424 6948793.94669657 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:NumeroPRM>666</RecoStaR:NumeroPRM>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_PointDeComptage_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointDeComptage_Reco gml:id="idcd346965-1a53-4b0d-a05c-96bf632eda8a">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idab15f3d6-caa9-42ae-9710-d02d93c62a10" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointDeComptage_Reco_3.geom0"><gml:pos>535068.594595053 6948774.2174609 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:NumeroPRM>666</RecoStaR:NumeroPRM>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_PointDeComptage_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointDeComptage_Reco gml:id="id8864bf27-5431-4f4a-a869-579872ac3c20">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idf1365ae1-8fb8-44ee-b62a-1b7d73607379" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointDeComptage_Reco_4.geom0"><gml:pos>535789.214021985 6948766.62176154 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:NumeroPRM>666</RecoStaR:NumeroPRM>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_PointDeComptage_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id924be4c3-0ed8-4d3f-a6fc-2a3dccd5809d">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_1.geom0"><gml:pos>535524.098201918 6948798.22413462 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>OPENRECO</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idb92bdab2-557e-4a46-a67b-14701af78de6">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_2.geom0"><gml:pos>535580.97029759 6948801.34118458 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>2</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id28a1bbb1-6536-40d3-8a27-fa11014fd544">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_3.geom0"><gml:pos>535584.278646647 6948801.45371346 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>3</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idb076cf54-c494-429b-903c-702d8c71516f">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_4.geom0"><gml:pos>535586.956833977 6948801.81380587 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>4</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id09b12f7b-4083-48d8-aba5-b027df5255e8">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_5.geom0"><gml:pos>535588.307180531 6948801.81380587 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>5</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id3228a879-0164-49ea-b528-7f97cd4c5fdd">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_6.geom0"><gml:pos>535588.656020057 6948801.63375967 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>6</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id1c8d4d95-09cc-47f2-a12b-1c08679a54ac">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_7.geom0"><gml:pos>535588.779801824 6948801.44527379 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>7</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id229824ad-3be6-4ec1-a934-2554c5447aa5">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_8.geom0"><gml:pos>535589.004859583 6948801.63094645 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>8</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ida6119c10-a532-4f68-8673-433bc5418947">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_9.geom0"><gml:pos>535589.499986653 6948801.67033155 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>9</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ida1816584-c452-4248-aca3-e2ed400a0c03">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_10.geom0"><gml:pos>535589.789748517 6948801.63938611 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>10</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idfe9a8133-adc8-495c-8cab-525e5374bc95">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_11.geom0"><gml:pos>535589.924783173 6948801.52967045 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>11</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id62246329-a0c5-44f7-bbdc-b85ce438cd3b">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_12.geom0"><gml:pos>535003.962710087 6948773.84499467 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>10</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id47e4f95e-027a-434f-b2d3-8ea066b89540">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_13.geom0"><gml:pos>535021.179628634 6948774.80711659 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>11</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ide29458f0-a771-450e-944c-e6e21ec1536f">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_14.geom0"><gml:pos>535012.723083348 6948774.40201262 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>12</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id434dcccb-0f38-439d-a36b-126d7b5caada">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_15.geom0"><gml:pos>535012.317979382 6948774.07286565 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>13</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id96eecea7-7e16-4cb1-b69a-a7b2bf92c8ae">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_16.geom0"><gml:pos>535012.191384392 6948773.5158477 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>14</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id9886d32a-b871-418a-b439-a92a5c7652af">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_17.geom0"><gml:pos>535012.419255373 6948770.02182599 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>15</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idc236995e-895c-48c9-bb08-a3fd262227ae">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_18.geom0"><gml:pos>535581.430501151 6948801.53448371 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>20</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id260b4c2b-3101-4ecf-883c-09322571ea46">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_19.geom0"><gml:pos>535581.91789186 6948801.6990572 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>21</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id8925c8ef-57dc-4c22-93d1-dcae7b559bf5">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_20.geom0"><gml:pos>535582.341985074 6948801.98389593 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>22</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id8ea0a0a8-57c7-40c4-a191-94674fc6df94">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_21.geom0"><gml:pos>535057.890593098 6948788.30530734 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>30</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ideaf10df6-2024-41e3-b0a5-68d0e60c46f8">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_22.geom0"><gml:pos>535064.827998512 6948788.7610493 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>31</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idc150c61e-f474-4498-b51e-86dd422b2293">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_23.geom0"><gml:pos>535071.613489939 6948789.01423928 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>32</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id849eec0a-6623-4f3d-b373-be228155b4e2">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_24.geom0"><gml:pos>535065.688844439 6948789.21679126 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>33</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ide64f2f06-5746-42d2-b6a3-ff2af3d6f9fe">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_25.geom0"><gml:pos>535066.183100112 6948790.06880149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>34</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id09f085f1-6be3-4671-9b40-2fa7d7c0c0df">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_26.geom0"><gml:pos>535066.195224397 6948791.74869105 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>35</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id32bf6422-78ee-4724-848c-a503605167a1">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_27.geom0"><gml:pos>535065.233102478 6948800.00268435 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>36</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id9dbcd26f-fa81-4040-aece-452f900e9aba">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_28.geom0"><gml:pos>535065.131826487 6948809.32007556 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>37</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idddee3769-a4b4-446f-87e1-9aa6523f0086">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_29.geom0"><gml:pos>535064.752041518 6948814.15600416 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>38</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id47ec58cf-8b81-4874-8981-e853947d1742">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_30.geom0"><gml:pos>535191.051115044 6948796.59886159 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>50</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idb4e5f2ba-e01f-4a96-bc75-334da73a99f7">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_31.geom0"><gml:pos>535193.886842805 6948797.6622595 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>51</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id64492898-cf88-49e0-a392-88f2e580212b">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_32.geom0"><gml:pos>535195.760448646 6948799.73841732 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>52</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idd892df4e-2fcf-4485-bea4-6087593ba193">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_33.geom0"><gml:pos>535197.02639854 6948799.9409693 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>53</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ide045117f-cf58-43b3-af41-77c2221266a6">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_34.geom0"><gml:pos>535198.241710437 6948798.52310542 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>54</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id4543c3f0-d8d1-4865-b29b-289f36042e3a">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_35.geom0"><gml:pos>535201.229352185 6948797.51034551 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>55</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id2fa27ebc-d3fb-48fa-8b84-aa994e0141c1">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_36.geom0"><gml:pos>535183.050311719 6948796.14311962 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>56</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ide3e3cb2e-3804-43e4-a73a-a29fc4fe4cb3">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_37.geom0"><gml:pos>535209.837811458 6948797.86481148 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>57</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id52701231-c044-4316-a0f5-dcb849910523">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_38.geom0"><gml:pos>535196.418742591 6948799.68777933 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>58</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id0bf76d8d-c14c-4960-a8b4-15f80fc7bdc8">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_39.geom0"><gml:pos>535196.216190608 6948797.10524154 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>59</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id42adc7df-3db5-4b40-962b-10617f68a64c">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_40.geom0"><gml:pos>535196.444061589 6948794.87716973 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>60</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idaa7f1d5f-249a-41e7-9b06-5d58329b407d">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_41.geom0"><gml:pos>535196.89980355 6948783.78744867 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>61</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id2234807e-8657-48cb-afe0-7c97e50aac3b">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_42.geom0"><gml:pos>535197.001079542 6948780.36938396 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>62</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id3c777808-1a69-4143-893a-4e9f2e39c39c">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_43.geom0"><gml:pos>535195.608534659 6948780.33140546 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>63</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id5e66eac0-c211-4db4-9709-8129781a5c4d">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_44.geom0"><gml:pos>535198.267029435 6948780.48331945 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>64</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id06d06f50-37fa-4ae2-9525-9479de286a14">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_45.geom0"><gml:pos>535198.406283923 6948779.04013657 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>65</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id586265ae-ae90-4821-b9d9-bd5c52e129c5">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_46.geom0"><gml:pos>535195.671832154 6948778.88822258 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>66</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id8bd82204-79ae-4741-a94a-c3da31037a2c">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_47.geom0"><gml:pos>535198.431602921 6948781.10363489 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>67</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id3148345c-b84b-422e-97c1-02cf29bcbf83">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_48.geom0"><gml:pos>535198.988620874 6948778.54641611 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>68</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id0c378c97-21ec-40e2-8fc6-a6346d9e4413">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_49.geom0"><gml:pos>535195.102154702 6948778.15397164 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>69</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id02099a72-db79-44b3-8b0b-ab66c0294917">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_50.geom0"><gml:pos>535194.88694322 6948781.12895389 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>70</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id5ebe23f4-45f8-4fbc-9516-6a626c40e2b9">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_51.geom0"><gml:pos>535199.178513358 6948781.54671736 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>71</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id99d2df37-b070-46fe-afd5-62ded0a42fff">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_52.geom0"><gml:pos>535197.899903966 6948799.63714133 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>72</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id22532dfb-7732-4a31-bba5-0d747939c6a0">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_53.geom0"><gml:pos>535194.709710235 6948799.42192985 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>73</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idc5d28dcd-ae24-44e3-a604-11368b3274c2">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_54.geom0"><gml:pos>535194.557796248 6948801.57404467 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>74</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id85daadf2-05bf-48d2-954f-7781b2591480">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_55.geom0"><gml:pos>535197.773308976 6948801.76393715 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>75</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id569bbadb-c96c-4cd3-8b2f-4bd280c9b68c">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_56.geom0"><gml:pos>535193.9374808 6948799.51054635 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>76</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id2d7436c7-f92c-4fcb-86e6-19293384a202">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_57.geom0"><gml:pos>535193.785566813 6948802.06776513 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>77</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id7f87f0c8-b9d2-4265-9f75-28fbe071cb64">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_58.geom0"><gml:pos>535198.330326929 6948802.4095716 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>78</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idf2e9e267-27c4-4ce7-bbe8-bfc7f0e3affc">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_59.geom0"><gml:pos>535198.570857409 6948799.21937787 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>79</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idc3ffa474-e472-483e-91dd-e0fa24b4987a">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_60.geom0"><gml:pos>535194.013437794 6948798.95352839 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>80</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id8ebf8e19-90de-4ed7-a40f-944c44f363ab">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_61.geom0"><gml:pos>535462.755610857 6948798.50411618 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>100</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id7fa85c9b-37fa-423e-a3ba-ce93a2c09c3e">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_62.geom0"><gml:pos>535463.629116283 6948797.82999786 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>101</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id3b84ff99-a402-441e-bc75-18ff75c95e52">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_63.geom0"><gml:pos>535464.236772232 6948795.52280418 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>102</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id4b3de044-15b3-420c-9398-5ad0cbf6b716">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_64.geom0"><gml:pos>535464.626051824 6948796.88053544 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>103</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id3c9fd33f-5231-4916-8010-4d6ae0c29525">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_65.geom0"><gml:pos>535465.31915939 6948798.07685809 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>104</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idf9ef484d-4073-4e74-b941-5ed9b0436625">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_66.geom0"><gml:pos>535466.543965912 6948798.74148179 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>105</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id92e1e794-1112-458f-ba96-18f99308a862">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_67.geom0"><gml:pos>535465.366632511 6948795.9595569 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>106</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idf3bda29f-4136-4abd-8e48-a76f08ea325c">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_68.geom0"><gml:pos>535466.971224001 6948796.13995476 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>107</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id04eff00d-5680-4037-a65b-d45f06e8922b">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_69.geom0"><gml:pos>535467.683320816 6948795.95006227 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>108</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id4a8f779b-19e7-4181-8b63-1c64508b4de6">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_70.geom0"><gml:pos>535468.243503644 6948795.3329117 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>109</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idb250576b-ccf3-4d51-a7aa-66b552ea1663">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_71.geom0"><gml:pos>535469.401847796 6948794.49738477 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>110</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id084584b6-73b9-4dfc-8f7c-16a911289831">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_72.geom0"><gml:pos>535471.24380489 6948794.1555783 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>111</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id6cc58858-4848-40b7-925a-37f0c43eb90f">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_73.geom0"><gml:pos>535473.02879424 6948793.94669657 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>112</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ide8b095f4-3736-4500-a30c-651c2fd2847f">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_74.geom0"><gml:pos>535066.5087448 6948777.3684893 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>200</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id4a19e402-6e5b-49c8-9196-d955261d89f0">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_75.geom0"><gml:pos>535067.913949181 6948776.00126342 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>201</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id5b3f498a-9f8a-426f-b131-0d5eb20d8f4e">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_76.geom0"><gml:pos>535068.550000109 6948774.30580149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>202</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id6b0506e3-9037-46c3-8284-9b5c6da2043a">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_77.geom0"><gml:pos>535069.262185817 6948775.48855371 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>203</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id4fbf27b5-d266-4afc-a1ad-c74a37444951">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_78.geom0"><gml:pos>535070.496486963 6948776.45700538 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>204</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idba79a175-985b-4718-af4f-ab25156273b2">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_79.geom0"><gml:pos>535072.927110758 6948777.61534953 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>205</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id768082d4-e882-4738-80d7-ec45cf616705">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_80.geom0"><gml:pos>535812.029603933 6948786.49400999 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>300</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id85bfee26-cbb1-4133-9ac0-a510e4eb289b">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_81.geom0"><gml:pos>535812.380905029 6948786.17119277 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>301</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id32c38e16-afa7-4ab2-8627-c4899ce43c2e">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_82.geom0"><gml:pos>535812.893614735 6948786.07624652 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>302</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id6e0473b4-e75c-4006-a42f-492897809b2e">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_83.geom0"><gml:pos>535812.684733003 6948785.70595618 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>303</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id5368e50e-7b53-404b-9b1a-6c8d3e186286">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_84.geom0"><gml:pos>535812.7227115 6948784.8894185 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>304</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idc6101944-3ecd-465c-a5e8-c7dc60b442c7">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_85.geom0"><gml:pos>535811.725775959 6948785.4021282 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>305</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id7c58642f-1961-4f73-ac86-294dc3ed0631">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_86.geom0"><gml:pos>535809.674937132 6948784.65205289 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>306</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id77fbf15f-a19a-4d09-b83d-3a9b4a699963">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_87.geom0"><gml:pos>535808.393162865 6948783.58865498 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>307</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id203810cf-105c-463f-99c2-e451ae23fb1b">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_88.geom0"><gml:pos>535807.690560675 6948782.78161193 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>308</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idf3c595e6-6b68-4020-be42-0ce04e142f03">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_89.geom0"><gml:pos>535802.126710895 6948776.34425672 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>309</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idc3f32125-6ade-4b32-a9b4-77f482933264">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_90.geom0"><gml:pos>535795.594409446 6948770.49556822 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>310</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idc892d7be-e88e-4343-95c0-f5ba2c83cd6a">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_91.geom0"><gml:pos>535789.214021985 6948766.62176154 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>311</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ide754debe-51be-43fb-bbf4-c8625c9c0055">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_92.geom0"><gml:pos>535782.580000112 6948741.75000149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>312</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id64350194-bcaa-463c-a6bc-19838395f084">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_93.geom0"><gml:pos>535793.540000112 6948743.83000149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>313</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id5ef54bdb-c04c-493b-b6df-73869f032659">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_94.geom0"><gml:pos>535788.900000112 6948768.17000149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>314</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idaefff4a9-7b58-4d24-b0fc-48dbbb7fa75d">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_95.geom0"><gml:pos>535786.560000112 6948769.76000149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>315</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id4862f8dc-cc0c-4e82-b38a-504fd8b5d110">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_96.geom0"><gml:pos>535777.560000112 6948768.04000149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>316</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id071573c5-219b-410a-acc7-8fbccc598216">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_97.geom0"><gml:pos>535303.840000112 6948786.42000149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1000</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id10d9eef8-e6b9-4d44-9706-921e6c3a4257">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_98.geom0"><gml:pos>535340.550000112 6948633.25000149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1001</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id6648a2ed-f0ab-41f2-a990-f5bc66905f5c">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_99.geom0"><gml:pos>535419.710000112 6948641.79000149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1002</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id6b3d5df8-a281-4e8b-9278-408b42c971f0">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_100.geom0"><gml:pos>535402.159937309 6948749.23433368 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>100</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id91431d6d-b7af-4539-ac35-7299ad236543">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_101.geom0"><gml:pos>535392.142928372 6948791.06207469 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1004</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id108ef87d-681f-4173-a4a4-727cd307fcbd">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_102.geom0"><gml:pos>535373.300000112 6948742.17000149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1005</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idcd6ae665-661c-406e-abcc-b718465b31e5">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_103.geom0"><gml:pos>535394.660000112 6948746.36000149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1006</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id38f42ea6-ca3d-47a2-92ea-d4ab5f5313fe">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_104.geom0"><gml:pos>535375.230000112 6948732.35000149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1007</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idd53415c9-39ff-4ac5-af7d-0e4f1db3e724">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_105.geom0"><gml:pos>535396.580000112 6948736.53000149 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1008</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id2f302fec-45f8-4c83-af54-aace9b61b6fc">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_106.geom0"><gml:pos>535383.728293168 6948744.16104118 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1009</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idda2c3a72-cfa9-41f4-a024-e5eae54baf99">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_107.geom0"><gml:pos>535381.15525001 6948749.87680495 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1010</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id75311f80-7ec7-412d-8e84-c84ca768c17e">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_108.geom0"><gml:pos>535379.636110139 6948755.155816 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1011</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ide8c0465d-7878-41c2-bd6c-4e61b54f5f17">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_109.geom0"><gml:pos>535377.091550853 6948763.4351283 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1012</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id1b9d4d7b-5899-478f-af7f-3376ed8a641e">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_110.geom0"><gml:pos>535370.635206398 6948775.13250532 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1013</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id83b44bfd-3338-42cd-bb7d-1f89077b5d93">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_111.geom0"><gml:pos>535359.013786379 6948785.91839841 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1014</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id230c72f6-1b6e-483a-bbc3-861f82e97eac">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_112.geom0"><gml:pos>535355.747635655 6948791.15943096 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1020</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PosteElectrique_Reco gml:id="ida7278456-a3cc-41d6-bb3f-8379bd39be93">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id0c722a92-692f-4883-9aac-fb9e90019ade" />
      <RecoStaR:Categorie xlink:href="Manoeuvre" />
      <RecoStaR:Code>666</RecoStaR:Code>
      <RecoStaR:InformationSupplementaire>ARM</RecoStaR:InformationSupplementaire>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypePoste xlink:href="AC3M" />
    </RecoStaR:RPD_PosteElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PosteElectrique_Reco gml:id="ide2b736ca-7b56-4b81-8492-8c6b10e22360">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idfbdfb2c7-70e9-44bf-a86d-a9779bbc15e2" />
      <RecoStaR:Categorie xlink:href="Distribution" />
      <RecoStaR:Code>666</RecoStaR:Code>
      <RecoStaR:InformationSupplementaire>antenne</RecoStaR:InformationSupplementaire>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypePoste xlink:href="PSSA" />
    </RecoStaR:RPD_PosteElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PosteElectrique_Reco gml:id="id39ecee18-5548-41f3-856e-93f99f071dcc">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id930af892-c791-4690-a0b4-57e383123b40" />
      <RecoStaR:Categorie xlink:href="PosteSource" />
      <RecoStaR:Code>source</RecoStaR:Code>
      <RecoStaR:InformationSupplementaire>source</RecoStaR:InformationSupplementaire>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypePoste xlink:href="GR2B" />
    </RecoStaR:RPD_PosteElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Support_Reco gml:id="id05d52f19-fafb-4596-812e-843b18eed213">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Classe xlink:href="D" />
      <RecoStaR:Effort uom="kN">10</RecoStaR:Effort>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Support_Reco_1.geom0"><gml:pos>535580.97029759 6948801.34118458 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:HauteurPoteau uom="m">11</RecoStaR:HauteurPoteau>
      <RecoStaR:Matiere xlink:href="Beton" />
      <RecoStaR:NatureSupport xlink:href="Poteau" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_Support_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Support_Reco gml:id="idf1365ae1-8fb8-44ee-b62a-1b7d73607379">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Support_Reco_2.geom0"><gml:pos>535789.214021985 6948766.62176154 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:NatureSupport xlink:href="Facade" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_Support_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_SupportModules_Reco gml:id="idb2223b97-9609-4d77-96e0-c52eecf1f898">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idcfcc3830-1c3d-408d-82f7-b5ec0ee827ba" />
      <RecoStaR:NombrePlages>6</RecoStaR:NombrePlages>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_SupportModules_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_SupportModules_Reco gml:id="id849e314b-30f0-488a-9ce3-45f54843ff1c">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id75f2acf3-22c0-4a07-a60c-6b769341cf14" />
      <RecoStaR:NombrePlages>9</RecoStaR:NombrePlages>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_SupportModules_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_SupportModules_Reco gml:id="id9e3fe083-7061-4ad1-ad86-42cf787fba4e">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idab15f3d6-caa9-42ae-9710-d02d93c62a10" />
      <RecoStaR:NombrePlages>9</RecoStaR:NombrePlages>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_SupportModules_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_SupportModules_Reco gml:id="idd6520913-02fc-44d2-96d7-b70e2b4dd9b6">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id8cf7d98a-8fc3-4c5e-b5fb-0de9b2021277" />
      <RecoStaR:NombrePlages>9</RecoStaR:NombrePlages>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_SupportModules_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Terre_Reco gml:id="id6926cb2f-2d42-46a4-9ba9-350fa47b028f">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idcfcc3830-1c3d-408d-82f7-b5ec0ee827ba" />
      <RecoStaR:NatureTerre xlink:href="TerreNeutre" />
      <RecoStaR:Resistance uom="ohms">0</RecoStaR:Resistance>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_Terre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Terre_Reco gml:id="idd9460d88-c1e9-4e84-bec8-54690c62502d">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id9eb1cb4d-0ac4-43a7-9b7c-57a6ce950221" />
      <RecoStaR:NatureTerre xlink:href="TerreNeutre" />
      <RecoStaR:Resistance uom="ohms">0</RecoStaR:Resistance>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_Terre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Terre_Reco gml:id="id9fce9b89-6483-40be-b42e-1bea06be0040">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:NatureTerre xlink:href="TerreNeutre" />
      <RecoStaR:Resistance uom="ohms">0</RecoStaR:Resistance>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_Terre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Terre_Reco gml:id="id2901ab60-a0da-48a4-8844-e51c3bfb7a64">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idc3b01631-75cd-46b5-b981-5221323706e1" />
      <RecoStaR:NatureTerre xlink:href="TerreNeutre" />
      <RecoStaR:Resistance uom="ohms">0</RecoStaR:Resistance>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_Terre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Terre_Reco gml:id="id4b87e98c-2e82-45ca-b9dd-de45952152ed">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:NatureTerre xlink:href="TerreMasses" />
      <RecoStaR:Resistance uom="ohms">0</RecoStaR:Resistance>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_Terre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Terre_Reco gml:id="ide8b98221-6e68-4c48-a8c4-07ccaff4472c">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:NatureTerre xlink:href="TerreMasses" />
      <RecoStaR:Resistance uom="ohms">0</RecoStaR:Resistance>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_Terre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Terre_Reco gml:id="ide344e953-0053-4b48-9d7a-049257f445a3">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id75f2acf3-22c0-4a07-a60c-6b769341cf14" />
      <RecoStaR:NatureTerre xlink:href="TerreNeutre" />
      <RecoStaR:Resistance uom="ohms">0</RecoStaR:Resistance>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_Terre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Terre_Reco gml:id="id63edb3e3-1958-4aa0-8b91-0c4f5ca47ab8">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idab15f3d6-caa9-42ae-9710-d02d93c62a10" />
      <RecoStaR:NatureTerre xlink:href="TerreNeutre" />
      <RecoStaR:Resistance uom="ohms">0</RecoStaR:Resistance>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_Terre_Reco>
  </gml:featureMember>
</gml:FeatureCollection>
