<?xml version="1.0" encoding="utf-8" ?>
<gml:FeatureCollection
    xmlns:ogr_gmlas="http://gdal.org/ogr/gmlas"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:RecoStaR="http://StaR-Elec.com"
    xmlns:gco="http://www.isotc211.org/2005/gco"
    xmlns:gmd="http://www.isotc211.org/2005/gmd"
    xmlns:gsr="http://www.isotc211.org/2005/gsr"
    xmlns:gss="http://www.isotc211.org/2005/gss"
    xmlns:gts="http://www.isotc211.org/2005/gts"
    xmlns:gml="http://www.opengis.net/gml/3.2"
    xmlns:gmlexr="http://www.opengis.net/gml/3.3/exr"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xsi:schemaLocation="http://StaR-Elec.com https://gitlab.com/StaR-Elec/StaR-Elec/-/raw/main/RecoStaR/SchemaStarElecRecoStar.xsd" >
<!-- GML au format OpenRecoStar v0.7 via GDAL / QGIS. -->
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id286b3e96-aa9f-4534-8a5b-d5a27679d44f">
      <RecoStaR:noeudreseau xlink:href="idc01c2a30-e21d-4e2f-bde0-1b47e5121903" />
      <RecoStaR:cableelectrique xlink:href="id062c8a40-4bb1-4d8a-987d-d22586b5c4a7" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id9c17b1fd-ba65-4f51-807f-3f74ac26afdb">
      <RecoStaR:noeudreseau xlink:href="id75e65bce-0b92-4b26-a08e-97c8275fa3d7" />
      <RecoStaR:cableelectrique xlink:href="id284a40c5-8a57-479a-9b71-026365eac10b" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id128c0854-bee6-44d6-8d1a-fd7ce2c946a2">
      <RecoStaR:noeudreseau xlink:href="id5e875cca-f920-40bf-99a4-b5861aa2c62c" />
      <RecoStaR:cableelectrique xlink:href="id395661be-a182-429b-ace4-373a5cb76116" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="ide4ce8fb4-81cc-48e1-8fe2-97a27304ad35">
      <RecoStaR:noeudreseau xlink:href="id8f0ec1f8-22f6-4441-987e-048e016baf0b" />
      <RecoStaR:cableelectrique xlink:href="id44b236cc-c2d9-428a-a2ea-60769b05ae2d" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idbf1f3ad3-7e06-41ee-8745-fd6d38c4be0d">
      <RecoStaR:noeudreseau xlink:href="idbf965adc-db89-4ebb-8721-7dc8548fde99" />
      <RecoStaR:cableelectrique xlink:href="id888ef3d8-6762-4c23-8494-1fbb7d327f17" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id6298378b-ba64-4f76-844a-18ab44a98c0d">
      <RecoStaR:noeudreseau xlink:href="id5eadc88c-a76c-46c5-a241-12cfb42be3df" />
      <RecoStaR:cableelectrique xlink:href="idbff2ffd9-5aad-4a0d-9704-f837db69d311" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idb2d6a3f6-62bf-4151-8170-3467694685e4">
      <RecoStaR:noeudreseau xlink:href="idedb9c5b1-885b-4edc-a0cf-7369562d8c3e" />
      <RecoStaR:cableelectrique xlink:href="idfa33ce87-0afa-4be9-a338-e9fecf368f40" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id4e1a7f81-6d3c-4ea6-8a7d-96777f267e03">
      <RecoStaR:noeudreseau xlink:href="idff29882d-5f14-4071-b7fb-c302c98c2607" />
      <RecoStaR:cableelectrique xlink:href="id284a40c5-8a57-479a-9b71-026365eac10b" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id0cadf8a8-8141-4a17-8486-a7185aec233b">
      <RecoStaR:noeudreseau xlink:href="id4a45c33e-c393-4a46-9117-5cab18fac4e2" />
      <RecoStaR:cableelectrique xlink:href="idbff2ffd9-5aad-4a0d-9704-f837db69d311" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idb21d2280-490f-4316-81f5-05c3c9077a82">
      <RecoStaR:noeudreseau xlink:href="ida8643163-a4b6-4a30-8b41-355c97deca8d" />
      <RecoStaR:cableelectrique xlink:href="idb11c9d97-887f-496c-b630-e1fcc7a89b67" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idea6bbae6-1789-47d0-8f11-b985a66ff971">
      <RecoStaR:noeudreseau xlink:href="idbbe46605-1e7e-45de-828e-939770ff6ff3" />
      <RecoStaR:cableelectrique xlink:href="id062c8a40-4bb1-4d8a-987d-d22586b5c4a7" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id33b1f5b7-3300-48a2-8e19-4d3c6a2e1339">
      <RecoStaR:noeudreseau xlink:href="idbbe46605-1e7e-45de-828e-939770ff6ff3" />
      <RecoStaR:cableelectrique xlink:href="id395661be-a182-429b-ace4-373a5cb76116" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idf059700f-b33f-4582-8190-9a264c0d8af9">
      <RecoStaR:noeudreseau xlink:href="idee3ab106-4e1a-411d-81b0-ae29992e1ed4" />
      <RecoStaR:cableelectrique xlink:href="id44b236cc-c2d9-428a-a2ea-60769b05ae2d" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="ida321ae1a-4142-495b-89a5-b48fc90636a1">
      <RecoStaR:noeudreseau xlink:href="id7f0ec3f4-35eb-4820-b885-54df9468a017" />
      <RecoStaR:cableelectrique xlink:href="id888ef3d8-6762-4c23-8494-1fbb7d327f17" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id61837092-91cd-4b35-8aab-34fea2d77772">
      <RecoStaR:noeudreseau xlink:href="id7f0ec3f4-35eb-4820-b885-54df9468a017" />
      <RecoStaR:cableelectrique xlink:href="idfa33ce87-0afa-4be9-a338-e9fecf368f40" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id5f3859a9-2e40-4a5e-8a6d-b79c22c88f65">
      <RecoStaR:cables xlink:href="id284a40c5-8a57-479a-9b71-026365eac10b" />
      <RecoStaR:cheminement xlink:href="idb3eaa01c-55eb-42bd-839a-195f2da8b12c" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id68e826c4-7295-49bc-81cb-9c5b1477201e">
      <RecoStaR:cables xlink:href="id395661be-a182-429b-ace4-373a5cb76116" />
      <RecoStaR:cheminement xlink:href="id67286b4d-60a0-4317-847e-12f70cfb52db" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="ida796a4bc-70b5-4e63-84ca-75a7dc45ea01">
      <RecoStaR:cables xlink:href="id062c8a40-4bb1-4d8a-987d-d22586b5c4a7" />
      <RecoStaR:cheminement xlink:href="id16eda36b-47d8-4cf3-88e3-df625c5fe6a8" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id582ef08c-a9d8-4ae5-8fd0-498eb19aacd1">
      <RecoStaR:cables xlink:href="idbff2ffd9-5aad-4a0d-9704-f837db69d311" />
      <RecoStaR:cheminement xlink:href="id0803ae88-fc8f-4788-8d99-699b9040d0df" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id66ea7b3b-d839-4457-8bfc-11c3b37a46b7">
      <RecoStaR:cables xlink:href="id44b236cc-c2d9-428a-a2ea-60769b05ae2d" />
      <RecoStaR:cheminement xlink:href="idc8f0155f-28be-4dae-8e6f-1046b4a28e6d" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id1c8ae36c-944c-4e8a-8469-4187e2502367">
      <RecoStaR:cables xlink:href="idb11c9d97-887f-496c-b630-e1fcc7a89b67" />
      <RecoStaR:cheminement xlink:href="id9a4288ab-562d-4582-8c72-f8fb964af8bc" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id486f1561-bf40-4740-8f5b-4a9c7b60724d">
      <RecoStaR:cables xlink:href="id0c9fea22-33ac-4d0f-9170-31af385258e7" />
      <RecoStaR:cheminement xlink:href="id990b62b9-3fe1-484b-8f5c-cb89dd18f28d" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="iddc9b9d2d-59a5-4b9a-85e8-cf22bf523504">
      <RecoStaR:cables xlink:href="idfa33ce87-0afa-4be9-a338-e9fecf368f40" />
      <RecoStaR:cheminement xlink:href="iddfd46ebf-66ad-41f1-8ee3-b331882b7c6b" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id48ffe9aa-741b-4b69-8cd7-f940ce1c3ed7">
      <RecoStaR:cables xlink:href="id888ef3d8-6762-4c23-8494-1fbb7d327f17" />
      <RecoStaR:cheminement xlink:href="id0bb376c8-9b9b-43c0-81f7-6c4fc9203d9e" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Metadata gml:id="id61ee70fe-ac9e-445e-a08d-7e49361cea9f">
      <RecoStaR:Datecreation>2025-01-14</RecoStaR:Datecreation>
      <RecoStaR:Logiciel>QGIS OpenRecoStar Plugin</RecoStaR:Logiciel>
      <RecoStaR:Producteur>Enedis</RecoStaR:Producteur>
      <RecoStaR:Responsable>Enedis</RecoStaR:Responsable>
      <RecoStaR:SRS>EPSG:2154</RecoStaR:SRS>
    </RecoStaR:Metadata>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="idd8ff3a2b-95c1-4728-8820-4453152e84e2">
      <RecoStaR:ouvrage xlink:href="id75e65bce-0b92-4b26-a08e-97c8275fa3d7" />
      <RecoStaR:materiel xlink:href="id33dbdce0-885f-4750-b4ba-c67c873dfe09" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="idd2db7f15-f8cf-4478-8b8d-dac2e8bc4087">
      <RecoStaR:ouvrage xlink:href="id5e875cca-f920-40bf-99a4-b5861aa2c62c" />
      <RecoStaR:materiel xlink:href="ideee4e417-d3f6-4098-8099-c2e1a500b40a" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="idaacdec2e-8fd6-43c3-8649-7cc430003e86">
      <RecoStaR:ouvrage xlink:href="idedb9c5b1-885b-4edc-a0cf-7369562d8c3e" />
      <RecoStaR:materiel xlink:href="id38bd8f88-5c47-40f8-8d4b-7eb92772e6d9" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:ReseauUtilite gml:id="Reseau">
      <RecoStaR:Mention>Test export GML OpenRecoStar</RecoStaR:Mention>
      <RecoStaR:Nom>Réseau public de distribution</RecoStaR:Nom>
      <RecoStaR:Responsable>Enedis</RecoStaR:Responsable>
      <RecoStaR:Theme>ELECTRD</RecoStaR:Theme>
    </RecoStaR:ReseauUtilite>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id284a40c5-8a57-479a-9b71-026365eac10b">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>LiaisonReseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">0</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id395661be-a182-429b-ace4-373a5cb76116">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>LiaisonReseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">0</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id062c8a40-4bb1-4d8a-987d-d22586b5c4a7">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>DerivationIndividuelle</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>2</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">0</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idbff2ffd9-5aad-4a0d-9704-f837db69d311">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>DerivationIndividuelle</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>2</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">35</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id44b236cc-c2d9-428a-a2ea-60769b05ae2d">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>LiaisonReseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">0</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idb11c9d97-887f-496c-b630-e1fcc7a89b67">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">150</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">70</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>Functional</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id0c9fea22-33ac-4d0f-9170-31af385258e7">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">150</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">70</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>Functional</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idfa33ce87-0afa-4be9-a338-e9fecf368f40">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>LiaisonReseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">35</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id888ef3d8-6762-4c23-8494-1fbb7d327f17">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>DerivationIndividuelle</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>2</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">35</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>Functional</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="idd45db6bf-ce24-4e0c-bcff-666a843f1772">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="id3b9f506e-05a8-485e-b89d-16507b9d5eb4" />
      <RecoStaR:FonctionCoffret xlink:href="Separable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_1.geom0"><gml:pos>589991.752109706 6427283.54973879 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="Encastree" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="CIBE" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="id0a27f004-1616-41f7-9b0a-96c79e695b12">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="id233ca6ca-41f1-4231-a8ac-f6ea678299b9" />
      <RecoStaR:FonctionCoffret xlink:href="Separable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_2.geom0"><gml:pos>590026.027550024 6427296.74450001 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="Encastree" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="CIBE" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="id13707938-1272-489a-ae6f-5a5d56605340">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="idb1e965d9-9864-4c52-bed2-9762fc3306e1" />
      <RecoStaR:FonctionCoffret xlink:href="Manoeuvrable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_3.geom0"><gml:pos>589961.563150065 6427313.39102503 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="Encastree" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="RMBT300" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="id01c2f7d6-8772-4d2b-86a9-f544a2e66760">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="id6c9491b9-e51e-49e8-b540-c08c5224a11a" />
      <RecoStaR:FonctionCoffret xlink:href="Separable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_4.geom0"><gml:pos>589958.035150068 6427309.93582503 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="SurSocleBeton" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="CIBE" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="ida2149364-3344-4fc7-bf95-5d36dff7b0bf">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="id38a08b83-72a2-4bef-9f9b-8de58468f488" />
      <RecoStaR:FonctionCoffret xlink:href="Separable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_5.geom0"><gml:pos>589990.456852642 6427356.98027274 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="Encastree" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="CIBE" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CoupeCircuitAFusibles_Reco gml:id="ide741c4af-92ce-4df6-b4dd-2976b88f6871">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idd45db6bf-ce24-4e0c-bcff-666a843f1772" />
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CoupeCircuitAFusibles_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CoupeCircuitAFusibles_Reco gml:id="idbbe46605-1e7e-45de-828e-939770ff6ff3">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id0a27f004-1616-41f7-9b0a-96c79e695b12" />
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CoupeCircuitAFusibles_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CoupeCircuitAFusibles_Reco gml:id="idee3ab106-4e1a-411d-81b0-ae29992e1ed4">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id01c2f7d6-8772-4d2b-86a9-f544a2e66760" />
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CoupeCircuitAFusibles_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CoupeCircuitAFusibles_Reco gml:id="id7f0ec3f4-35eb-4820-b885-54df9468a017">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="ida2149364-3344-4fc7-bf95-5d36dff7b0bf" />
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CoupeCircuitAFusibles_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="id3b9f506e-05a8-485e-b89d-16507b9d5eb4">
      <RecoStaR:Ligne2.5D><gml:LineString srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_1.geom0"><gml:posList srsDimension="3">589991.752109706 6427283.54973879 0 589991.465218279 6427283.63745031 0 589991.406743938 6427283.44618936 0 589991.980526792 6427283.27076633 0 589992.039001133 6427283.46202728 0 589991.752109706 6427283.54973879 0</gml:posList></gml:LineString></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_1.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">589991.752109706 6427283.54973879 0 589991.465218279 6427283.63745031 0 589991.406743938 6427283.44618936 0 589991.980526792 6427283.27076633 0 589992.039001133 6427283.46202728 0 589991.752109706 6427283.54973879 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="id233ca6ca-41f1-4231-a8ac-f6ea678299b9">
      <RecoStaR:Ligne2.5D><gml:LineString srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_2.geom0"><gml:posList srsDimension="3">590026.027550024 6427296.74450001 0 590025.736461306 6427296.81707658 0 590025.688076927 6427296.62301743 0 590026.270254363 6427296.4778643 0 590026.318638742 6427296.67192344 0 590026.027550024 6427296.74450001 0</gml:posList></gml:LineString></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_2.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">590026.027550024 6427296.74450001 0 590025.736461306 6427296.81707658 0 590025.688076927 6427296.62301743 0 590026.270254363 6427296.4778643 0 590026.318638742 6427296.67192344 0 590026.027550024 6427296.74450001 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="idb1e965d9-9864-4c52-bed2-9762fc3306e1">
      <RecoStaR:Ligne2.5D><gml:LineString srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_3.geom0"><gml:posList srsDimension="3">589961.563150065 6427313.39102503 0 589961.863150065 6427313.39102503 0 589961.863150065 6427313.59102503 0 589961.263150065 6427313.59102503 0 589961.263150065 6427313.39102503 0 589961.563150065 6427313.39102503 0</gml:posList></gml:LineString></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_3.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">589961.563150065 6427313.39102503 0 589961.863150065 6427313.39102503 0 589961.863150065 6427313.59102503 0 589961.263150065 6427313.59102503 0 589961.263150065 6427313.39102503 0 589961.563150065 6427313.39102503 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="id6c9491b9-e51e-49e8-b540-c08c5224a11a">
      <RecoStaR:Ligne2.5D><gml:LineString srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_4.geom0"><gml:posList srsDimension="3">589958.035150068 6427309.93582503 0 589958.335150069 6427309.93582503 0 589958.335150069 6427310.13582504 0 589957.735150068 6427310.13582504 0 589957.735150068 6427309.93582503 0 589958.035150068 6427309.93582503 0</gml:posList></gml:LineString></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_4.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">589958.035150068 6427309.93582503 0 589958.335150069 6427309.93582503 0 589958.335150069 6427310.13582504 0 589957.735150068 6427310.13582504 0 589957.735150068 6427309.93582503 0 589958.035150068 6427309.93582503 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="id38a08b83-72a2-4bef-9f9b-8de58468f488">
      <RecoStaR:Ligne2.5D><gml:LineString srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_5.geom0"><gml:posList srsDimension="3">589990.456852642 6427356.98027274 0 589990.756852642 6427356.98027274 0 589990.756852642 6427357.18027274 0 589990.156852642 6427357.18027274 0 589990.156852642 6427356.98027274 0 589990.456852642 6427356.98027274 0</gml:posList></gml:LineString></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_geomsupp_5.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">589990.456852642 6427356.98027274 0 589990.756852642 6427356.98027274 0 589990.756852642 6427357.18027274 0 589990.156852642 6427357.18027274 0 589990.156852642 6427356.98027274 0 589990.456852642 6427356.98027274 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="id75e65bce-0b92-4b26-a08e-97c8275fa3d7">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_1.geom0"><gml:pos>589994.102902834 6427312.69957359 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Jonction</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="id5e875cca-f920-40bf-99a4-b5861aa2c62c">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idaf0e8d52-a223-425f-bcb9-d53f758eed15" />
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_2.geom0"><gml:pos>590017.829150019 6427306.95890002 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Jonction</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="idedb9c5b1-885b-4edc-a0cf-7369562d8c3e">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_3.geom0"><gml:pos>589991.747049434 6427354.39987916 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Derivation</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="id33dbdce0-885f-4750-b4ba-c67c873dfe09">
      <RecoStaR:Fabricant>inconnu</RecoStaR:Fabricant>
      <RecoStaR:Modele>inconnu</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>inconnu</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="ideee4e417-d3f6-4098-8099-c2e1a500b40a">
      <RecoStaR:Fabricant>inconnu</RecoStaR:Fabricant>
      <RecoStaR:Modele>inconnu</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>inconnu</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="id38bd8f88-5c47-40f8-8d4b-7eb92772e6d9">
      <RecoStaR:Fabricant>A</RecoStaR:Fabricant>
      <RecoStaR:Modele>B</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>C</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_ModuleRaccordement_Reco gml:id="id8f0ec1f8-22f6-4441-987e-048e016baf0b">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id13707938-1272-489a-ae6f-5a5d56605340" />
      <RecoStaR:Coupure>false</RecoStaR:Coupure>
      <RecoStaR:NbPlagesOccupees>1</RecoStaR:NbPlagesOccupees>
      <RecoStaR:Protection>false</RecoStaR:Protection>
    </RecoStaR:RPD_ModuleRaccordement_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id16eda36b-47d8-4cf3-88e3-df625c5fe6a8">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_1.geom0"><gml:posList srsDimension="3">590026.027550024 6427296.74450001 0 590023.608350023 6427285.1861 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id990b62b9-3fe1-484b-8f5c-cb89dd18f28d">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_2.geom0"><gml:posList srsDimension="3">589952.820764711 6427324.06236978 0 589952.783469705 6427324.12791605 0 589952.745048023 6427324.19280832 0 589952.705511131 6427324.25702721 0 589952.664870825 6427324.32055356 0 589952.623139235 6427324.38336842 0 589952.580328812 6427324.44545304 0 589952.536452333 6427324.50678888 0 589952.49152289 6427324.56735766 0 589952.445553891 6427324.6271413 0 589952.398559052 6427324.68612195 0 589952.350552399 6427324.74428201 0 589952.301548257 6427324.80160413 0 589952.251561249 6427324.85807121 0 589952.200606292 6427324.91366639 0 589952.148698591 6427324.96837309 0 589952.095853635 6427325.02217497 0 589952.042087196 6427325.07505598 0 589951.987415316 6427325.12700035 0 589951.931854311 6427325.17799257 0 589951.875420761 6427325.22801743 0 589951.818131506 6427325.27705999 0 589951.760003642 6427325.32510563 0 589951.701054514 6427325.37214 0 589951.641301714 6427325.41814907 0 589951.580763072 6427325.46311911 0 589951.519456655 6427325.5070367 0 589951.457400755 6427325.54988874 0 589951.394613893 6427325.59166244 0 589951.331114803 6427325.63234533 0 589951.266922435 6427325.67192527 0 589951.202055944 6427325.71039045 0 589951.136534687 6427325.74772939 0 589951.070378216 6427325.78393096 0 589951.003606274 6427325.81898434 0 589950.936238785 6427325.85287907 0 589950.868295853 6427325.88560505 0 589950.799797752 6427325.91715251 0 589950.730764924 6427325.94751203 0 589950.661217968 6427325.97667455 0 589950.591177637 6427326.00463137 0 589950.520664834 6427326.03137415 0 589950.449700598 6427326.05689491 0 589950.378306108 6427326.08118603 0 589950.306502667 6427326.10424026 0 589950.234311702 6427326.12605073 0 589950.161754757 6427326.14661092 0 589950.088853483 6427326.16591471 0 589950.015629633 6427326.18395632 0 589949.94210506 6427326.20073037 0 589949.868301704 6427326.21623187 0 589949.794241588 6427326.23045618 0 589949.719946812 6427326.24339906 0 589949.645439548 6427326.25505664 0 589949.570742028 6427326.26542546 0 589949.495876543 6427326.27450241 0 589949.420865435 6427326.28228478 0 589949.345731086 6427326.28877026 0 589949.270495918 6427326.29395691 0 589949.195182382 6427326.29784317 0 589949.119812952 6427326.3004279 0 589949.044410119 6427326.30171032 0 589948.968996384 6427326.30169004 0 589948.893594252 6427326.30036707 0 589948.818226223 6427326.29774181 0 589948.742914787 6427326.29381505 0 589948.66768242 6427326.28858794 0 589948.592551569 6427326.28206206 0 589948.517544657 6427326.27423934 0 589948.442684064 6427326.26512214 0 589948.367992131 6427326.25471315 0 589948.293491147 6427326.2430155 0 589948.219203342 6427326.23003267 0 589948.145150887 6427326.21576853 0 589948.071355877 6427326.20022735 0 589947.997840336 6427326.18341375 0 589947.9246262 6427326.16533277 0 589947.851735317 6427326.14598978 0 589947.779189439 6427326.12539057 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>B</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>C</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="idb3eaa01c-55eb-42bd-839a-195f2da8b12c">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_3.geom0"><gml:posList srsDimension="3">589994.102902834 6427312.69957359 0 589997.394013214 6427305.6471942 0 589994.57306146 6427292.717832 0 589991.752109706 6427283.54973879 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id67286b4d-60a0-4317-847e-12f70cfb52db">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_4.geom0"><gml:posList srsDimension="3">590017.829150019 6427306.95890002 0 590026.430750025 6427303.73330002 0 590027.505950025 6427301.98610002 0 590026.027550024 6427296.74450001 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="idc8f0155f-28be-4dae-8e6f-1046b4a28e6d">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_5.geom0"><gml:posList srsDimension="3">589961.358750065 6427313.39102503 0 589961.221550065 6427313.18942503 0 589960.319950066 6427313.18942503 0 589958.035150068 6427309.93582503 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id0bb376c8-9b9b-43c0-81f7-6c4fc9203d9e">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_6.geom0"><gml:posList srsDimension="3">589990.551169484 6427357.18027274 0 589990.709564179 6427357.95943995 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>C</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>C</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id9a4288ab-562d-4582-8c72-f8fb964af8bc">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_7.geom0"><gml:posList srsDimension="3">589952.820764711 6427324.06236978 0 589958.343984387 6427319.73985003 0 589961.509466019 6427315.41733029 0 589961.475355225 6427313.59102503 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>C</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id0803ae88-fc8f-4788-8d99-699b9040d0df">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_8.geom0"><gml:posList srsDimension="3">589961.736750064 6427313.39102503 0 589960.135150066 6427306.15302504 0 589960.986350065 6427305.83942504 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="iddfd46ebf-66ad-41f1-8ee3-b331882b7c6b">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString srsName="EPSG:2154" gml:id="RPD_PleineTerre_Reco_virt_9.geom0"><gml:posList srsDimension="3">589991.747049434 6427354.39987916 0 589990.549009556 6427355.36752675 0 589990.456852642 6427356.98027274 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointDeComptage_Reco gml:id="idc01c2a30-e21d-4e2f-bde0-1b47e5121903">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointDeComptage_Reco_1.geom0"><gml:pos>590023.608350023 6427285.1861 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:NumeroPRM>14227758354925</RecoStaR:NumeroPRM>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_PointDeComptage_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointDeComptage_Reco gml:id="id5eadc88c-a76c-46c5-a241-12cfb42be3df">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idf4058177-306d-4fe0-9f32-fb2f5e60783a" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointDeComptage_Reco_2.geom0"><gml:pos>589960.986350065 6427305.83942504 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:NumeroPRM>14547382903928</RecoStaR:NumeroPRM>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_PointDeComptage_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointDeComptage_Reco gml:id="idff29882d-5f14-4071-b7fb-c302c98c2607">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idd45db6bf-ce24-4e0c-bcff-666a843f1772" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointDeComptage_Reco_3.geom0"><gml:pos>589991.643773976 6427283.4371329 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:NumeroPRM>12345678901234</RecoStaR:NumeroPRM>
      <RecoStaR:PrecisionXY>C</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>C</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>Functional</RecoStaR:Statut>
    </RecoStaR:RPD_PointDeComptage_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointDeComptage_Reco gml:id="id4a45c33e-c393-4a46-9117-5cab18fac4e2">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id13707938-1272-489a-ae6f-5a5d56605340" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointDeComptage_Reco_4.geom0"><gml:pos>589961.69298074 6427313.50084386 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:NumeroPRM>123</RecoStaR:NumeroPRM>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_PointDeComptage_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointDeComptage_Reco gml:id="idbf965adc-db89-4ebb-8721-7dc8548fde99">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointDeComptage_Reco_5.geom0"><gml:pos>589990.709564179 6427357.95943995 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:NumeroPRM>12345678901234</RecoStaR:NumeroPRM>
      <RecoStaR:PrecisionXY>C</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>C</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>Functional</RecoStaR:Statut>
    </RecoStaR:RPD_PointDeComptage_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id13f13d31-1d0c-4a9e-9d79-fe524b79b468">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_1.geom0"><gml:pos>589991.752109706 6427283.54973879 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">56</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id992c36b7-0669-4cf0-8acd-16f136a4b859">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_2.geom0"><gml:pos>589994.57306146 6427292.717832 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">55.8</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>2</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ida0ed4682-c4e3-4b9e-859c-54ba10352f3b">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_3.geom0"><gml:pos>589997.394013214 6427305.6471942 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">55.7</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>3</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id3be695ca-f184-4b76-8a98-f7abaff8ff74">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_4.geom0"><gml:pos>589994.102902834 6427312.69957359 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">55.4</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>4</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id584093e7-29e4-41d0-8bea-5dce5c2305af">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_5.geom0"><gml:pos>590023.608350023 6427285.1861 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id6c2b6b14-4b06-4797-b5e9-d3ff80e48070">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_6.geom0"><gml:pos>590026.027550024 6427296.74450001 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>2</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id9db9e2c2-cdae-443b-afc2-0f201d4c9069">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_7.geom0"><gml:pos>590027.505950025 6427301.98610002 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>3</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id0d90fe63-0b23-444f-8fea-d0fc595580f7">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_8.geom0"><gml:pos>590026.430750025 6427303.73330002 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>4</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idfb94a1b5-f67a-4771-9fca-ad5bc20275a6">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_9.geom0"><gml:pos>590017.829150019 6427306.95890002 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>5</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idaaaaac49-1427-4305-8d3b-74888863ca5a">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_10.geom0"><gml:pos>589960.986350065 6427305.83942504 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idc3d5a185-fcb1-45c4-be69-9a1c45f5ab57">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_11.geom0"><gml:pos>589960.135150066 6427306.15302504 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>2</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idb3bb4efd-d46d-4a25-8466-8ec78db30f5b">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_12.geom0"><gml:pos>589961.563150065 6427313.39102503 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>3</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ide85195f6-7ecc-426c-932a-4da65f22c210">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_13.geom0"><gml:pos>589961.221550065 6427313.18942503 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>4</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idfc4b00e5-541d-47f4-bd73-b584cf725b7b">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_14.geom0"><gml:pos>589960.319950066 6427313.18942503 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>5</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id12d7c35a-9b6a-4c00-9e13-a9ba627d85ce">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_15.geom0"><gml:pos>589958.035150068 6427309.93582503 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>5</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id57f25c55-569f-47d2-8aaf-ce55589bc0cc">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_16.geom0"><gml:pos>589952.820764711 6427324.06236978 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">123</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>47</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id3b107ca5-d1d4-4d96-be24-72a64cb967c3">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_17.geom0"><gml:pos>589991.747049434 6427354.39987916 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id36652136-02a6-48bb-809a-6ebd716acd9c">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_18.geom0"><gml:pos>589990.549009556 6427355.36752675 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>2</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id64547746-5826-4032-a104-f19d5e41c210">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_19.geom0"><gml:pos>589990.456852642 6427356.98027274 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>3</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Support_Reco gml:id="idaf0e8d52-a223-425f-bcb9-d53f758eed15">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Classe xlink:href="B" />
      <RecoStaR:Effort uom="kN">10</RecoStaR:Effort>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Support_Reco_1.geom0"><gml:pos>590017.829150019 6427306.95890002 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:HauteurPoteau uom="m">11</RecoStaR:HauteurPoteau>
      <RecoStaR:Matiere xlink:href="Beton" />
      <RecoStaR:NatureSupport xlink:href="Poteau" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_Support_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Support_Reco gml:id="idf4058177-306d-4fe0-9f32-fb2f5e60783a">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Effort uom="kN">0</RecoStaR:Effort>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Support_Reco_2.geom0"><gml:pos>589960.986350065 6427305.83942504 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:HauteurPoteau uom="m">0</RecoStaR:HauteurPoteau>
      <RecoStaR:Matiere xlink:href="Beton" />
      <RecoStaR:NatureSupport xlink:href="Facade" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_Support_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_SupportModules_Reco gml:id="ida8643163-a4b6-4a30-8b41-355c97deca8d">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="id13707938-1272-489a-ae6f-5a5d56605340" />
      <RecoStaR:NombrePlages>6</RecoStaR:NombrePlages>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_SupportModules_Reco>
  </gml:featureMember>
</gml:FeatureCollection>
