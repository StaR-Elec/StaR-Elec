<?xml version="1.0" encoding="utf-8" ?>
<gml:FeatureCollection
    xmlns:ogr_gmlas="http://gdal.org/ogr/gmlas"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:RecoStaR="http://StaR-Elec.com"
    xmlns:gco="http://www.isotc211.org/2005/gco"
    xmlns:gmd="http://www.isotc211.org/2005/gmd"
    xmlns:gsr="http://www.isotc211.org/2005/gsr"
    xmlns:gss="http://www.isotc211.org/2005/gss"
    xmlns:gts="http://www.isotc211.org/2005/gts"
    xmlns:gml="http://www.opengis.net/gml/3.2"
    xmlns:gmlexr="http://www.opengis.net/gml/3.3/exr"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xsi:schemaLocation="http://StaR-Elec.com https://gitlab.com/StaR-Elec/StaR-Elec/-/raw/main/RecoStaR/SchemaStarElecRecoStar.xsd" >
<!-- GML au format OpenRecoStar v0.7 via GDAL / QGIS. DATE: 2024-09-27T17:10:47.246388 GPKG: Reco-Star-Elec-RPD.gpkg -->
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idcb185c11-0f46-4b17-8315-374e9d789ea5">
      <RecoStaR:noeudreseau xlink:href="idb8daf23b-81b2-484b-89c7-ca490255dc30" />
      <RecoStaR:cableelectrique xlink:href="id1a1d65e2-0834-4d62-8989-3352c72ea172" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id4782df97-ee6d-4a6c-81cc-7415dea25560">
      <RecoStaR:noeudreseau xlink:href="idb8daf23b-81b2-484b-89c7-ca490255dc30" />
      <RecoStaR:cableelectrique xlink:href="idc2f955e3-b01e-4759-a7c6-32f378e3589a" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="id8961c01b-bdb6-4c83-8488-0e8bf6f4367e">
      <RecoStaR:noeudreseau xlink:href="idb8daf23b-81b2-484b-89c7-ca490255dc30" />
      <RecoStaR:cableelectrique xlink:href="idce1c0485-2c5d-4c60-996d-be1cafdcfb27" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:CableElectrique_NoeudReseau gml:id="idc080f137-9ef9-492c-84e2-b8feecc5a47c">
      <RecoStaR:noeudreseau xlink:href="id9b9e90d5-0911-4c57-a345-b5c1067cdbca" />
      <RecoStaR:cableelectrique xlink:href="id1a1d65e2-0834-4d62-8989-3352c72ea172" />
    </RecoStaR:CableElectrique_NoeudReseau>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="idcc0865eb-0f0e-4586-89cc-d5d44e0c8597">
      <RecoStaR:cables xlink:href="idce1c0485-2c5d-4c60-996d-be1cafdcfb27" />
      <RecoStaR:cheminement xlink:href="id1c63efaa-47d0-4d1f-8015-4c3409c58cbd" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id7fe0bbd3-380b-4210-8f70-3914d3cf2eb1">
      <RecoStaR:cables xlink:href="idc2f955e3-b01e-4759-a7c6-32f378e3589a" />
      <RecoStaR:cheminement xlink:href="id2c6ac07a-c587-48a8-89f0-a95703429466" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Cheminement_Cables gml:id="id94f7fce9-94f2-45b0-8eca-2ff8fa78a17f">
      <RecoStaR:cables xlink:href="id1a1d65e2-0834-4d62-8989-3352c72ea172" />
      <RecoStaR:cheminement xlink:href="idfc820288-c3fe-4924-8ab2-4ddd83151c2a" />
    </RecoStaR:Cheminement_Cables>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Metadata gml:id="id0e6bd89c-d30c-4334-8f5e-e400c8561e3a">
      <RecoStaR:Datecreation>2024-09-27</RecoStaR:Datecreation>
      <RecoStaR:Logiciel>QGIS OpenRecoStar Plugin</RecoStaR:Logiciel>
      <RecoStaR:Producteur>Enedis</RecoStaR:Producteur>
      <RecoStaR:Responsable>Enedis</RecoStaR:Responsable>
      <RecoStaR:SRS>EPSG:2154</RecoStaR:SRS>
    </RecoStaR:Metadata>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:Ouvrage_Materiel gml:id="id6580bac5-e558-48e7-82fd-0f4730de0127">
      <RecoStaR:ouvrage xlink:href="idb8daf23b-81b2-484b-89c7-ca490255dc30" />
      <RecoStaR:materiel xlink:href="id6028fed2-9adc-4a46-a508-b4916d6bf349" />
    </RecoStaR:Ouvrage_Materiel>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:ReseauUtilite gml:id="Reseau">
      <RecoStaR:Mention>Test export GML OpenRecoStar</RecoStaR:Mention>
      <RecoStaR:Nom>Réseau public de distribution</RecoStaR:Nom>
      <RecoStaR:Responsable>Enedis</RecoStaR:Responsable>
      <RecoStaR:Theme>ELECTRD</RecoStaR:Theme>
    </RecoStaR:ReseauUtilite>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idce1c0485-2c5d-4c60-996d-be1cafdcfb27">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">150</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">70</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>Functional</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="idc2f955e3-b01e-4759-a7c6-32f378e3589a">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>Reseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">150</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">70</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>Functional</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CableElectrique_Reco gml:id="id1a1d65e2-0834-4d62-8989-3352c72ea172">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:FonctionCable xlink:href="DistributionEnergie" />
      <RecoStaR:HierarchieBT>LiaisonReseau</RecoStaR:HierarchieBT>
      <RecoStaR:Isolant>Reticulee</RecoStaR:Isolant>
      <RecoStaR:Materiau>Alu</RecoStaR:Materiau>
      <RecoStaR:NombreConducteurs>4</RecoStaR:NombreConducteurs>
      <RecoStaR:Section uom="mm-2">35</RecoStaR:Section>
      <RecoStaR:SectionNeutre uom="mm-2">35</RecoStaR:SectionNeutre>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CableElectrique_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Coffret_Reco gml:id="idbdaf6bef-0869-46ae-a8cc-2fc03be32990">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:geometriesupplementaire xlink:href="id6924366e-954f-4611-9db2-9cc05c4c0152" />
      <RecoStaR:FonctionCoffret xlink:href="Separable" />
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Coffret_Reco_1.geom0"><gml:pos>612410.48163068 6311342.77768836 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:ImplantationArmoire xlink:href="Encastree" />
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:TypeCoffret xlink:href="CIBE" />
    </RecoStaR:RPD_Coffret_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_CoupeCircuitAFusibles_Reco gml:id="id9b9e90d5-0911-4c57-a345-b5c1067cdbca">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:conteneur xlink:href="idbdaf6bef-0869-46ae-a8cc-2fc03be32990" />
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
    </RecoStaR:RPD_CoupeCircuitAFusibles_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_GeometrieSupplementaire_Reco gml:id="id6924366e-954f-4611-9db2-9cc05c4c0152">
      <RecoStaR:Ligne2.5D><gml:Polygon gml:id="RPD_Coffret_Reco_geomsupp_1.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">612410.48163068 6311342.77768836 0 612410.238925582 6311342.60135279 0 612410.356482632 6311342.43954939 0 612410.841892829 6311342.79222054 0 612410.724335779 6311342.95402394 0 612410.48163068 6311342.77768836 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Ligne2.5D>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Surface2.5D><gml:Polygon gml:id="RPD_Coffret_Reco_geomsupp_1.geom0"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">612410.48163068 6311342.77768836 0 612410.238925582 6311342.60135279 0 612410.356482632 6311342.43954939 0 612410.841892829 6311342.79222054 0 612410.724335779 6311342.95402394 0 612410.48163068 6311342.77768836 0</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon></RecoStaR:Surface2.5D>
    </RecoStaR:RPD_GeometrieSupplementaire_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Jonction_Reco gml:id="idb8daf23b-81b2-484b-89c7-ca490255dc30">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:DomaineTension>BT</RecoStaR:DomaineTension>
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_Jonction_Reco_1.geom0"><gml:pos>612399.233413479 6311349.59645663 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
      <RecoStaR:Statut>UnderCommissionning</RecoStaR:Statut>
      <RecoStaR:TypeJonction>Derivation</RecoStaR:TypeJonction>
    </RecoStaR:RPD_Jonction_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_Materiel_Reco gml:id="id6028fed2-9adc-4a46-a508-b4916d6bf349">
      <RecoStaR:Fabricant>NEXANS</RecoStaR:Fabricant>
      <RecoStaR:Modele>J3UP</RecoStaR:Modele>
      <RecoStaR:NumeroSerieLot>DKJO9809HZ</RecoStaR:NumeroSerieLot>
    </RecoStaR:RPD_Materiel_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="idfc820288-c3fe-4924-8ab2-4ddd83151c2a">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString gml:id="RPD_PleineTerre_Reco_virt_1.geom0"><gml:posList srsDimension="3">612399.233413479 6311349.59645663 0 612400.95438823 6311346.56056404 0 612406.208382228 6311343.65335403 0 612410.026284534 6311343.44319427 0 612410.48163068 6311342.77768836 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>A</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>A</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id2c6ac07a-c587-48a8-89f0-a95703429466">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString gml:id="RPD_PleineTerre_Reco_virt_2.geom0"><gml:posList srsDimension="3">612399.233413479 6311349.59645663 0 612399.877319461 6311349.82679698 0 612421.313614974 6311357.53265484 0 612447.093212191 6311366.70963102 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>B</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>C</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PleineTerre_Reco gml:id="id1c63efaa-47d0-4d1f-8015-4c3409c58cbd">
      <RecoStaR:reseau xlink:href="Reseau">
</RecoStaR:reseau>
      <RecoStaR:Geometrie><gml:LineString gml:id="RPD_PleineTerre_Reco_virt_3.geom0"><gml:posList srsDimension="3">612374.027668989 6311340.57976754 0 612399.233413479 6311349.59645663 0</gml:posList></gml:LineString></RecoStaR:Geometrie>
      <RecoStaR:PrecisionXY>B</RecoStaR:PrecisionXY>
      <RecoStaR:PrecisionZ>C</RecoStaR:PrecisionZ>
    </RecoStaR:RPD_PleineTerre_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="ida9e0d238-bade-4fef-a52f-7593bf11fa0f">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_1.geom0"><gml:pos>612399.233413479 6311349.59645663 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>1</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="idcd16d97b-d7fc-4902-a2bb-b2e8900bf4b3">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_2.geom0"><gml:pos>612400.95438823 6311346.56056404 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>2</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id702d11fd-77e7-4243-992d-4881cc7ff25b">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_3.geom0"><gml:pos>612406.208382228 6311343.65335403 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>3</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="iddc0a9963-8551-439b-b201-0d753503a050">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_4.geom0"><gml:pos>612410.026284534 6311343.44319427 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>4</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
  <gml:featureMember>
    <RecoStaR:RPD_PointLeveOuvrageReseau_Reco gml:id="id57425e8b-8c35-401d-b550-dd8a4c92eb1a">
      <RecoStaR:Geometrie><gml:Point srsName="EPSG:2154" gml:id="RPD_PointLeveOuvrageReseau_Reco_5.geom0"><gml:pos>612410.48163068 6311342.77768836 0</gml:pos></gml:Point></RecoStaR:Geometrie>
      <RecoStaR:Leve uom="m">0</RecoStaR:Leve>
      <RecoStaR:NumeroPoint>5</RecoStaR:NumeroPoint>
      <RecoStaR:PrecisionXYnum>10</RecoStaR:PrecisionXYnum>
      <RecoStaR:PrecisionZnum>10</RecoStaR:PrecisionZnum>
      <RecoStaR:Producteur>test recostar</RecoStaR:Producteur>
      <RecoStaR:TypeLeve>AltitudeGeneratrice</RecoStaR:TypeLeve>
    </RecoStaR:RPD_PointLeveOuvrageReseau_Reco>
  </gml:featureMember>
</gml:FeatureCollection>
