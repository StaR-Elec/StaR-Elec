# StaR-Elec

StaR-Elec est un standard d'échange de données géographiques pour les projets de construction de réseaux électriques.

Il s'agit d'un standard validé par le CNIG : https://cnig.gouv.fr/ressources-pcrs-dt-dict-reseaux-electriques-a11745.html

## Documentation

 * Webinaires de présentation de la déclinaison de StaR-Elec des 12 et 13 septembre
    * [Enregistrement vidéo](https://youtu.be/iqstWoaQ28k)
    * [Supports ppt](https://gitlab.com/StaR-Elec/StaR-Elec/-/tree/main/Documentation)